<?php $user_position = $this->session->userdata('user_position'); ?>
<div class="content">
   <div class="container-fluid">

      <div class="animated fadeIn widget grid6">
         <div class="widget-heading dashboard-header">DASHBOARD</div>
         <div class="panel-body">

            <div class="form-group"> 
                    <div class="row top_tiles main-widget">
                         <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url("sent_issues?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=1") ?>" class="tile-stats color-danger<?php echo ($this->Models->status() == 1?' active-panel':'') ?>">
                                <?php $_open = $this->Models->ticket_status(1) ?>
                                <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                                <span class="count count_1"><?php echo $_open->num_rows() ?></span>
                                <h3>TOTAL In Progress</h3>
                            </a>
                        </div>
                       
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url("sent_issues?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=4") ?>" class="tile-stats color-info<?php echo ($this->Models->status() == 4?' active-panel':'') ?>">
                                <?php $_ongoing = $this->Models->ticket_status(4) ?>
                                <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                                <span class="count count_3"><?php echo $_ongoing->num_rows() ?></span>
                                <h3>TOTAL Return to Sender</h3>
                            </a>
                        </div>
                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url("sent_issues?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=3") ?>" class="tile-stats color-primary<?php echo ($this->Models->status() == 3?' active-panel':'') ?>">
                                <?php $_resolved = $this->Models->ticket_status(3) ?>
                                <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                                <span class="count count_4"><?php echo $_resolved->num_rows() ?></span>
                                <h3>TOTAL Completed</h3>
                            </a>
                        </div>
                    </div>
                </div>
            
            
            <div class="row">

               <div class="col-sm-6 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Select Year</span></span>
                     <select class="form-control select-year-report" data-select="<?php echo date('Y') ?>">
                         <?php for ($x = 2016; $x <= 2020; $x++) { ?>
                            <option value="<?php echo $x ?>"><?php echo $x ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Select Month</span></span>
                     <select name="status" class="form-control select-month-report" data-select="<?php echo date('n') ?>">
                         <?php for ($a = 1; $a <= 12; $a++) { ?>
                             <?php
                             $dateObj = DateTime::createFromFormat('!m', $a);
                             $monthName = $dateObj->format('F'); // March
                             ?>
                            <option value="<?php echo $a ?>"><?php echo $monthName ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-4 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Date From</span></span>
                     <input type="text" class="form-control date-from" />
                  </div>
               </div>
               <div class="col-sm-4 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Date To</span></span>
                     <input type="text" class="form-control date-to" disabled="disabled" />
                  </div>
               </div>
               <div class="col-sm-2 form-group">
                  <button class="btn btn-primary btn-block export-all-button" disabled="disabled"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export All  KPI</button>
               </div>
               <div class="col-sm-2 form-group">
                  <button class="btn btn-default btn-block show-all-button" disabled="disabled"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Show All  KPI</button>
               </div>
            </div>

            
            
            
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <table class="inbox-table table table-bordered">
                        <thead>
                           <tr>
                              <th>Month</th>
                              <th>Number of issue</th>
                           </tr>
                        </thead>
                        <tbody class="table-month-dash">
                              
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="col-sm-6 dash-month" style="text-align:center;">
                  <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="line-height:300px;height:300px;"></i>
               </div>
               
               
               <div class="col-sm-12">
                  <canvas id="year-report" height="80"></canvas>
               </div>
               <div class="col-sm-12">
                  <canvas id="montly-report" height="80"></canvas>
               </div>
            </div>
            <hr>
            <div class="row">
               <div class="col-sm-6">
                  <div class="text-center">Type of issues</div>
                  <canvas id="issue-report"></canvas>
               </div>
               <div class="col-sm-6">
                  <div class="text-center">Status</div>
                  <canvas id="status-report"></canvas>
               </div>
            </div>
         </div>
      </div>

      <?php if ($user_position === '0') { ?>
      
          <div class="animated fadeIn widget grid6">
             <div class="panel-body">
                <div class="row">
                   <div class="col-sm-6 form-group">
                      <div class="input-group">
                         <span class="input-group-addon" id="basic-addon3"><span>Date From</span></span>
                         <input type="text" class="form-control date-from date-from-report" />
                      </div>
                   </div>
                   <div class="col-sm-6 form-group">
                      <div class="input-group">
                         <span class="input-group-addon" id="basic-addon3"><span>Date To</span></span>
                         <input type="text" class="form-control date-to date-to-report" disabled="disabled" />
                      </div>
                   </div>

                </div>
                <table class="inbox-table table table-bordered">
                   <thead>
                      <tr>
                         <th class="hide-xs text-center">Name</th>
                         <th class="hide-xs description">Lastname</th>
                         <th class="hide-xs text-center">Report</th>
                      </tr>
                   </thead>
                   <tbody class="support-view-report">
                       <?php
                       $_it = $this->Models->user_position_support();
                       foreach ($_it->result() as $sup) {
                           ?>
                          <tr>
                             <td><?php echo $sup->fname ?></td>
                             <td><?php echo $sup->lastname ?></td>
                             <td class="text-center">
                                <a href="<?php echo $sup->ID ?>" class="export-report-btn btn btn-success btn-xs"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export My Report</a>
                                <a href="<?php echo $sup->ID ?>" class="view-report-btn btn btn-info btn-xs"><i class="fa fa-book" aria-hidden="true"></i> View My Report</a>
                                <a href="<?php echo site_url('calendar/user/' . $sup->ID) ?>" class="btn btn-primary btn-xs"><i class="fa fa-calendar" aria-hidden="true"></i> View My Calendar</a>
                             </td>
                          </tr>
                          <?php
                      }
                      ?>
                   </tbody>
                </table>
             </div>
          </div>
      <?php } ?>

     
   </div>
   <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>


