<?php date_default_timezone_set('Asia/Manila'); ?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="<?php echo site_url('images/ico.png'); ?>">
        <meta name="page-url" class="page-url" content="<?php echo site_url(); ?>" data-type="<?php echo $this->session->userdata('user_position') ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta property="og:image" content="<?php echo site_url('page/final-logo.png'); ?>" />
        <meta property="og:image:width" content="500" />
        <meta property="og:image:height" content="500" />
        <title><?php print $this->Models->title(); ?></title>

        <link href="<?php echo site_url('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('css/bootstrap-theme.css'); ?>" rel="stylesheet" type="text/css"/>

        <link href="<?php echo site_url('jquery-ui/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('jquery-ui/jquery-ui.structure.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('jquery-ui/jquery-ui.theme.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/animate.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/theme.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/font.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/datatables.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/template.css?ver=2'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/pnotify.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/pnotify.buttons.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/pnotify.nonblock.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/fullcalendar.min.css'); ?>" rel='stylesheet' />
        <link href="<?php echo site_url('assets/css/fullcalendar.print.css'); ?>" rel='stylesheet' media='print' />
        <link href="<?php echo site_url('assets/css/jquery.datetimepicker.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/jquery.qtip.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/stylesheet.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo site_url('assets/css/style.css?ver=3'); ?>" rel="stylesheet" type="text/css"/>

        <script src="<?php echo site_url('js/pace.min.js'); ?>" data-pace-options='{ "ajax": false }' ></script>
        <script src="<?php echo site_url('js/jquery-2.1.3.min.js'); ?>"></script>
        <script src="<?php echo site_url('js/jquery.datatables.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('js/datatables.bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?php echo site_url('js/jquery.form.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('js/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo site_url('js/jquery.ellipsis.js'); ?>"></script>
        <script src="<?php echo site_url('js/notify.js'); ?>"></script>
        <script src="<?php echo site_url('js/jquery.timepicker.min.js'); ?>"></script>
        <script src="<?php echo site_url('js/jquery.cropit.js'); ?>"></script>
        <script src="<?php echo site_url('js/confirm.js'); ?>"></script>
        <script src="<?php echo site_url('js/message.js'); ?>"></script>
        <script src="<?php echo site_url('js/cookies.js'); ?>" type="text/javascript"></script>
        <script src='<?php echo site_url('js/jquery.datetimepicker.full.min.js'); ?>'></script>
        <script src='<?php echo site_url('ckeditor/ckeditor.js'); ?>'></script>
        <script src="<?php echo site_url('js/pubnub.4.0.11.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('js/pnotify.js'); ?>"></script>
        <script src="<?php echo site_url('js/pnotify.buttons.js'); ?>"></script>
        <script src="<?php echo site_url('js/pnotify.nonblock.js'); ?>"></script>
        <script src="<?php echo site_url('js/pnotify.desktop.js'); ?>"></script>
        <script src='<?php echo site_url('js/moment.min.js'); ?>'></script>
        <script src='<?php echo site_url('js/jquery.qtip.min.js'); ?>'></script>
        <script src='<?php echo site_url('js/fullcalendar.min.js'); ?>'></script>
        <script src="<?php echo site_url('js/Chart.bundle.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo site_url('assets/js/javascript.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo site_url('assets/js/script.js?ver='.rand(1,99999)); ?>3" type="text/javascript"></script>

    </head>
    <body class="<?php echo (!$this->session->userdata('user_session')?'login-panel':'') ?>">
        <div class="main<?php echo (!(get_cookie('sidemenu')) ? '' : ' close-sidemenu') ?>">