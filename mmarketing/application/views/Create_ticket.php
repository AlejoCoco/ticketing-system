<?php
    $get_last_id =  $this->Modules->get_last_id();
    foreach ($get_last_id->result() as $_lastid) {
          $lastid = $_lastid->ID;
}


?>
    <?php
             $query_data = $this->Models->user_id($this->session->userdata('user_session'));
             $name_data = $query_data->result();
             ?>

<div class="content">
    <div class="container-fluid">
        <form class="create-ticket-form animated fadeIn widget grid6" method="post" enctype="multipart/form-data">
            <div class="widget-heading">CREATE NEW TICKET</div>
            <div class="panel-heading">
            <div class="row">
                    <div class="col-sm-5 form-group">
                         <div class="input-group">
                          <span style="font-weight:bold;">Job Order # 
                            <?php echo $lastid+1; ?>
                          </span>
                        </div>
                    </div>
                    <div class="col-sm-1 form-group"></div>
                    <div class="col-sm-6 form-group">
                        <div class="input-group">
                           
                        </div>
                    </div>
                </div>
             <div class="row">
                    <div class="col-sm-5 form-group">
                         <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Name of Requestor </span>
                            <input name="requestor" class="form-control input-inline " value="<?php echo $name_data[0]->fname . ' ' . $name_data[0]->lastname; ?>">
                        </div>
                    </div>
                    <div class="col-sm-1 form-group"></div>
                    <div class="col-sm-6 form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Requesting Department </span>
                             <select name="requestby" class="form-control input-inline ">
                                <option value="">Select Here</option>
                                <option value="Executive Team">Executive Team</option>
                                <option value="Client Success">Client Success</option>
                                <option value="Human Resources">Human Resources</option>
                                <option value="Recruitment">Recruitment</option>
                                <option value="People Engagement">People Engagement</option>
                                <option value="Training">Training</option>
                                <option value="IT">IT</option>
                                <option value="Admin and Facilities">Admin and Facilities</option>
                                <option value="Finance">Finance</option>
                                <option value="Marketing Production">Marketing Production</option>
                                <option value="Marketing Content">Marketing Content</option>
                                <option value="Marketing CRM and OB">Marketing CRM and OB</option>
                                <option value="Marketing Digital">Marketing Digital</option>
                            </select>
                        </div>
                    </div>
                </div>
            <div class="row">
                    <div class="col-sm-5 form-group">
                         <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Project Title </span>
                            <input name="Title" class="form-control input-inline ">
                        </div>
                    </div>
                    <div class="col-sm-1 form-group"></div>
                    <div class="col-sm-6 form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Target Audience </span>
                             <select name="target" class="form-control input-inline ">
                                <option value="">Select Here</option>
                                <option value="TOA ANZ">TOA ANZ</option>
                                <option value="TOA USA">TOA USA</option>
                                <option value="TOA Global Employees">TOA Global Employees</option>
                                <option value="TOA Global Recruitement">TOA Global Recruitement</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5 form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">This is a job for</span>
                             <select name="subject" class="form-control input-inline others">
                                <option value="">Select Here</option>
                                <option value="1">Marketing Production</option>
                                <option value="2">Marketing Content</option>
                                <option value="3">Marketing CRM</option>
                                <option value="4">Marketing Digital</option>
                            </select>
                        
                            <input type="hidden" name="priority" class="form-control input-inline" value="1">
                        </div>
                    </div>
                    <div class="col-sm-1 form-group"></div>
                    <div class="col-sm-6 form-group">
                          <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Required Before </span>
                            <input name="required" class="form-control input-inline date-content">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-5 form-group">

                      
                    </div>
                    <div class="col-sm-1 form-group">
                        <input type="file" name="file[]" class="form-control fileupload" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                    </div>
                    <div class="col-sm-6 form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Approval By</span>
                             <select name="approvedby" class="form-control input-inline approvedby">
                                <option value="">Select Here</option>
                                <option value="Chief Approval">Chief Approval</option>
                                 <option value="Director Approval">Director Approval</option>
                            </select>
                        
                            <input type="hidden" name="priority" class="form-control input-inline" value="1">
                        </div>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-sm-5 form-group">
                        <div class="input-group othersfield hide ">
                            <span class="input-group-addon" id="basic-addon3">Others </span>
                            <input name="otherfield" class="form-control input-inline ">
                        </div>
                    </div>
                    <div class="col-sm-1 form-group"></div>
                    <div class="col-sm-6 form-group">
                            
                    </div>
                </div>
            </div>
          


         
            <div class="panel-body overflow-x">
                <div class="form-group"> 
                    <p><strong>Detail the output you need. <span style='color:red;'>*</span></strong><span style='color:lightgray;'> (For example - tarpauline banner artwork for Christmas event.)</span></p>
                    <textarea class="create-ticket" name="create_ticket" id="create-ticket"></textarea>
                </div>
                <div class="form-group hide"> 
                    <p><strong>Content brief <span style='color:red;'>*</span></strong><span style='color:lightgray;'>(The words that need to be in the output)</span></p>
                    <textarea class="contenttextarea" name="contenttextarea" id="contenttextarea" style="width:100%;height:280px;"></textarea>
                </div>
                 <div class="form-group"> 
                    <div class="row">
                    <div class="col-sm-5 form-group">
                       <div class="input-group">

                            <input type="text" class="form-control fake-path" placeholder="doc,ppt,xsl,pdf,image only" readonly="readonly">
                            <span class="input-group-btn">
                                <button class="btn btn-default attached" type="button">Attach file <i class="fa fa-paperclip" aria-hidden="true"></i> MAX 5MB</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 form-group"></div>
                    <div class="col-sm-6 form-group">
                      <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Link to delivery </span>
                            <input name="link" class="form-control input-inline ">
                        </div>
                    </div>
                </div>
                </div>

                <div class="progress hide">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                        0% Complete (success)
                    </div>
                </div>
                 <div class='row'>
                    <div class="col-sm-12" style="text-align:center !important;">
                            <button type="submit" class="btn btn-success btn-send"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> <span>SEND</span></button>
                       
                    </div>
            </div>
            </div>
           
        </form>
    </div>
    <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>
<script>
    (function (j) {
        j(window).load(function () {

        });
    })(jQuery);
    
    $('.others').on('change', function (e) {
        var valueofothers = $('.others').val();
        console.log(valueofothers);
        if(valueofothers == 5){
           $('.othersfield').removeClass('hide');
        }else{
           $('.othersfield').addClass('hide');
        }
        
        
    });


</script>  