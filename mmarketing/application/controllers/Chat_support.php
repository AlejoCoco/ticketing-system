<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_support extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->model('Modules');
        $this->load->model('Models');
    }
    function index() {
 
    }
    function user(){
        $this->load->view('includes/header');
         if ($this->session->userdata('user_session'))   {
           
        } else {
            $this->load->view('Login');
        }
        $this->load->view('includes/footer');
    }
}
