<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Models extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        header('Access-Control-Allow-Origin: *');
    }

    function index() {
        
    }

    function title() {
        if (($this->uri->segment(1))) {
            return ucfirst(str_replace('_', ' ', $this->uri->segment(1))) . ' | The Outsourced Accountant';
        } else {
            return 'The Outsourced Accountant';
        }
    }

    function from_date() {
        if (($this->uri->segment(6))) {
            return $this->uri->segment(6);
        } else {
            return date('m-d-Y');
        }
    }

    function to_date() {
        if (($this->uri->segment(7))) {
            return $this->uri->segment(7);
        } else {
            return date('m-d-Y');
        }
    }

    function max_row() {
        if (!($this->uri->segment(4))) {
            return '15';
        } else if (is_numeric($this->uri->segment(4))) {
            return ltrim(rtrim($this->uri->segment(4)));
        } else {
            return '15';
        }
    }

    function page() {
        if (!$this->input->get('page')) {
            return '1';
        } else if (is_numeric($this->input->get('page'))) {
            return ltrim(rtrim($this->input->get('page')));
        } else {
            return '1';
        }
    }

    function status() {
        if (!$this->input->get('status')) {
            return '1';
        } else if (is_numeric($this->input->get('status'))) {
            return ltrim(rtrim($this->input->get('status')));
        } else {
            return '1';
        }
    }

    function limit_text($text, $limit) {
        $_space = array("\n", "&nbsp;");
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
            $text = str_replace('&nbsp;', ' ', $text);
        }
        return strip_tags(addslashes(str_replace($_space, ' ', $text)));
    }

    function special_chars($des) {
        $clear = strip_tags($des);
        $clear = html_entity_decode($clear);
        $clear = urldecode($clear);
        $clear = preg_replace('/[^A-Za-z0-9]/', ' ', $clear);
        $clear = preg_replace('/ +/', ' ', $clear);
        $clear = trim($clear);
        return $clear;
    }

    function quotes($string) {
        $chars = array("'", '"', '\\');
        return str_replace($chars, '', $string);
    }

    function start_date() {
        if ((!$this->input->get('start_date'))) {
            return date('m-d-Y');
        } else if (($this->input->get('start_date'))) {
            return ($this->input->get('start_date'));
        } else {
            return date('m-d-Y');
        }
    }

    function update_rdate_submitted($data) {
        $_data = array("date_solve" => strtotime($data['submited_date']));
        $this->db->where('ID', $data['date_id']);
        return $this->db->update('message', $_data);
    }

    function update_date_submitted($data) {
        $_data = array("date" => strtotime($data['submited_date']));
        $this->db->where('ID', $data['date_id']);
        return $this->db->update('message', $_data);
    }

    function end_date() {
        if (!($this->input->get('end_date'))) {
            return date('m-d-Y');
        } else if (($this->input->get('end_date'))) {
            return ($this->input->get('end_date'));
        } else {
            return date('m-d-Y');
        }
    }

    function lctn() {
        if (!($this->input->get('location'))) {
            return '';
        } else if (($this->input->get('location'))) {
            return $this->input->get('location');
        } else {
            return '';
        }
    }

    function keyword() {
        if (!($this->input->get('keyword'))) {
            return '';
        } else {
            return $this->quotes($this->input->get('keyword'));
        }
    }

    function cc_search() {
        if (!($this->input->post('name'))) {
            return '';
        } else {
            return $this->quotes($this->input->post('name'));
        }
    }

    function login_query($data) {
        return $this->db->query("SELECT * FROM `user_info` WHERE email ='" . $data['email'] . "' and password='" . $data['password'] . "' ");
    }

    function login_online($data, $id) {
        $this->db->where('ID', $id);
        return $this->db->update('user_info', $data);
    }

    function user_password($data, $id) {
        $this->db->where('ID', $id);
        return $this->db->update('user_info', $data);
    }

    function delete_user($id) {
        $this->db->where('ID', $id);
        return $this->db->delete('user_info');
    }

    function user_add($data) {
        return $this->db->insert('user_info', $data);
    }

    function user_update_status($data) {
        $this->db->where('ID', $this->session->userdata('user_session'));
        return $this->db->update('user_info', $data);
    }

    function user_update($data, $id) {
        $this->db->where('ID', $id);
        return $this->db->update('user_info', $data);
    }

    function user_position_support() {
        return $this->db->query("SELECT * FROM `user_info` WHERE position = '1' || position = '0' ");
    }

    function user_position() {
        return $this->db->query("SELECT ID AS value, concat(fname, ' ', lastname) AS label FROM `user_info` WHERE position = '1' || position = '0' ");
    }

    function user_id($id) {
        return $this->db->query("SELECT * FROM `user_info` WHERE ID = " . $id);
    }

    function users() {
        return $this->db->query("SELECT * FROM `user_info` ORDER BY `lastname`");
    }

    function cc_users() {
        $srh = explode(" ", $this->cc_search());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "`fname` LIKE '%" . $field . "%'";
                $conditions[] = "`lastname` LIKE '%" . $field . "%'";
                $conditions[] = "`email` LIKE '%" . $field . "%'";
            }
        }
        $_query = '';
        if (count($conditions) > 0) {
            $_query .= "WHERE " . implode(' || ', $conditions);
        }
        return $this->db->query("SELECT ID AS value, concat(fname, ' ', lastname) AS label FROM `user_info` " . $_query . " ");
    }

    function user_info() {
        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->pgn() * $setLimit) - ($setLimit);

        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "`first` LIKE '%" . $field . "%'";
                $conditions[] = "`lastname` LIKE '%" . $field . "%'";
                $conditions[] = "`email` LIKE '%" . $field . "%'";
            }
            if ($this->lctn()) {
                $conditions[] = "user_info.location LIKE '%" . $this->lctn() . "%'";
            }
        }
        $_query = "SELECT * FROM `user_info` ";
        if (count($conditions) > 0) {
            $_query .= "WHERE " . implode(' || ', $conditions);
        }
        return $this->db->query($_query . " ORDER BY `lastname` LIMIT $pageLimit , $setLimit");
    }

    function user_pagination() {
        $page_url = base_url($this->uri->segment(1) . '');
        $per_page = $this->max_row();
        $page = $this->pgn();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "`first` LIKE '%" . $field . "%'";
                $conditions[] = "`lastname` LIKE '%" . $field . "%'";
                $conditions[] = "`email` LIKE '%" . $field . "%'";
            }
            if ($this->lctn()) {
                $conditions[] = "user_info.location LIKE '%" . $this->lctn() . "%'";
            }
        }
        $_query = "SELECT * FROM `user_info` ";
        if (count($conditions) > 0) {
            $_query .= "WHERE " . implode(' || ', $conditions);
        }
        $query = $this->db->query($_query . " ORDER BY `lastname`");
        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate.= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "?location=" . $this->lctn() . "'>Next</a></li>";
                $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "'>Last</a></li>";
            } else {
                $setPaginate.= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate.= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate.= "</ul>\n";
        }
        return $setPaginate;
    }

    ////////// reports
    function issue_status($status) {
        if ($status == 1) {
            return '<strong class="label label-danger">OPEN</strong>';
        } else if ($status == 2) {
            return '<strong class="label label-warning">RE OPEN</strong>';
        } else if ($status == 3) {
            return '<strong class="label label-info">ONGOING</strong>';
        } else if ($status == 4) {
            return '<strong class="label label-primary">RESOLVED</strong>';
        }
    }

    function issue_string($status) {
        if ($status == 1) {
            return 'In Progress';
        } else if ($status == 2) {
            return 'RE OPEN';
        } else if ($status == 3) {
            return 'Completed';
        } else if ($status == 4) {
            return 'Return to Sender';
        }
    }

    function issue_type($status) {
        if ($status == 1) {
            return 'Marketing Production';
        } else if ($status == 2) {
            return 'Marketing Content';
        } else if ($status == 3) {
            return 'Marketing CRM';
        } else if ($status == 4) {
            return 'Marketing Digital';
        } 

  
    }

    function priority_string($priority) {
        if ($priority == 1) {
            return 'Important and Urgent';
        } else if ($priority == 2) {
            return 'Important but not urgent';
        } else if ($priority == 3) {
            return 'Not important but urgent';
        } else if ($priority == 4) {
            return 'Not important and not urgent';
        }
    }

    function priority($priority) {
        if ($priority == 1) {
            return '<strong class="label label-danger">Important and Urgent</strong>';
        } else if ($priority == 2) {
            return '<strong class="label label-warning">Important but not urgent</strong>';
        } else if ($priority == 3) {
            return '<strong class="label label-info">Not important but urgent</strong>';
        } else if ($priority == 4) {
            return '<strong class="label label-primary">Not important and not urgent</strong>';
        }
    }

    function support_email() {
        return $this->db->query("SELECT email FROM `user_info` WHERE position = '111'  ");
    }

    function mailer($data, $id) {
        $user = $this->user_id($this->session->userdata('user_session'));
        $_user = $user->result();
        $_email = '';
        $email = $this->support_email();
        foreach ($email->result() as $supports) {
            $_email .= $supports->email . ',';
        }
        $_email = rtrim($_email, ",");
        $_email = $_user[0]->email . "," . $_email;
        $_message = "<html>";
        $_message .= "<head>";
        $_message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>";
        $_message .= "<title>The Outsourced Accountant</title>";
        $_message .= "</head>";
        $_message .= "<body bgcolor='#efefef' style='font-family: sans-serif;font-size:14px;color: #8c8c8c;'>";
        $_message .= '<div style="text-align:center;"><img src="' . site_url('images/final-logo.png') . '" alt="logo" align="center"></div>';
        $_message .= '<div bgcolor="#FFFFFF" style="padding: 15px;background-color: #FFFFFF; max-width:900px; margin: 0 auto 0 auto; text-align:center;">';
        $_message .= '<table style="background-color:#FFF;width:100%;max-width:900px; border:1px solid #DDD;border-spacing: 0px;border-collapse: collapse; font-family:sans-serif;color: #8c8c8c;font-size: 14px;text-align:left;">';
        $_message .= '<tbody>';
        $_message .= '<tr>';
        $_message .= '<td style="padding:15px;"><img src="' . site_url('images/TOA logo.png') . '" alt="logo"></td>';
        $_message .= '</tr>';
        $_message .= '<tr>';
        $_message .= '<td style="padding: 15px;border: 1px solid #DDD;background-color: #FFFFFF;">From: ' . $_user[0]->fname . ' ' . $_user[0]->lastname . "<br/>\n";
        $_message .= 'Location: ' . $_user[0]->location . "<br/>";
        $_message .= 'Subject: ' . $this->issue_type($data['subject']) . "<br/>\n";
        $_message .= 'Ticket Link: <a href="' . site_url('issues_page/issue_id/' . $id) . '" style="color:#337ab7">Click Here</a>';
        $_message .= '</td>';
        $_message .= '</tr>';
        $_message .= '<tr>';
        $_message .= '<td  style="padding: 15px;border-top: 1px solid #DDD;background-color: #FFFFFF;">';
        $_message .= ' Ask Question Staff will follow-up with you as soon as possible.<br>';
        $_message .= 'You can view this ticket`s progress online at (<a href="' . site_url('issues_page/issue_id/' . $id) . '" style="color:#337ab7">' . site_url() . '</a>)<br><br>';
        $_message .= 'Your Marketing Team<br>Ticketing System !!!  Support When you need IT!<br><br>';
        $_message .= '<hr><br><br>';
        $_message .= $data['message'];
        $_message .= '</td>';
        $_message .= '</tr>';
        $_message .= '</tbody>';
        $_message .= '</table>';
        $_message .= '</div>';
        $_message .= '<div style="text-align:center; padding:15px;font-size:12px;color#c3c3c3;margin:15px;">Marketing Ticketing System @ The Outsourced Accountant 2017</div>';
        $_message .= "</body>";
        $_message .= "</html>";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: <' . $_user[0]->email . '>' . "\r\n";
        return @mail($_email, 'We Hear You!!! (' . $this->issue_type($data['subject']) . ')', $_message, $headers);
    }

    function edit_dscrptn($data) {
        $massage = array('message' => $data['message']);
        $this->db->where('ID', $data['ID']);
        return $this->db->update('message', $massage);
    }

    function delete_comment($id) {
        $this->db->where('ID', $id);
        return $this->db->delete('comment');
    }

    function id_comment($id) {
        return $this->db->query("SELECT comment.ID,fname,lastname,comments,user_info.ID AS UID FROM comment INNER JOIN user_info ON user_info.ID = comment.user_id  WHERE comment.message_id='" . $id . "' ");
    }

    function ticket_comment($value) {
        $_data = $this->ticket_issue_id($value['message_id']);
        $_data = $_data->result();
        $user = $this->user_id($_data[0]->sender);
        $_user = $user->result();
        $_email = '';
        $email = $this->support_email();
        foreach ($email->result() as $supports) {
            $_email .= $supports->email . ',';
        }
        $_email = $_email . $_user[0]->email;
        $_message = "";
        $_message .= "<html>";
        $_message .= "<head>";
        $_message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>";
        $_message .= "<title>The Outsourced Accountant</title>";
        $_message .= "</head>";
        $_message .= '<div style="text-align:center;"><img src="' . site_url('images/final-logo.png') . '" alt="logo" align="center"></div>';
        $_message .= "<body bgcolor='#efefef' style='font-family: sans-serif;font-size:14px;color: #8c8c8c;'>";
        $_message .= '<div bgcolor="#FFFFFF" style="padding: 15px;background-color: #FFFFFF; max-width:900px; margin: 0 auto 0 auto; text-align:center;">';
        $_message .= '<table align="center" style="background-color:#FFF;width:100%;max-width:900px; border:1px solid #DDD;border-spacing: 0px;border-collapse: collapse; font-family:sans-serif;color: #8c8c8c;font-size: 14px;text-align:left;">';
        $_message .= '<tbody>' . "\n";
        $_message .= '<tr>' . "\n";
        $_message .= '<td style="padding:15px;"><img src="' . site_url('images/TOA logo.png') . '" alt="logo"></td>' . "\n";
        $_message .= '</tr>' . "\n";
        $_message .= '<tr>' . "\n";
        $_message .= '<td style="padding:5px;border: 1px solid #DDD;background-color:#FFFFFF;">Comment from: ' . $_user[0]->fname . ' ' . $_user[0]->lastname . '<br>' . "\n";
        $_message .= 'Location: ' . $_user[0]->location . '<br>' . "\n";
        $_message .= 'Ticket Link: <a href="' . site_url(preg_replace('/\s+/', '', 'issues_page/issue_id/') . $value['message_id']) . '" style="color:#337ab7"><i style="color:#337ab7">Click Here</i></a><br>' . "\n";
        $_message .= '</td>' . "\n";
        $_message .= '</tr>' . "\n";
        $_message .= '<tr>' . "\n";
        $_message .= '<td  style="padding: 15px;border-top: 1px solid #DDD;background-color: #FFFFFF;">';
        $_message .= $value['comments'];
        $_message .= '</td>';
        $_message .= '</tr>';
        $_message .= '</tbody>';
        $_message .= '</table>';
        $_message .= '</div>';
        $_message .= '<div style="text-align:center; padding:15px;font-size:12px;color#c3c3c3;margin:15px;">Marketing Ticketing System @ The Outsourced Accountant 2017</div>';
        $_message .= "</body>";
        $_message .= "</html>";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: <' . $_user[0]->email . '>' . "\r\n";
        @mail($_email, 'Comment from ticket ID#' . $value['message_id'], $_message, $headers);
        return $this->db->insert('comment', $value);
    }

    function mail_cc($data) {
        $user = $this->user_id($this->session->userdata('user_session'));
        $_user = $user->result();


        $_data = $this->Models->ticket_issue_id($data['cc_issue_id']);
        $_result = $_data->result();

        $user_cc = $this->user_id($data['cc_user_id']);
        $ruser = $user_cc->result();


        $_message = "<html>";
        $_message .= "<head>";
        $_message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>";
        $_message .= "<title>The Outsourced Accountant</title>";
        $_message .= "</head>";
        $_message .= "<body bgcolor='#efefef' style='font-family: sans-serif;font-size:14px;color: #8c8c8c;'>";
        $_message .= '<div style="text-align:center;"><img src="' . site_url('images/final-logo.png') . '" alt="logo" align="center"></div>';
        $_message .= '<div bgcolor="#FFFFFF" style="padding: 15px;background-color: #FFFFFF; max-width:900px; margin: 0 auto 0 auto; text-align:center;">';
        $_message .= '<table style="background-color:#FFF;width:100%;max-width:900px; border:1px solid #DDD;border-spacing: 0px;border-collapse: collapse; font-family:sans-serif;color: #8c8c8c;font-size: 14px;text-align:left;">';
        $_message .= '<tbody>';
        $_message .= '<tr>';
        $_message .= '<td style="padding:15px;"><img src="' . site_url('images/TOA logo.png') . '" alt="logo"></td>';
        $_message .= '</tr>';
        $_message .= '<tr>';
        $_message .= '<td style="padding: 15px;border: 1px solid #DDD;background-color: #FFFFFF;">From: ' . $_user[0]->fname . ' ' . $_user[0]->lastname . "<br/>\n";
        $_message .= 'Location: ' . $_user[0]->location . "<br/>";
        $_message .= 'Subject: ' . $this->issue_type($_result[0]->subject) . "<br/>\n";
        $_message .= 'Ticket Link: <a href="' . site_url('issues_page/issue_id/' . $data['cc_issue_id']) . '" style="color:#337ab7">Click Here</a>';
        $_message .= '</td>';
        $_message .= '</tr>';
        $_message .= '<tr>';
        $_message .= '<td  style="padding: 15px;border-top: 1px solid #DDD;background-color: #FFFFFF;">';
        $_message .= 'A Marketing Staff will follow-up with you as soon as possible.<br>';
        $_message .= 'You can view this ticket`s progress online at (<a href="' . site_url('issues_page/issue_id/' . $data['cc_issue_id']) . '" style="color:#337ab7">' . site_url() . '</a>)<br><br>';
        $_message .= 'Your Marketing Team<br>Ticketing System !!!  Support When you need IT!<br><br>';
        $_message .= '<hr><br><br>';
        $_message .= $_result[0]->message;
        $_message .= '</td>';
        $_message .= '</tr>';
        $_message .= '</tbody>';
        $_message .= '</table>';
        $_message .= '</div>';
        $_message .= '<div style="text-align:center; padding:15px;font-size:12px;color#c3c3c3;margin:15px;">Marketing Ticketing System @ The Outsourced Accountant 2017</div>';
        $_message .= "</body>";
        $_message .= "</html>";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: <' . $_user[0]->email . '>' . "\r\n";
        return @mail($ruser[0]->email, 'We Hear You!!! (' . $this->issue_type($_result[0]->subject) . ')', $_message, $headers);
    }

    function cc_comment($value) {
        $_data = $this->ticket_issue_id($value['message_id']);
        $_data = $_data->result();


        $user = $this->user_id($_data[0]->sender);
        $_user = $user->result();



        $_email = '';
        $email = $this->cc_comment_id($value['message_id']);
        foreach ($email->result() as $supports) {
            $_email .= $supports->email . ',';
        }
        $_email = rtrim($_email, ",");

        $_message = "";
        $_message .= "<html>";
        $_message .= "<head>";
        $_message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>";
        $_message .= "<title>The Outsourced Accountant</title>";
        $_message .= "</head>";
        $_message .= '<div style="text-align:center;"><img src="' . site_url('images/final-logo.png') . '" alt="logo" align="center"></div>';
        $_message .= "<body bgcolor='#efefef' style='font-family: sans-serif;font-size:14px;color: #8c8c8c;'>";
        $_message .= '<div bgcolor="#FFFFFF" style="padding: 15px;background-color: #FFFFFF; max-width:900px; margin: 0 auto 0 auto; text-align:center;">';
        $_message .= '<table align="center" style="background-color:#FFF;width:100%;max-width:900px; border:1px solid #DDD;border-spacing: 0px;border-collapse: collapse; font-family:sans-serif;color: #8c8c8c;font-size: 14px;text-align:left;">';
        $_message .= '<tbody>' . "\n";
        $_message .= '<tr>' . "\n";
        $_message .= '<td style="padding:15px;"><img src="' . site_url('images/TOA logo.png') . '" alt="logo"></td>' . "\n";
        $_message .= '</tr>' . "\n";
        $_message .= '<tr>' . "\n";
        $_message .= '<td style="padding:5px;border: 1px solid #DDD;background-color:#FFFFFF;">Comment from: ' . $_user[0]->fname . ' ' . $_user[0]->lastname . '<br>' . "\n";
        $_message .= 'Location: ' . $_user[0]->location . '<br>' . "\n";
        $_message .= 'Ticket Link: <a href="' . site_url(preg_replace('/\s+/', '', 'issues_page/issue_id/') . $value['message_id']) . '" style="color:#337ab7"><i style="color:#337ab7">Click Here</i></a><br>' . "\n";
        $_message .= '</td>' . "\n";
        $_message .= '</tr>' . "\n";
        $_message .= '<tr>' . "\n";
        $_message .= '<td  style="padding: 15px;border-top: 1px solid #DDD;background-color: #FFFFFF;">';
        $_message .= $value['comments'];
        $_message .= '</td>';
        $_message .= '</tr>';
        $_message .= '</tbody>';
        $_message .= '</table>';
        $_message .= '</div>';
        $_message .= '<div style="text-align:center; padding:15px;font-size:12px;color#c3c3c3;margin:15px;">Marketing Ticketing System @ The Outsourced Accountant 2017</div>';
        $_message .= "</body>";
        $_message .= "</html>";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: <' . $_user[0]->email . '>' . "\r\n";
        @mail($_email, 'Comment from ticket ID#' . $value['message_id'], $_message, $headers);
        return $this->db->insert('comment', $value);
    }

    function duplicate_cc($data) {
        return $this->db->query("SELECT * FROM `cc_table`  WHERE `cc_user_id`='" . $data["cc_user_id"] . "' AND `cc_issue_id`='" . $data["cc_issue_id"] . "' ");
    }

    function add_new_cc($value) {
        return $this->db->insert('cc_table', $value);
    }

    function cc_comment_id($data) {
        return $this->db->query("SELECT * FROM `cc_table`  WHERE `cc_issue_id`='" . $data["cc_issue_id"] . "' ");
    }

    function cc_ajax($issueid) {
        return $this->db->query("SELECT *,cc_table.ID AS GETID FROM `cc_table` INNER JOIN user_info ON user_info.ID = cc_table.cc_user_id WHERE cc_table.cc_issue_id='" . $issueid . "' ");
    }

    function delete_cc($id) {
        return $this->db->query("DELETE FROM `cc_table` WHERE `ID`='$id' ");
    }

    function add_new_assignee($value) {
        return $this->db->insert('assignee', $value);
    }

    function duplicate_assignee($data) {
        return $this->db->query("SELECT * FROM `assignee`  WHERE `issueid`='" . $data["issueid"] . "' AND `assignee`='" . $data["assignee"] . "' ");
    }

    function popup_assignee($issueid) {
        return $this->db->query("SELECT * FROM `assignee` INNER JOIN user_info ON user_info.ID = assignee.assignee WHERE issueid='" . $issueid . "' ");
    }

    function check_assignee($issueid) {
        return $this->db->query("SELECT * FROM `assignee` WHERE issueid='" . $issueid . "' ");
    }

    function delete_assignee($id) {
        return $this->db->query("DELETE FROM `assignee` WHERE `ID`='$id' ");
    }

    function ticket_attached($id) {
        return $this->db->query("SELECT * FROM `files` WHERE `key_id`='$id' ");
    }

    function ticket_assignee($id) {
        return $this->db->query("SELECT *,assignee.ID AS GETID   FROM `assignee` INNER JOIN user_info ON user_info.ID = assignee.assignee WHERE assignee.issueid='" . $id . "' ");
    }

    function ticket_update_subject($data) {
        
        if($data['ID'] != '5'){
             $status = array('subject' => $data['value'], 'sub_category' => '');
        }else{
             $status = array('subject' => $data['value'], );
        }
        
      
        $this->db->where('ID', $data['ID']);
        return $this->db->update('message', $status);
    }
    function ticket_update_subcategory($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET sub_category='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_requestby($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET requestby='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_target($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET target='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_title($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET title='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_purpose($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET purpose='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_link($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET link='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_requestor($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET requestor='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_approvedby($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET approvedby='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function update_date_required($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET date_required='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function update_content($valone, $valtwo) {
        
          return $this->db->query("UPDATE  `message` SET content='".$valtwo."'  WHERE ID = '" . $valone . "' ");
    }
    function ticket_update_status($data) {
        $status = array('status' => $data['status'], 'date_solve' => strtotime('now'));
        $this->db->where('ID', $data['ID']);
        $user = $this->user_id($this->session->userdata('user_session'));
        $_user = $user->result();
        $_assn = $this->ticket_assignee($data['ID']);
        $_email = '';
        foreach ($_assn->result() as $_it) {
            $_email .= $_it->email . ',';
        }
        $_message = "";
        $_message .= "<html>";
        $_message .= "<head>";
        $_message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>";
        $_message .= "<title>The Outsourced Accountant</title>";
        $_message .= "</head>";
        $_message .= "<body bgcolor='#efefef' style='font-family: sans-serif;font-size:14px;color: #8c8c8c;'>";
        $_message .= '<div style="text-align:center;"><img src="' . site_url('images/final-logo.png') . '" alt="logo" align="center"></div>';
        $_message .= '<div bgcolor="#FFFFFF" style="padding: 15px;background-color: #FFFFFF; max-width:900px; margin: 0 auto 0 auto; text-align:center;">';
        $_message .= '<table align="center" style="background-color:#FFF;width:100%;max-width:900px; border:1px solid #DDD;border-spacing: 0px;border-collapse: collapse; font-family:sans-serif;color: #8c8c8c;font-size: 14px;text-align:left;">';
        $_message .= '<tbody>';
        $_message .= '<tr>';
        $_message .= '<td style="padding:5px;"><img src="' . site_url('images/TOA logo.png') . '" alt="logo"></td>';
        $_message .= '</tr>';
        $_message .= '<tr>';
        $_message .= '<td style="padding:5px;border: 1px solid #DDD;background-color:#FFFFFF;">Status update by: ' . $_user[0]->fname . ' ' . $_user[0]->lastname . "<br>\n";
        $_message .= 'Location: ' . $_user[0]->location . "<br>\n";
        $_message .= 'Ticket Link: <a href="' . site_url(preg_replace('/\s+/', '', 'issues_page/issue_id/') . $data['ID']) . '" style="color:#337ab7"><i style="color:#337ab7">Click Here</i></a>' . "<br>\n";
        $_message .= '</td>' . "\n";
        $_message .= '</tr>' . "\n";
        $_message .= '<tr>' . "\n";
        $_message .= '<td  style="padding: 15px;border-top: 1px solid #DDD;background-color: #FFFFFF;">' . "\n";



        $_message .= 'Issue status: ' . $this->issue_status($data['status']);

        $_message .= '</td>';
        $_message .= '</tr>';
        $_message .= '</tbody>';
        $_message .= '</table>';
        $_message .= '</div>';
        $_message .= '<div style="text-align:center; padding:15px;font-size:12px;color#c3c3c3;margin:15px;">IT Ticketing System @ The Outsourced Accountant 2017</div>';
        $_message .= "</body>";
        $_message .= "</html>";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: <' . $_user[0]->email . '>' . "\r\n";
        @mail($_email . $data['email'], 'Ticket status: ' . $this->issue_string($data['status']), $_message, $headers);
        return $this->db->update('message', $status);
    }

    function ticket_issue_id($id) {
        return $this->db->query("SELECT * FROM `message` INNER JOIN user_info ON user_info.ID = message.sender  WHERE message.ID = " . $id);
    }

    function create_new_ticket($insert) {
        return $this->db->insert('message', $insert);
    }

    function new_file($insert) {
        return $this->db->insert('files', $insert);
    }

    function all_ticket_status() {
        return $this->db->query("SELECT ID,key_id,date,status,sender FROM `message` WHERE status = '1' || status = '2' || status = '3' ORDER BY `ID` ASC");
    }

    function ticket_status($status) {
        $user_position = $this->session->userdata('user_position');
        $user_session = $this->session->userdata('user_session');
        if ($user_position === '') {
            return $this->db->query("SELECT * FROM `message` WHERE status = '" . $status . "' and sender='" . $user_session . "' ");
        } else {
            return $this->db->query("SELECT * FROM `message` WHERE status = '" . $status . "' ");
        }
    }

    function ticket_status_it($status) {
        $user_session = $this->session->userdata('user_session');
        $_query = " SELECT data.*, c.* FROM (SELECT a.*, b.email, b.position, b.fname, b.middle,b.lastname, b.location FROM assignee as a LEFT JOIN(SELECT * FROM user_info) as b ON a.assignee = b.ID ) as data LEFT JOIN(SELECT * FROM message) as c ON data.issueid = c.ID ";
        $_query .= "WHERE (c.status='" . $status . "') and (data.assignee='" . $user_session . "')";
        return $this->db->query($_query);
    }

    function time_range($unix_date, $now) {
        if (empty($unix_date)) {
            return "No date provided";
        }if (empty($now)) {
            return "No date provided";
        }
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        if (empty($unix_date)) {
            return "Bad date";
        }
        if ($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "ago";
        } else {
            $difference = $unix_date - $now;
            $tense = "from now";
        }
        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }
        $difference = round($difference);
        if ($difference != 1) {
            $periods[$j].= "s";
        }
        return "$difference $periods[$j] {$tense}";
    }

    function validateDate($date) {
        $date = str_replace('-', '/', $date);
        $d = DateTime::createFromFormat('m/d/Y', $date);
        return $d && $d->format('m/d/Y') === $date;
    }

    function ticket_assignee_user() {
        if ($this->uri->segment(5)) {
            $user_position = $this->uri->segment(5);
        } else {
            $user_position = $this->session->userdata('user_session');
        }
        return $this->db->query("SELECT * FROM `assignee` INNER JOIN message ON assignee.issueid = message.ID INNER JOIN user_info ON user_info.ID =  assignee.assignee  WHERE (assignee.assignee ='" . $user_position . "') and (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") ");
    }

    function ticket_assignee_total_count($subject) {
        if ($this->uri->segment(5)) {
            $user_position = $this->uri->segment(5);
        } else {
            $user_position = $this->session->userdata('user_session');
        }
        return $this->db->query("SELECT * FROM `assignee` INNER JOIN message ON assignee.issueid = message.ID  WHERE (assignee.assignee ='" . $user_position . "') and (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") and (message.subject='" . $subject . "') ");
    }

    function ticket_it_total_count($subject) {
        if (($this->uri->segment(3)) and ( $this->uri->segment(4))) {
            return $this->db->query("SELECT * FROM `message`  WHERE  (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") and (message.subject='" . $subject . "') ");
        } else {
            redirect(site_url());
        }
    }

    function ticket_assignee_count($subject, $status) {
        if ($this->uri->segment(5)) {
            $user_position = $this->uri->segment(5);
        } else {
            $user_position = $this->session->userdata('user_session');
        }
        return $this->db->query("SELECT * FROM `assignee` INNER JOIN message ON assignee.issueid = message.ID  WHERE (assignee.assignee ='" . $user_position . "') and (assignee.assignee ='" . $user_position . "') and (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") and (message.subject='" . $subject . "' and message.status='" . $status . "') ");
    }

    function ticket_all_user() {
        return $this->db->query("SELECT * FROM `message`  WHERE (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ")  ");
        //return $this->db->query("SELECT * FROM `assignee` INNER JOIN message ON assignee.issueid = message.ID INNER JOIN user_info ON user_info.ID =  assignee.assignee  WHERE (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") ");
    }

    function ticket_all_total_count($subject) {
        return $this->db->query("SELECT * FROM `assignee` INNER JOIN message ON assignee.issueid = message.ID  WHERE (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") and (message.subject='" . $subject . "') ");
    }

    function ticket_all_count($subject, $status) {
        return $this->db->query("SELECT * FROM `message` WHERE (date BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") and (subject='" . $subject . "' and status='" . $status . "') ");
    }
    function dash_all_count($year,$month,$subject, $status) {
        return $this->db->query("SELECT * FROM `message` WHERE (date BETWEEN " . strtotime(date('m/01/Y',strtotime($month.'/01/'.$year.' 12:00 AM'))) . " and " . strtotime(date('m/t/Y',strtotime($month.'/01/'.$year . ' 11:59 PM'))). ") and (subject='" . $subject . "' and status='" . $status . "') ");
    }
    function dash_total_count($year,$month,$subject) {
        if ($year) {
            return $this->db->query("SELECT * FROM `message`  WHERE  (date BETWEEN " . strtotime(date('m/01/Y',strtotime($month.'/01/'.$year.' 12:00 AM'))) . " and " . strtotime(date('m/t/Y',strtotime($month.'/01/'.$year . ' 11:59 PM'))) . ") and (message.subject='" . $subject . "') ");
        } else {
            redirect(site_url());
        }
    }
    function all_tickets() {
        return $this->db->query("SELECT * FROM `message` ORDER BY `ID` DESC ");
    }

    function latest_ticket($id) {
        return $this->db->query("SELECT * FROM `message` WHERE sender = '" . $id . "'  ORDER BY `ID` DESC ");
    }

    function latest_guest_ticket($id) {
        return $this->db->query("SELECT * FROM `message` WHERE key_id = '" . $id . "'  ORDER BY `ID` DESC ");
    }

    function ticket_status_support() {
        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "c.message LIKE '%" . $field . "%'";
                $conditions[] = "c.key_id LIKE '%" . $field . "%'";
                $conditions[] = "c.date LIKE '%" . $field . "%'";
                $conditions[] = "data.fname LIKE '%" . $field . "%'";
                $conditions[] = "data.lastname LIKE '%" . $field . "%'";
            }
            if ($this->lctn()) {
                $conditions[] = "data.location LIKE '%" . $this->lctn() . "%'";
            }
        }
        $_query = " SELECT data.*, c.* FROM (SELECT a.*, b.email, b.position, b.fname, b.middle,b.lastname, b.location FROM assignee as a LEFT JOIN(SELECT * FROM user_info) as b ON a.assignee = b.ID ) as data LEFT JOIN(SELECT * FROM message) as c ON data.issueid = c.ID ";
        $user_session = $this->session->userdata('user_session');
        if (count($conditions) > 0) {
            $_query .= "WHERE (c.status='" . $this->status() . "') and (" . implode(' || ', $conditions) . ')';
        } else {
            $_query .= "WHERE (c.status='" . $this->status() . "') and (data.assignee='" . $user_session . "')";
        }

        return $this->db->query($_query . " ORDER BY c.ID DESC LIMIT $pageLimit , $setLimit");
    }

    function ticket_status_support_pagination() {
        $page_url = base_url($this->uri->segment(1) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "c.message LIKE '%" . $field . "%'";
                $conditions[] = "c.key_id LIKE '%" . $field . "%'";
                $conditions[] = "c.date LIKE '%" . $field . "%'";
                $conditions[] = "data.fname LIKE '%" . $field . "%'";
                $conditions[] = "data.lastname LIKE '%" . $field . "%'";
            }
            if ($this->lctn()) {
                $conditions[] = "data.location LIKE '%" . $this->lctn() . "%'";
            }
        }
        $_query = " SELECT data.*, c.* FROM (SELECT a.*, b.email, b.position, b.fname, b.middle,b.lastname, b.location FROM assignee as a LEFT JOIN(SELECT * FROM user_info) as b ON a.assignee = b.ID ) as data LEFT JOIN(SELECT * FROM message) as c ON data.issueid = c.ID ";
        $user_session = $this->session->userdata('user_session');
        if (count($conditions) > 0) {
            $_query .= "WHERE (c.status='" . $this->status() . "') and (" . implode(' || ', $conditions) . ')';
        } else {
            $_query .= "WHERE (c.status='" . $this->status() . "') and (data.assignee='" . $user_session . "')";
        }
        $query = $this->db->query($_query . " ORDER BY `c`.`ID` DESC");
        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate.= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "?location=" . $this->lctn() . "&status=" . $this->status() . "'>Next</a></li>";
                $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>Last</a></li>";
            } else {
                $setPaginate.= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate.= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate.= "</ul>\n";
        }
        return $setPaginate;
    }

    function ticket_status_all() {
        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "a.message LIKE '%" . $field . "%'";
                $conditions[] = "a.key_id LIKE '%" . $field . "%'";
                $conditions[] = "a.date LIKE '%" . $field . "%'";
                $conditions[] = "b.fname LIKE '%" . $field . "%'";
                $conditions[] = "b.lastname LIKE '%" . $field . "%'";
            }
            if ($this->lctn()) {
                $conditions[] = "b.location LIKE '%" . $this->lctn() . "%'";
            }
        }
        $_query = "SELECT a.* , b.fname, b.middle, b.lastname, b.position, b.email, b.location FROM `message` as a LEFT JOIN (SELECT * FROM user_info) as b ON a.sender = b.ID ";
        $user_position = $this->session->userdata('user_position');
        $user_session = $this->session->userdata('user_session');
        if ($user_position === '') {
            if (count($conditions) > 0) {
                $_query .= "WHERE (`status`='" . $this->status() . "' and b.ID = '" . $user_session . "') and (" . implode(' || ', $conditions) . ')';
            } else {
                $_query .= "WHERE (`status`='" . $this->status() . "' and b.ID = '" . $user_session . "')";
            }
        } else {
            if (count($conditions) > 0) {
                $_query .= "WHERE (`status`='" . $this->status() . "') and (" . implode(' || ', $conditions) . ')';
            } else {
                $_query .= "WHERE (`status`='" . $this->status() . "')";
            }
        }
        return $this->db->query($_query . " ORDER BY `ID` DESC LIMIT $pageLimit , $setLimit");
    }

    function ticket_status_pagination() {
        $page_url = base_url($this->uri->segment(1) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "a.message LIKE '%" . $field . "%'";
                $conditions[] = "a.key_id LIKE '%" . $field . "%'";
                $conditions[] = "a.date LIKE '%" . $field . "%'";
                $conditions[] = "b.fname LIKE '%" . $field . "%'";
                $conditions[] = "b.lastname LIKE '%" . $field . "%'";
            }
            if ($this->lctn()) {
                $conditions[] = "b.location LIKE '%" . $this->lctn() . "%'";
            }
        }
        $_query = "SELECT a.* , b.fname, b.middle, b.lastname, b.position, b.email, b.location FROM `message` as a LEFT JOIN (SELECT * FROM user_info) as b ON a.sender = b.ID ";
        $user_position = $this->session->userdata('user_position');
        $user_session = $this->session->userdata('user_session');
        if ($user_position === '') {
            if (count($conditions) > 0) {
                $_query .= "WHERE (`status`='" . $this->status() . "' and a.ID = '" . $user_session . "') and (" . implode(' || ', $conditions) . ')';
            } else {
                $_query .= "WHERE (`status`='" . $this->status() . "' and a.ID = '" . $user_session . "')";
            }
        } else {
            if (count($conditions) > 0) {
                $_query .= "WHERE (`status`='" . $this->status() . "') and (" . implode(' || ', $conditions) . ')';
            } else {
                $_query .= "WHERE (`status`='" . $this->status() . "')";
            }
        }
        $query = $this->db->query($_query . " ORDER BY `ID` DESC");
        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate.= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "?location=" . $this->lctn() . "&status=" . $this->status() . "'>Next</a></li>";
                $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>Last</a></li>";
            } else {
                $setPaginate.= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate.= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate.= "</ul>\n";
        }
        return $setPaginate;
    }

    function chat_insert($data) {
        return $this->db->insert('message_support', $data);
    }

    function user_calendar($id) {
        return $this->db->query("SELECT * FROM `assignee` INNER JOIN message ON assignee.issueid = message.ID WHERE assignee.assignee='" . $id . "' ");
    }

    function chat_support($data) {
        $setLimit = 10;
        $pageLimit = (($data['page'] === '' ? 1 : $data['page']) * $setLimit) - ($setLimit);
        return $this->db->query("SELECT * FROM message_support WHERE (from_id='" . $this->session->userdata('user_session') . "' AND to_id='" . $data['chat_id'] . "') || (to_id='" . $this->session->userdata('user_session') . "' AND from_id='" . $data['chat_id'] . "') ORDER BY ID ASC ");
    }

    function chat_update_read($id) {
        $this->db->where('ID', $id);
        return $this->db->update("message_support", array('read' => '1'));
    }

    function my_chat_support() {
        return $this->db->query("SELECT * FROM message_support WHERE to_id='" . $this->session->userdata('user_session') . "' ");
    }

    function online_support_and_user($data) {
        $srh = explode(" ", $this->quotes(htmlspecialchars($data)));
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "fname LIKE '%" . $field . "%'";
                $conditions[] = "lastname LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT *,if(`online`> " . strtotime('now') . ",if(`online`> " . strtotime(date('Y-m-d H:i:s', strtotime("+15 minutes"))) . ",'ONLINE', 'ON_IDLE'), 'OFFLINE') as STATUS FROM `user_info`";
        if (count($conditions) > 0) {
            $_query .= "WHERE (" . implode(' || ', $conditions) . ')';
        } else {
            
        }
        if ($this->session->userdata('user_position') === '1' || $this->session->userdata('user_position') === '0') {
            return $this->db->query($_query . " ORDER BY online,last_login");
        } else {
            return $this->db->query($_query . " AND (position = '1' || position = '0') ORDER BY online");
        }
    }

    function test_online() {

        if ($this->session->userdata('user_position') === '1' || $this->session->userdata('user_position') === '0') {

            $_query = "SELECT distinct(from_id), SORT, not_read, STATUS, ID, email, `position`, fname, middle,lastname,
job_title, location,online, last_login,availability
FROM(SELECT my_data.*, if(`online`> " . strtotime('now') . ",if(`online`> " . strtotime(date('Y-m-d H:i:s', strtotime("+15 minutes"))) . ",'ONLINE', 'ON_IDLE'), 'OFFLINE') as STATUS,  b.*  FROM 
(

SELECT  'a' as SORT, count(`read`) as not_read, from_id 
FROM message_support where to_id='" . $this->session->userdata('user_session') . "' and `read`='0'
GROUP BY from_id

UNION
SELECT 'b' as SORT,0 as not_read, ID as from_id FROM user_info
where online >0 and
ID not in('" . $this->session->userdata('user_session') . "')

UNION
SELECT   'c' as SORT, 0 as not_read,ID as from_id  FROM user_info where ID NOT IN('" . $this->session->userdata('user_session') . "') and online='0'
GROUP BY ID 

ORDER BY   SORT ASC LIMIT 1,40)  as my_data
LEFT JOIN(SELECT  ID, email, position, fname, middle,lastname, job_title, location, online, last_login,availability FROM user_info) as b 
ON my_data.from_id = b.ID GROUP BY ID) as all_informtion ORDER BY SORT, online  DESC ";
            return $this->db->query($_query);
        } else {
            return $this->db->query("SELECT *,if(`online`> " . strtotime('now') . ",if(`online`> " . strtotime(date('Y-m-d H:i:s', strtotime("+15 minutes"))) . ",'ONLINE', 'ON_IDLE'), 'OFFLINE') as STATUS FROM `user_info` WHERE position = '1' || position = '0' ORDER BY STATUS DESC");
        }
    }

    function create_transfer($data){
         return $this->db->insert('transfer_list', $data);
    }
    function created_transfer(){
         return $this->db->query("SELECT * FROM `transfer_list` ORDER BY `IDT` DESC");
    }
    
    function created_transfer_id($data){
         return $this->db->query("SELECT * FROM `transfer_list` WHERE IDT = ".$data." ORDER BY `IDT` DESC");
    }
    
    function transfer_status(){
        if (!$this->input->get('status')) {
            return '';
        } else if (is_numeric($this->input->get('status'))) {
            return ltrim(rtrim($this->input->get('status')));
        } else {
            return '';
        }
    }
    
    
    function transfer_all_date(){
        $_query = "SELECT * FROM `transfer_list` WHERE (transfer_start BETWEEN " . strtotime(str_replace('-', '/', $this->uri->segment(3)) . ' 12:00 AM') . " and " . strtotime(str_replace('-', '/', $this->uri->segment(4)) . ' 11:59 PM') . ") ";
        return $this->db->query($_query);
    }
    
    function transfer_all() {
        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "client_support LIKE '%" . $field . "%'";
                $conditions[] = "client_name LIKE '%" . $field . "%'";
                $conditions[] = "transfer_from LIKE '%" . $field . "%'";
                $conditions[] = "transfer_to LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT * FROM `transfer_list`";
        if (count($conditions) > 0) {
            $_query .= " WHERE (" . implode(' || ', $conditions) . ') '.($this->transfer_status() == '' ? '' : ' and client_support = "'.$this->transfer_status().'"') ;
        }else{
            $_query .= ($this->transfer_status() == '' ? '' : ' WHERE client_support = "'.$this->transfer_status().'"') ;
        }
        
        return $this->db->query($_query . " ORDER BY `IDT` DESC LIMIT $pageLimit , $setLimit");
    }

    function transfer_pagination() {
        $page_url = base_url($this->uri->segment(1) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "client_support LIKE '%" . $field . "%'";
                $conditions[] = "client_name LIKE '%" . $field . "%'";
                $conditions[] = "transfer_from LIKE '%" . $field . "%'";
                $conditions[] = "transfer_to LIKE '%" . $field . "%'";
                $conditions[] = "transfer_involve LIKE '%" . $field . "%'";
            }

        }
        $_query = "SELECT * FROM `transfer_list`";
        
        if (count($conditions) > 0) {
            $_query .= "WHERE (" . implode(' || ', $conditions) . ')';
        } 
        
        $query = $this->db->query($_query . " ORDER BY `IDT` DESC");
        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate.= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate.= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>2</a></li>";
                    $setPaginate.= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate.= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate.= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "?location=" . $this->lctn() . "&status=" . $this->status() . "'>Next</a></li>";
                $setPaginate.= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "&location=" . $this->lctn() . "&status=" . $this->status() . "'>Last</a></li>";
            } else {
                $setPaginate.= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate.= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate.= "</ul>\n";
        }
        return $setPaginate;
    }
    function transfer_assignee($id){
        return $this->db->query("SELECT fname,lastname,transfer_it.ID AS ITID,user_info.ID AS UID FROM `transfer_it` INNER JOIN user_info ON transfer_it.transferuserit = user_info.id WHERE transfer_it.transferid = $id");
    }
    function update_transfer_($data,$id) {
        $this->db->where('IDT', $id);
        return $this->db->update('transfer_list', $data);
    }
    function check_transfer_assignee($data) {
        return $this->db->query('SELECT * FROM transfer_it WHERE transferid="'.$data['transferid'].'" AND transferuserit="'.$data['transferuserit'].'" ');
    }

    function new_transfer_assignee($data) {
        
        return $this->db->insert('transfer_it', $data);
        
    }
    
    function delete_trans_assignee($id) {
        $this->db->where('ID', $id);
        $this->db->delete('transfer_it');
        
    }
    function AddPlayTime($times) {
        $minutes = 0;
        $hours = 0;
        $days = 0;
        foreach ($times as $time) {
            list($day, $hour, $minute) = explode(':', $time);
            $days +=$day;
            
            $minutes += $hour * 60;
            $minutes += $minute;
        }
      
        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;
        return sprintf('%02d:%02d:%02d', $days, $hours, $minutes);
    }

    function delete_add_pc($data) {
        $this->db->where('IDL',$data);
        $this->db->delete('transfer_pc_list');
    }
    
    function show_add_pc($data) {
        return $this->db->query('SELECT * FROM transfer_pc_list WHERE transfer_id="' . $data . '" ');
    }

    function add_pc_name($data){
          return $this->db->insert('transfer_pc_list', $data);
    }

    
    function check_ratings_issues($data) {
        return $this->db->query('SELECT * FROM ratings WHERE issue_id="' . $data . '" ');
    }

    function ratings_issues($data){
          return $this->db->insert('ratings', $data);
    }

    function ratings_update($data, $id) {
        $this->db->where('issue_id', $id);
        return $this->db->update('ratings', $data);
    }
    function ratings_query_all() {
        return $this->db->query('SELECT * FROM ratings');
    }
    function ratings_query($data) {
        return $this->db->query('SELECT * FROM ratings WHERE rate_number="' . $data . '" ');
    }

}
