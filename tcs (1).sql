-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 04:50 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tcs`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignee`
--

CREATE TABLE `assignee` (
  `ID` int(11) NOT NULL,
  `issueid` varchar(200) NOT NULL,
  `assignee` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignee`
--

INSERT INTO `assignee` (`ID`, `issueid`, `assignee`) VALUES
(1, '41', '295'),
(2, '44', '4'),
(3, '43', '298'),
(4, '42', '298'),
(5, '30', '298'),
(6, '48', '867'),
(7, '49', '310'),
(8, '51', '310'),
(9, '1', '298'),
(10, '15168', '4'),
(11, '15180', '241'),
(12, '15181', '241');

-- --------------------------------------------------------

--
-- Table structure for table `cc_table`
--

CREATE TABLE `cc_table` (
  `ID` int(11) NOT NULL,
  `cc_user_id` int(11) NOT NULL,
  `cc_issue_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `ID` int(11) NOT NULL,
  `message_id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`ID`, `message_id`, `user_id`, `comments`) VALUES
(1, '1', '295', '<p>in complete new revision</p>\r\n'),
(2, '15180', '241', '<p>Need tlksmalkdlsa</p>\r\n'),
(3, '15180', '984', '<p>sdas</p>\r\n'),
(4, '15181', '241', '<p>test</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `ID` int(11) NOT NULL,
  `key_id` varchar(100) NOT NULL,
  `filename` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`ID`, `key_id`, `filename`) VALUES
(1, '150839475774', '150839475774_download.pdf'),
(2, '151694515498', '151694515498_contact-us-banner.jpg'),
(3, '151695018546', '151695018546_contact-us-banner.jpg'),
(4, '151859036193', '151859036193_all_star_final.png'),
(5, '151859135686', '151859135686_all_star_final.png'),
(6, '151859144559', '151859144559_1.png'),
(7, '151859460177', '151859460177_20161026_053146.jpg'),
(8, '151859475183', '151859475183_2x2.png'),
(9, '151859494193', '151859494193_2x2.png'),
(10, '151859498028', '151859498028_head.jpg'),
(11, '151859502067', '151859502067_head.jpg'),
(12, '151859510743', '151859510743_all_star_final.png'),
(13, '151859520366', '151859520366_head.jpg'),
(14, '151864857520', '151864857520_head.jpg'),
(15, '151864866915', '151864866915_2x2.png'),
(16, '151864875027', '151864875027_2x2.png'),
(17, '151864902333', '151864902333_2x2.png'),
(18, '151864906482', '151864906482_head.jpg'),
(19, '151864924143', '151864924143_head.jpg'),
(20, '151864934857', '151864934857_2x2.png'),
(21, '151864962028', '151864962028_1.png'),
(22, '151864967177', '151864967177_head.jpg'),
(23, '151864974740', '151864974740_2x2.png'),
(24, '151864977386', '151864977386_head.jpg'),
(25, '151864980138', '151864980138_head.jpg'),
(26, '151865019758', '151865019758_head.jpg'),
(27, '151865027167', '151865027167_head.jpg'),
(28, '151917027310', '151917027310_jojo.png'),
(29, '152480860391', '152480860391_yoast.jpg'),
(30, '152481154123', '152481154123_wallpaper.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `ID` int(11) NOT NULL,
  `priority` varchar(200) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `message` text NOT NULL,
  `key_id` varchar(100) NOT NULL,
  `sender` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_solve` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `sub_category` varchar(100) DEFAULT NULL,
  `sub_sub_category` varchar(70) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `purpose` text,
  `date_required` varchar(20) DEFAULT NULL,
  `link` text,
  `content` text,
  `requestby` text,
  `target` text,
  `approvedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`ID`, `priority`, `subject`, `message`, `key_id`, `sender`, `date`, `date_solve`, `status`, `sub_category`, `sub_sub_category`, `title`, `purpose`, `date_required`, `link`, `content`, `requestby`, `target`, `approvedby`) VALUES
(15181, '2', '18', '<p>TEST</p>\r\n', '152481154123', '984', '1524811541', '1524811602', '3', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15182, '3', '18', '<p>Test</p>\r\n', '152481192451', '985', '1524811924', '', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15183, '1', '14', '<p>test</p>\r\n', '152481312223', '986', '1524813122', '', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message_it`
--

CREATE TABLE `message_it` (
  `ID` int(11) NOT NULL,
  `priority` varchar(200) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `message` text NOT NULL,
  `key_id` varchar(100) NOT NULL,
  `sender` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `date_solve` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_support`
--

CREATE TABLE `message_support` (
  `ID` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `chat_message` text NOT NULL,
  `read` int(11) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_support`
--

INSERT INTO `message_support` (`ID`, `from_id`, `to_id`, `chat_message`, `read`, `date`) VALUES
(1, 984, 241, 'hello ', 1, 1524808775),
(2, 984, 241, 'how are you', 1, 1524808799),
(3, 241, 984, 'sadas', 1, 1524808818);

-- --------------------------------------------------------

--
-- Table structure for table `patient_record_tbl`
--

CREATE TABLE `patient_record_tbl` (
  `ID` int(11) NOT NULL,
  `patient_name` text CHARACTER SET latin1,
  `assisted_id` text CHARACTER SET latin1,
  `date_stamp` text CHARACTER SET latin1,
  `day` text CHARACTER SET latin1,
  `month` text CHARACTER SET latin1,
  `year` text CHARACTER SET latin1,
  `description` text CHARACTER SET latin1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_record_tbl`
--

INSERT INTO `patient_record_tbl` (`ID`, `patient_name`, `assisted_id`, `date_stamp`, `day`, `month`, `year`, `description`) VALUES
(1, 'Neigh Figueroa', 'Lawrence Guiao', '01/18/18 10:54:33', '18', '01', '2018', '<p>pk to remind</p>\r\n'),
(4, 'Sarrah Eden Altes', 'Neigh Figueroa', '01/18/18 10:55:05', '18', '01', '2018', '<p>hey dude</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `IDR` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `rate_number` int(11) NOT NULL,
  `rate_comment` text NOT NULL,
  `rate_date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`IDR`, `sender_id`, `issue_id`, `rate_number`, `rate_comment`, `rate_date`) VALUES
(0, 295, 1, 5, '5', 1516945248);

-- --------------------------------------------------------

--
-- Table structure for table `read_items`
--

CREATE TABLE `read_items` (
  `ID` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_it`
--

CREATE TABLE `transfer_it` (
  `ID` int(11) NOT NULL,
  `transferid` text NOT NULL,
  `transferuserit` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_list`
--

CREATE TABLE `transfer_list` (
  `IDT` int(11) NOT NULL,
  `client_support` text NOT NULL,
  `client_name` text NOT NULL,
  `transfer_from` text NOT NULL,
  `transfer_to` text NOT NULL,
  `transfer_start` text NOT NULL,
  `transfer_end` text NOT NULL,
  `transfer_involve` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_pc_list`
--

CREATE TABLE `transfer_pc_list` (
  `IDL` int(11) NOT NULL,
  `transfer_id` text NOT NULL,
  `user_pc_name` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `ID` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `middle` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `job_title` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `online` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `availability` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`ID`, `email`, `password`, `position`, `fname`, `middle`, `lastname`, `job_title`, `location`, `online`, `last_login`, `availability`) VALUES
(241, 'admin@dosupport.ph', 'b3360cc45c2819fc1ea9b0f16c15fdee', '1', 'Admin', 'A.', 'Support', '', 'Clark', 1525046748, 0, 0),
(984, 'mikky@depthindustries.com', 'b3360cc45c2819fc1ea9b0f16c15fdee', '', 'Mikky', ' ', 'Icmat', 'Developer', 'Australia', 1524813408, 0, 0),
(985, 'Test@yahoo.com', 'b3360cc45c2819fc1ea9b0f16c15fdee', '', 'Test', '', 'Test', 'Guest Ticket', 'Clark', 0, 0, 0),
(986, 'Test2@yahoo.com', 'b3360cc45c2819fc1ea9b0f16c15fdee', '', 'Test', '', 'Test', 'Guest Ticket', 'Clark', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignee`
--
ALTER TABLE `assignee`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cc_table`
--
ALTER TABLE `cc_table`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `message_support`
--
ALTER TABLE `message_support`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `patient_record_tbl`
--
ALTER TABLE `patient_record_tbl`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `read_items`
--
ALTER TABLE `read_items`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transfer_list`
--
ALTER TABLE `transfer_list`
  ADD PRIMARY KEY (`IDT`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignee`
--
ALTER TABLE `assignee`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `cc_table`
--
ALTER TABLE `cc_table`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15184;

--
-- AUTO_INCREMENT for table `message_support`
--
ALTER TABLE `message_support`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `patient_record_tbl`
--
ALTER TABLE `patient_record_tbl`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `read_items`
--
ALTER TABLE `read_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transfer_list`
--
ALTER TABLE `transfer_list`
  MODIFY `IDT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=987;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
