<?php
if (is_numeric($this->uri->segment(3))) {
    $this->uri->segment(3);
    $_data = $this->Models->ticket_issue_id($this->uri->segment(3));
    if ($_data->num_rows() === 0) {
        redirect(site_url());
    }
    $_data = $_data->result();
    $_assn = $this->Models->ticket_assignee($this->uri->segment(3));
    $user_position = $this->session->userdata('user_position');
} else {
    redirect(site_url());
}

$value_others = $_data[0]->sub_category;

$_SET = isset($_POST['otherfield']) ? $_POST['otherfield'] : $value_others;
?>
<div class="content">
    <div class="container-fluid">
        <div class="animated fadeIn widget grid6">
            <div class="widget-heading view-issues">VIEW TICKET</div>
            <div class="panel-heading">
              <div class="row">
                    <div class="col-sm-6 form-group">
                     <span style="font-weight:bold;">Job Order # 
                            <?php echo $this->uri->segment(3); ?>
                          </span>
                           
                    </div>
                    <div class="col-sm-6 form-group">
                      
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <form class="">
                            <div class="input-group  form-group-sm">
                                <span class="input-group-addon"><span>Project Title</span></span>
                                <input type="text" class="form-control input-inline update-title" name="titlefield" value="<?php echo $_data[0]->title ?>">
                        </div>

                        </form>
                    </div>
                    <div class="col-sm-6 form-group">
                        <div class="input-group  form-group-sm">
                                 <span class="input-group-addon" id="basic-addon3">Requesting Department </span>
                             <select name="requestby" class="form-control input-inline update-requestby">
                                <option value="Executive Team"  <?php if($_data[0]->requestby == "Executive"){echo "selected='selected'";}else{echo '';} ?>>Executive Team</option>
                                <option value="Client Success"  <?php if($_data[0]->requestby == "Client Success"){echo "selected='selected'";}else{echo '';} ?>>Client Success</option>
                                <option value="Human Resources" <?php if($_data[0]->requestby == "Human Resources"){echo "selected='selected'";}else{echo '';} ?>>Human Resources</option>
                                <option value="Recruitment" <?php if($_data[0]->requestby == "Recruitment"){echo "selected='selected'";}else{echo '';} ?>>Recruitment</option>
                                <option value="People Engagement" <?php if($_data[0]->requestby == "People Engagement"){echo "selected='selected'";}else{echo '';} ?>>People Engagement</option>
                                <option value="Training" <?php if($_data[0]->requestby == "Training"){echo "selected='selected'";}else{echo '';} ?>>Training</option>
                                <option value="IT" <?php if($_data[0]->requestby == "IT"){echo "selected='selected'";}else{echo '';} ?>>IT</option>
                                <option value="Admin and Facilities" <?php if($_data[0]->requestby == "Admin and Facilities"){echo "selected='selected'";}else{echo '';} ?>>Admin and Facilities</option>
                                <option value="Finance" <?php if($_data[0]->requestby == "Finance"){echo "selected='selected'";}else{echo '';} ?>>Finance</option>
                                <option value="Marketing Production" <?php if($_data[0]->requestby == "Marketing Production"){echo "selected='selected'";}else{echo '';} ?>>Marketing Production</option>
                                <option value="Marketing Content" <?php if($_data[0]->requestby == "Marketing Content"){echo "selected='selected'";}else{echo '';} ?>>Marketing Content</option>
                                <option value="Marketing CRM and OB" <?php if($_data[0]->requestby == "Marketing CRM and OB"){echo "selected='selected'";}else{echo '';} ?>>Marketing CRM and OB</option>
                                <option value="Marketing Digital" <?php if($_data[0]->requestby == "Marketing Digital"){echo "selected='selected'";}else{echo '';} ?>>Marketing Digital</option>

                              
                            </select>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-sm-6 form-group">
                        <form class="">
                            <div class="input-group  form-group-sm">
                                 <span class="input-group-addon" id="basic-addon3">Name of Requestor</span>
                              <input name="requestor" class="form-control input-inline update-requestor requestor" value="<?php echo $_data[0]->requestor ?>">
                        </div>

                        </form>
                    </div>
                    <div class="col-sm-6 form-group">
                        <div class="input-group  form-group-sm">
                                <span class="input-group-addon" id="basic-addon3">Target Audience </span>
                             <select name="target" class="form-control input-inline update-target">
                                <option value="">Select Here</option>
                           
                                <option value="TOA ANZ" <?php if($_data[0]->target == "TOA ANZ"){echo "selected='selected'";}else{echo'';} ?>>TOA ANZ</option>
                                <option value="TOA USA" <?php if($_data[0]->target == "TOA USA"){echo "selected='selected'";}else{echo'';} ?>>TOA USA</option>
                                <option value="TOA Global Employees" <?php if($_data[0]->target == "TOA Global Employees"){echo "selected='selected'";}else{echo'';} ?>>TOA Global Employees</option>
                                <option value="TOA Global Recruitement" <?php if($_data[0]->target == "TOA Global Recruitement"){echo "selected='selected'";}else{echo'';} ?>>TOA Global Recruitement</option>


                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <form class="input-group message-status form-group-sm">
                            <span class="input-group-addon">
                                <span>STATUS</span>
                                <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" class="hide message-status-input" name="message_status">
                            </span>

                            <input type="hidden" value="<?php echo $_data[0]->email ?>" name="sender_update" >
                            <select name="status" class="form-control input-inline"  data-select="<?php echo $_data[0]->status ?>">
                                <option value="4">Return to Sender</option>
                                <option value="3">Completed</option>
                                
                                <option value="1">In Progress</option>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="submit" ><i class="fa fa-pencil" aria-hidden="true"></i> UPDATE STATUS</button>
                            </span>


                        </form>
                    </div>
                    <div class="col-sm-6 form-group">
                        <?php if ($user_position !== '') { ?>
                            <div class="input-group  form-group-sm">
                                <span class="input-group-addon"><span>ADD ASSIGNEE</span></span>
                                <input type="text" class="form-control input-inline find-assignee" data-bind="<?php echo $this->uri->segment(3) ?>">
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div  class="col-sm-6 form-group">
                        <div class="input-group  form-group-sm assignee-form" data-bind="<?php echo $this->uri->segment(3) ?>">
                            <span class="input-group-addon"><span>ASSIGNEE</span></span> 
                            <div class="form-control" style="overflow: hidden">
                                <?php foreach ($_assn->result() as $_it) { ?>
                                    <strong class="label label-info">
                                        <?php echo $_it->fname ?> <?php echo $_it->lastname ?> 
                                        <?php if ($user_position !== '') { ?>
                                            <a href="<?php echo $_it->GETID ?>" class="fa fa-times text-danger"></a>
                                        <?php } ?>
                                    </strong> 
                                <?php } ?>
                            </div> 
                        </div>              
                    </div>
                    <div class="col-sm-3 form-group">
                        <div class="input-group  form-group-sm">
                            <span class="input-group-addon"><span>This is a job for</span></span> 
                            <?php if ($user_position === '') { ?>
                                <div class="form-control"><?php echo $this->Models->issue_type($_data[0]->subject) ?></div>
                            <?php } else { ?>
                                <select class="update-subject form-control input-xs input-inline optionthis" data-bind="<?php echo $this->uri->segment(3) ?>" data-select="<?php echo $_data[0]->subject ?>">
                                    <option value="">SELECT HERE</option>
                                    <option value="1">Marketing Production</option>
                                <option value="2">Marketing Content</option>
                                <option value="3">Marketing CRM</option>
                                <option value="4">Marketing Digital</option>
                                </select>
                            <?php } ?>
                        </div>
                       
                    </div>
                    <div  class="col-sm-3 form-group otherdiv hide">
                         <div class="input-group  form-group-sm ">
                                <span class="input-group-addon"><span>OTHERS</span></span>
                                <input name="otherfield" value="<?php echo $_SET; ?>" class=" form-control update-sub_category input-inline ui-autocomplete-input"> 
                            </div>        
                    </div>

                </div>  
                <div class="row  ">
                    
                      
                          <div  class="col-sm-6 form-group">
                        
                             <div class="input-group  form-group-sm">
                              <span class="input-group-addon"><span>Required Before</span></span>
                                <input type="text" class="form-control input-inline date-content update_date_required" name="requiredbefore" value="<?php echo $_data[0]->date_required ?>">
                        </div>     
                                </div>
                                 <div  class="col-sm-6 form-group">
                        <div class="input-group form-group-sm cc-user" data-bind="<?php echo $this->uri->segment(3) ?>">
                            <span class="input-group-addon cc-name-label">
                                <i class="fa fa-users"></i>
                            </span> 
                            <input type="text" class="form-control cc-input-search" data-bind="<?php echo $this->uri->segment(3) ?>"  placeholder="CC Search by name or email" value="">
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <form class="">
                            <div class="input-group  form-group-sm">
                                <span class="input-group-addon"><span>Link to Delivery</span></span>
                                <input type="text" class="form-control input-inline update-link" name="linktodelivery" value="<?php echo $_data[0]->link ?>">
                        </div>

                        </form>
                    </div>
                     <div class="col-sm-6 form-group">
                        <form class="">
                              <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Approval</span>
                             <select name="approvedby" class="form-control input-inline approvedby">
                                <option value="">Select Here</option>
                                <option value="Chief Approval" <?php if($_data[0]->approvedby == "Chief Approval"){echo "selected='selected'";}else{echo'';} ?>>Chief Approval</option>
                                 <option value="Director Approval"  <?php if($_data[0]->approvedby == "Director Approval"){echo "selected='selected'";}else{echo'';} ?>>Director Approval</option>
                            </select>
                        
                            
                        </div>
                        </div>

                        </form>
                    </div>
                   
                </div>
                <div class="row">
                    <div  class="col-sm-6 form-group">
                        <div class="input-group form-group-sm">
                            <span class="input-group-addon"><span>DATE</span></span> 
                            <?php if ($user_position === '') { ?>
                                <div class="form-control"><?php echo date('m/d/Y h:i:s A', $_data[0]->date) ?></div> 
                            <?php } else { ?>
                                <input type="text" class="form-control update-issue-date" data-bind="<?php echo $this->uri->segment(3) ?>" value="<?php echo date('m/d/Y h:i:s A', $_data[0]->date) ?>" />
                            <?php } ?>
                        </div>              
                    </div>
                    <div  class="col-sm-6 form-group">
                        <div class="input-group form-group-sm">
                            <span class="input-group-addon"><span>SENDER</span></span> 
                            <div class="form-control"><?php echo $_data[0]->fname ?> <?php echo $_data[0]->lastname ?></div> 
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php $attached = $this->Models->ticket_attached($_data[0]->key_id); ?>
                        <?php if ($attached->num_rows() > 0) { ?>
                            <div class="well well-sm">
                                <?php foreach ($attached->result() as $_file) { ?>
                                    <a href="<?php echo site_url('request/download?file=' . $_file->filename) ?>" target="blank" class="btn btn-default btn-xs"><?php echo $_file->filename ?></a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
          
            <div class="panel-body overflow-x">
              
                <p><strong>Detail the output you need. <span style='color:red;'>*</span></strong><span style='color:lightgray;'> (For example - tarpauline banner artwork for Christmas event.)</span></p>
                <hr>
                <div class="description">
                    <div class="description-container">
                        <?php echo $_data[0]->message ?>
                    </div>
                    <form class="edit-textarea hide">
                        <div class="form-group">
                            <input type="hidden" class="hide" value="<?php echo $this->uri->segment(3); ?>" name="edit_descptn_id" />
                            <textarea class="edit-descptn" id="edit-descptn" name="edit_descptn"><?php echo $_data[0]->message ?></textarea>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-comment" aria-hidden="true"></i> UPDATE</button>
                    </form>
                    <div class="text-right show-edit-textarea">
                        <button class="btn-primary btn"><i class="fa fa-pencil"></i> Edit Description</button>
                    </div>
                </div>
                <p class="hide"><strong>Content brief <span style='color:red;'>*</span></strong><span style='color:lightgray;'>(The words that need to be in the output)</span></p>
                <hr>

                 <div class="row hide">
                    <div  class="col-sm-12 form-group">
                         <textarea class="contenttextarea update_content" name="contenttextarea" id="contenttextarea" style="width:100%;height:280px;"> <?php echo $_data[0]->content ?></textarea>
                    </div>
                </div>


                <hr>
                <h4><i class="fa fa-star-half-o" aria-hidden="true"></i> RATE US</h4>
                <form class="stars">
                    <?php
                    $query = $this->Models->check_ratings_issues($this->uri->segment(3));
                    $r = $query->result();
                    ?>
                    <div class="star1 start-rating<?php echo (isset($r[0]) ? ($r[0]->rate_number > 0 ? ' star-checked' : '') : ''); ?>">
                        <input type="radio" class="star-radio" value="1" name="rating" title="1 Star" data-toggle="tooltip" data-placement="top"/>
                        <div class="star2 start-rating<?php echo (isset($r[0]) ? ($r[0]->rate_number > 1 ? ' star-checked' : '') : ''); ?>">
                            <input type="radio" class="star-radio" value="2" name="rating" title="2 Stars" data-toggle="tooltip" data-placement="top"/>
                            <div class="star3 start-rating<?php echo (isset($r[0]) ? ($r[0]->rate_number > 2 ? ' star-checked' : '') : ''); ?>">
                                <input type="radio" class="star-radio" value="3" name="rating" title="3 Stars" data-toggle="tooltip" data-placement="top"/>
                                <div class="star4 start-rating<?php echo (isset($r[0]) ? ($r[0]->rate_number > 3 ? ' star-checked' : '') : ''); ?>">
                                    <input type="radio" class="star-radio" value="4" name="rating" title="4 Stars" data-toggle="tooltip" data-placement="top"/>
                                    <div class="star5 start-rating<?php echo (isset($r[0]) ? ($r[0]->rate_number > 4 ? ' star-checked' : '') : ''); ?>">
                                        <input type="radio" class="star-radio" value="5" name="rating" title="5 Stars" data-toggle="tooltip" data-placement="top"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <h4><i class="fa fa-comment" aria-hidden="true"></i> COMMENTS</h4>
                <hr>   
                <div class="comment-output" data-bind="<?php echo $this->uri->segment(3) ?>" data-id="<?php echo $this->session->userdata('user_session') ?>">
                    <div class="list-group">
                        <?php
                        $comment = $this->Models->id_comment($this->uri->segment(3));
                        foreach ($comment->result() as $_comments) {
                            ?>
                            <div class="list-group-item">
                                <div class="comment-item-list">
                                    <img class="add-profile" src="<?php echo site_url('request/image_id/' . $_comments->UID) ?>.jpg">
                                </div>
                                <div class="comment-item-pro">
                                    <?php if ($_comments->UID === $this->session->userdata('user_session')) { ?>
                                        <a class="close text-danger" href="<?php echo $_comments->ID; ?>"><i class="fa fa-times"></i></a>
                                    <?php } ?>
                                    <p><strong<?php ($_comments->UID === $this->session->userdata('user_session') ? ' class="text-info"' : '') ?>><?php echo $_comments->fname; ?> <?php echo $_comments->lastname; ?></strong></p>
                                    <?php echo $_comments->comments; ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <form class="comment-form">
                    <input type="hidden" value="<?php echo $this->session->userdata('user_session'); ?>" name="user_id">
                    <input type="hidden" value="<?php echo $this->uri->segment(3); ?>" name="comment_id">
                    <div class="form-group">
                        <textarea class="comment-editor" id="comment_editor" name="comment"></textarea>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-comment" aria-hidden="true"></i> COMMENT</button>
                </form>
            </div>
            <div class="panel-footer text-right">
            </div>
        </div>
    </div>
    <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>


<div class="modal fade modal-rating" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Feed back</h4>
            </div>
            <div class="modal-body">
                <form class="form-suggestion">
                    <p>(You clicked "<span class="rating-numb"></span>")</p>
                    <input type="hidden" value="" name="sender_rate"  class="rating-numb-val">
                    <input type="hidden" value="<?php echo $this->session->userdata('user_session') ?>" name="sender_id">
                    <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" name="issue_id">
                    <strong>Comment/Suggestion</strong>
                    <div class="form-group">
                        <textarea class="form-control" name="suggestion"></textarea>
                    </div>
                </form>

                <div class="success-rating hide">
                    <span class="feedback-message">We're delighted that you are so happy with us at the moment and we really appreciate your feedback.</span>
                    <br><br>
                    Thanks
                    <br><br>    
                    IT Team 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default hide ok-rating" data-dismiss="modal">Ok</button>
                <button type="button" class="btn btn-primary submit-suggestion">Submit</button>
            </div>
        </div>
    </div>
</div>
<script>


    $(document).ready(function () {

        var valueofothers = $('.optionthis').val();
        if (valueofothers == 5) {
            $('.otherdiv').removeClass('hide');
        } else {
            $('.otherdiv').addClass('hide');
        }




        $('.optionthis').on('change', function (e) {
            var valueofothers = $('.optionthis').val();
            console.log(valueofothers);
            if (valueofothers == 5) {
                $('.otherdiv').removeClass('hide');
            } else {
                $('.otherdiv').addClass('hide');
            }

        });

    });
</script>