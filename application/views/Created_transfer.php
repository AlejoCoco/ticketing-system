
<?php
if (is_numeric($this->uri->segment(3))) {
    $this->uri->segment(3);
    $_data = $this->Models->created_transfer_id($this->uri->segment(3));
    if ($_data->num_rows() === 0) {
        redirect(site_url());
    }
    $_user = $this->Models->user_id($_data->result()[0]->transfer_involve);
    $_userid = $_user->result();
} else {
    redirect(site_url());
}
?>
<div class="content">
   <div class="container-fluid">
      <div class="create-transfer-form animated fadeIn widget grid6" method="post">
         <div class="panel-body overflow-x">
            <form  class="update-table-list" method="post">
               <input type="hidden" value="<?php echo $_data->result()[0]->IDT ?>" name="trans_id"/>
               <div class="row">
                  <div class="col-md-12 text-right form-group"><button class="btn btn-success" type="submit"><i class="fa fa-pencil"></i> UPDATE</button></div>
                  <div class="col-md-6">
                     <div class="input-group  form-group"> 
                        <span class="input-group-addon" id="basic-addon3">Select Status</span> 
                        <select name="category" class="form-control input-inline"> 
                           <option <?php echo ($_data->result()[0]->client_support == '1' ? 'selected="selected"' : '') ?> value="1">SUPPORT</option> 
                           <option <?php echo ($_data->result()[0]->client_support == '2' ? 'selected="selected"' : '') ?> value="2">CLIENT</option> 
                        </select> 
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="input-group  form-group"> 
                        <span class="input-group-addon" id="basic-addon3">Client Name</span>
                        <input type="text" class="form-control" value="<?php echo $_data->result()[0]->client_name ?>" name="client_name">
                     </div>
                  </div>
               </div>
               
               
     
               <div class="row">
                  <div class="col-md-6 form-group">
                     <div class="input-group"> 
                        <span class="input-group-addon date-picker" id="basic-addon3">Transfer From</span> 
                        <input type="text" class="form-control" value="<?php echo $_data->result()[0]->transfer_from ?>" name="transfe_from">
                     </div>
                  </div>
                  <div class="col-md-6 form-group">
                     <div class="input-group"> 
                        <span class="input-group-addon" id="basic-addon3">Transfer To</span>
                        <input type="text" class="form-control transfer-to" value="<?php echo $_data->result()[0]->transfer_to ?>" name="transfer_to">
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-4 form-group">
                     <div class="input-group"> 
                        <span class="input-group-addon" id="basic-addon3">Transfer Start</span> 
                        <input type="text" class="form-control transfer-start" value="<?php echo ($_data->result()[0]->transfer_start == '' ? '' : date('m/d/Y h:i:s A', $_data->result()[0]->transfer_start)) ?>" name="transfer_start">
                     </div>
                  </div>
                  <div class="col-md-4  form-group">
                     <div class="input-group"> 
                        <span class="input-group-addon" id="basic-addon3">Transfer End</span>
                        <input type="text" class="form-control transfer-end" value="<?php echo ($_data->result()[0]->transfer_end == '' ? '' : date('m/d/Y h:i:s A', $_data->result()[0]->transfer_end)) ?>" name="transfer_end">
                     </div>
                  </div>
                  <div class="col-md-4 form-group">
                     <div class="input-group"> 
                        <span class="input-group-addon" id="basic-addon3">Sender</span>
                        <div class="form-control" ><?php echo $_userid[0]->fname ?> <?php echo $_userid[0]->lastname ?></div>
                     </div>
                  </div>
               </div>

            </form>
            <div class="form-group table-responsive">
               <div class="input-group">  
                  <span class="input-group-addon trans-assignee" id="basic-addon3">
                      <?php
                      $query = $this->Models->transfer_assignee($this->uri->segment(3));
                      $row = $query->result();
                      if ($query->num_rows() > 0) {
                          foreach ($row as $value) {
                              ?>
                             <label class="label label-primary"><a href="<?php echo $value->ITID; ?>" class="fa fa-times"></a> <?php echo $value->fname . ' ' . $value->lastname ?></label>
                             <?php
                         }
                     } else {
                         echo 'Assignee';
                     }
                     ?>
                  </span>
                  <input type="text" class="form-control transfer-assignee" style="min-width: 250px" name="assignee" data-bind="<?php echo $this->uri->segment(3) ?>">
               </div>
            </div>

            <div class="row">
               <div class="col-md-6 form-group">
                  <div class="input-group"> 
                     <span class="input-group-addon" id="basic-addon3">PC Name</span> 
                     <input type="text" class="form-control add-pc-name" value="" name="pc_name" data-bind="<?php echo $this->uri->segment(3) ?>">
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-md-6 form-group">
                  <ul class="list-group pc-added-name">

                  </ul>
               </div>
            </div>
            
         </div>
      </div>
   </div>
   <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>
<script>
    (function (j) {
        j(window).load(function () {

        });
    })(jQuery);
</script>  