

<div class="container">
    <form class="Guest-form animated fadeIn widget grid6" method="post" enctype="multipart/form-data">
       <div class="widget-heading guest-header">GUEST TICKET</div>
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-12 text-left form-group" >
                    <a href="<?php echo base_url(); ?>" class="btn btn-success btn-sm" > <i class="fa fa-arrow-left"></i> BACK LOGIN</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Select Status</span>
                        <select name="priority" class="form-control input-inline">
                            <option value="">SELECT</option>
                            <option value="1">CRITICAL</option>
                            <option value="2">MAJOR</option>
                            <option value="3">MINOR</option>
                            <option value="4">TRIVIAL</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-1 form-group"></div>
                <div class="col-sm-6 form-group">
                    <div class="input-group pull-right">
                        <button type="submit" class="btn btn-success btn-send"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> <span>SEND</span></button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Select Type Of Issue</span>
                        <select name="subject" class="form-control input-inline">
                            <option value="">Select Here</option>
                            <option value="1">TRANSFER/SETUP COMPUTER</option>
                            <option value="2">INTERNET ISSUES</option>
                            <option value="3">NETWORK ISSUES</option>
                            <option value="4">HARDWARE ISSUES/CONCERN</option>
                            <option value="5">SOFTWARE ISSUES/CONCERN</option>
                            <option value="6">MARKETING</option>
                            <option value="7">SYSTEM TASK</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2 form-group">
                    <input type="file" name="file[]" class="form-control fileupload" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                </div>
                <div class="col-sm-5 form-group">
                    <div class="input-group">
                        <input type="text" class="form-control fake-path" placeholder="doc,ppt,xsl,pdf,image only" readonly="readonly">
                        <span class="input-group-btn">
                            <button class="btn btn-default attached" type="button">Attach file <i class="fa fa-paperclip" aria-hidden="true"></i> MAX 5MB</button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">First Name</span>
                        <input type="text" class="form-control" name="firstname">
                    </div> 
                </div>
                <div class="col-sm-3 form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Last Name</span>
                        <input type="text" class="form-control" name="lastname">
                    </div> 
                </div>
                <div class="col-sm-3 form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Email*</span>
                        <input type="text" class="form-control" name="email" >
                    </div> 
                </div>
                <div class="col-sm-3 form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Location</span>
                        <input type="text" class="form-control" name="location">
                    </div> 
                </div>
             </div>
        </div>

        <div class="panel-body overflow-x">
            <div class="form-group"> 
                <textarea class="create-guest-ticket" name="create_ticket" id="create-guest-ticket"></textarea>
            </div>
            <div class="progress hide">
                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                    40% Complete (success)
                </div>
            </div>
            <div class="validation">

            </div>
        </div>
    </form>
</div>
<div class="footer text-center muted">Time Clock The Outsourced Accountant 2017</div>


