<?php $user_position = $this->session->userdata('user_position'); ?>
<div class="content">
    <div class="container-fluid">
        <div class="animated fadeIn widget grid6">
           <div class="widget-heading sent-issues">SENT ISSUES</div>
            <div class="panel-heading">
                <form class="inbox-form row" method="get" action="" data-page="">
                    <div class="<?php echo ($user_position === ''?'col-sm-6':'col-sm-3') ?>">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3"><span>Select Status</span></span>
                            <select name="status" class="form-control select-inbox-status" data-select="<?php echo $this->Models->status() ?>">
                                <option value="1">Open Issues</option>
                                <option value="2">Reopen</option>
                                <option value="3">Ongoing</option>
                                <option value="4">Resolved</option>
                            </select>
                        </div>
                    </div>
                    <?php if($user_position !== ''){ ?>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3"><span>Select Location</span></span>
                            <select name="location" class="form-control">
                                <option value=""<?php echo ($this->Models->lctn() === ''?" selected='seleted'":"") ?>>All Location</option>
                                <option value="Clark"<?php echo ($this->Models->lctn() === 'Clark'?" selected='seleted'":"") ?>>Clark</option>
                                <option value="Manila"<?php echo ($this->Models->lctn() === 'Manila'?" selected='seleted'":"") ?>>Manila</option>
                                <option value="Australia"<?php echo ($this->Models->lctn() === 'Australia'?" selected='seleted'":"") ?>>Australia</option>
                            </select>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-sm-6">
                        <input type="hidden" class="hide" name="page" value="1">
                        <div class="input-group">
                            <input type="text" class="form-control" name="keyword"  value="<?php echo $this->Models->keyword()  ?>" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-body overflow-x">
                <div class="form-group"> 
                    <div class="row top_tiles main-widget">
                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url("sent_issues?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=1") ?>" class="tile-stats color-danger<?php echo ($this->Models->status() == 1?' active-panel':'') ?>">
                                <?php $_open = $this->Models->ticket_status(1) ?>
                                <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                                <span class="count count_1"><?php echo $_open->num_rows() ?></span>
                                <h3>TOTAL In Progress</h3>
                            </a>
                        </div>
                       
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url("sent_issues?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=4") ?>" class="tile-stats color-info<?php echo ($this->Models->status() == 4?' active-panel':'') ?>">
                                <?php $_ongoing = $this->Models->ticket_status(4) ?>
                                <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                                <span class="count count_3"><?php echo $_ongoing->num_rows() ?></span>
                                <h3>TOTAL Return to sender</h3>
                            </a>
                        </div>
                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url("sent_issues?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=3") ?>" class="tile-stats color-primary<?php echo ($this->Models->status() == 3?' active-panel':'') ?>">
                                <?php $_resolved = $this->Models->ticket_status(3) ?>
                                <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                                <span class="count count_4"><?php echo $_resolved->num_rows() ?></span>
                                <h3>TOTAL Completed</h3>
                            </a>
                        </div>
                    </div>
                </div>
                <table class="inbox-table table table-bordered">
                    <thead>
                        <tr>
                            <th class="hide-xs text-center">STATUS</th>
                            <th class="hide-xs description">DESCRIPTION</th>
                            <th class="hide-xs text-center">TRACKING ID</th>
                            <th class="ellipsis">SUBJECT</th>
                            <th class="hide-xs text-center hide">PRIORITY</th>
                            <th class="hide-xs text-center">SENDER</th>
                            <th class="hide-xs text-center">LOCATION</th>
                            <th class="hide-xs text-center">DATE</th>
                            <th class="hide-xs text-center">DATE SOLVED</th>
                            <th class="hide-xs text-center">ASSIGN</th>
                        </tr>
                    </thead>
                    <tbody class="status-<?php echo strtolower ($this->Models->issue_string($this->Models->status())); ?>">
                        <?php $_all_status = $this->Models->ticket_status_all($this->Models->status()) ?>
                        <?php foreach ($_all_status->result() as $_rows) { ?>
                        <tr class="remove_<?php echo $_rows->ID ?>">
                            <td class="text-center"><?php echo $this->Models->issue_status($_rows->status) ?></td>
                            <td><a  class="ellipsis" data-content="<?php  echo $this->Models->special_chars($_rows->message) ?>" href="<?php echo site_url('issues_page/issue_id/'.$_rows->ID) ?>"><?php echo $this->Models->limit_text($_rows->message, 5)  ?></a></td>
                            <td class="text-center"><a href="<?php echo site_url('issues_page/issue_id/'.$_rows->ID) ?>"><?php echo $_rows->key_id ?></a></td>
                            <td><a href="<?php echo site_url('issues_page/issue_id/'.$_rows->ID) ?>"><?php echo $this->Models->issue_type($_rows->subject) ?></a></td>
                            <td class="text-center hide"><?php echo $this->Models->priority($_rows->priority) ?></td>
                            <td class="text-center"><?php echo $_rows->fname ?> <?php echo $_rows->lastname ?></td>
                            <td class="text-center"><?php echo $_rows->location ?></td>
                            <td class="text-center"><?php echo date("m/d/Y h:i:s A",$_rows->date) ?></td>
                            <td class="text-center"><?php echo ($_rows->date_solve?date("m/d/Y h:i:s A",$_rows->date_solve):'') ?></td>
                            <?php $assignee = $this->Models->popup_assignee($_rows->ID) ?>
                            <?php 
                            $_names = '';
                            foreach ($assignee->result() as $_as) {
                               $_names .= $_as->fname.' '.$_as->lastname.'</br>';
                            } 
                            ?>
                            <td class="text-center"><span data-toggle="popover" data-content="<?php echo $_names ?>"><?php echo $this->Models->check_assignee($_rows->ID)->num_rows() ?></span></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer text-right">
                 <?php echo $this->Models->ticket_status_pagination() ?>
            </div>
        </div>
    </div>
    <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>
<script>
    (function (j) {
        j(window).load(function () {

        });
    })(jQuery);
</script>  