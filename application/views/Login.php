
<form class="login" method="post" autocomplete="false" autocomplete="off">
    <div class="text-center">
        <img alt="toa" src="https://depthoffshore.com/wp-content/uploads/2016/05/Depth_Offshore-white350x147png.png" style="height:80px;">
    </div>
    <div class="form-group">
        <label class="input-group input-add-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control" name="email_address" autocomplete="false" placeholder="Email">
        </label>
    </div>
    <div class="form-group">
        <label class="input-group input-add-group">
            <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
            <input type="password" class="form-control" name="user_password" autocomplete="false" placeholder="Password">
        </label>
    </div>
    <div class="checkbox">
        <div class="custom-input remember_me">
            <input type="checkbox" name="remember_me" value="1" id="remember"><label for="remember">Remember me</label>
        </div> 
    </div>
    <div class="form-group text-center">
       <button class="btn btn-primary btn-lg login-btn"><span class="loader-spinner"></span><span class="text-login">LOGIN</span></button>
    </div>
         <div class="form-group ">
        <a href="<?php echo base_url('guest'); ?>" class="btn btn-primary btn-block btn-lg">GUEST</a>
    </div>
    <div class="output"></div>
</form>
<div class="login-footer text-center muted" style="color:white">Ticketing The Outsourced Accountant 2017</div>
