<?php $user_position = $this->session->userdata('user_position'); ?>
<div class="content">
   <div class="container-fluid">
      <div class="animated fadeIn widget grid6">
         <div class="widget-heading report-header">REPORT</div>
         <div class="panel-heading">
            <form class="inbox-form row" method="get" action="" data-page="">
               <div class="<?php echo ($user_position === '' ? 'col-sm-6' : 'col-sm-3') ?>">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Select Status</span></span>
                     <select name="status" class="form-control select-inbox-status" data-select="<?php echo $this->Models->status() ?>">
                        <option value="1">Open Issues</option>
                        <option value="2">Reopen</option>
                        <option value="3">Ongoing</option>
                        <option value="4">Resolved</option>
                     </select>
                  </div>
               </div>
               <?php if ($user_position !== '') { ?>
                   <div class="col-sm-3">
                      <div class="input-group">
                         <span class="input-group-addon" id="basic-addon3"><span>Select Location</span></span>
                         <select name="location" class="form-control">
                            <option value=""<?php echo ($this->Models->lctn() === '' ? " selected='seleted'" : "") ?>>All Location</option>
                            <option value="Clark"<?php echo ($this->Models->lctn() === 'Clark' ? " selected='seleted'" : "") ?>>Clark</option>
                            <option value="Manila"<?php echo ($this->Models->lctn() === 'Manila' ? " selected='seleted'" : "") ?>>Manila</option>
                            <option value="Australia"<?php echo ($this->Models->lctn() === 'Australia' ? " selected='seleted'" : "") ?>>Australia</option>
                         </select>
                      </div>
                   </div>
               <?php } ?>
               <div class="col-sm-6">
                  <input type="hidden" class="hide" name="page" value="1">
                  <div class="input-group">
                     <input type="text" class="form-control" name="keyword"  value="<?php echo $this->Models->keyword() ?>" placeholder="Search for...">
                     <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                     </span>
                  </div>
               </div>
            </form>
         </div>
         <div class="panel-body overflow-x">
            <div class="row">
               <div class="col-sm-4 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Date From</span></span>
                     <input type="text" class="form-control date-from" />
                  </div>
               </div>
               <div class="col-sm-4 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Date To</span></span>
                     <input type="text" class="form-control date-to" disabled="disabled" />
                  </div>
               </div>
               <div class="col-sm-2 form-group">
                  <button class="btn btn-success btn-block export-button" disabled="disabled"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export KPI</button>
               </div>
               <div class="col-sm-2 form-group">
                  <button class="btn btn-default btn-block show-button" disabled="disabled"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Show KPI</button>
               </div>
            </div>
            <div class="form-group"> 
               <div class="row top_tiles">
                  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                     <a href="<?php echo site_url("reports?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=1") ?>" class="tile-stats color-danger<?php echo ($this->Models->status() == 1?' active-panel':'') ?>">
                         <?php $_open = $this->Models->ticket_status_it(1) ?>
                        <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                        <span class="count"><?php echo $_open->num_rows() ?></span>
                        <h3>Total Open Issues</h3>
                     </a>
                  </div>
                  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                     <a href="<?php echo site_url("reports?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=2") ?>" class="tile-stats color-warning<?php echo ($this->Models->status() == 2?' active-panel':'') ?>">
                         <?php $_reopen = $this->Models->ticket_status_it(2) ?>
                        <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                        <span class="count"><?php echo $_reopen->num_rows() ?></span>
                        <h3>Total Re Open Issues</h3>
                     </a>
                  </div>
                  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                     <a href="<?php echo site_url("reports?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=3") ?>" class="tile-stats color-info<?php echo ($this->Models->status() == 3?' active-panel':'') ?>">
                         <?php $_ongoing = $this->Models->ticket_status_it(3) ?>
                        <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                        <span class="count"><?php echo $_ongoing->num_rows() ?></span>
                        <h3>Total On Going Issues</h3>
                     </a>
                  </div>
                  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                     <a href="<?php echo site_url("reports?keyword=" . $this->Models->keyword() . "&location=" . $this->Models->lctn() . "&status=4") ?>" class="tile-stats color-primary<?php echo ($this->Models->status() == 4?' active-panel':'') ?>">
                         <?php $_resolved = $this->Models->ticket_status_it(4) ?>
                        <span class="icon"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
                        <span class="count"><?php echo $_resolved->num_rows() ?></span>
                        <h3>Total Resolved Issues</h3>
                     </a>
                  </div>
               </div>
            </div>
            <table class="inbox-table table table-bordered">
               <thead>
                  <tr>
                     <th class="hide-xs text-center">STATUS</th>
                     <th class="hide-xs description">DESCRIPTION</th>
                     <th class="hide-xs text-center">TRACKING ID</th>
                     <th class="ellipsis">SUBJECT</th>
                     <th class="hide-xs text-center">PRIORITY</th>
                     <th class="hide-xs text-center">SENDER</th>
                     <th class="hide-xs text-center">LOCATION</th>
                     <th class="hide-xs text-center">DATE</th>
                     <th class="hide-xs text-center">DATE SOLVED</th>
                     <th class="hide-xs text-center">ASSIGN</th>
                  </tr>
               </thead>
               <tbody>
                   <?php $_all_status = $this->Models->ticket_status_support($this->Models->status()) ?>
                   <?php foreach ($_all_status->result() as $_rows) { ?>
                      <tr>
                         <td class="text-center"><?php echo $this->Models->issue_status($_rows->status) ?></td>
                         <td><a class="ellipsis" href="<?php echo site_url('issues_page/issue_id/' . $_rows->ID) ?>"><?php echo $this->Models->limit_text($_rows->message, 5) ?></a></td>
                         <td class="text-center"><a href="<?php echo site_url('issues_page/issue_id/' . $_rows->ID) ?>"><?php echo $_rows->key_id ?></a></td>
                         <td><a  href="<?php echo site_url('issues_page/issue_id/' . $_rows->ID) ?>"><?php echo $this->Models->issue_type($_rows->subject) ?></a></td>

                         <td class="text-center"><?php echo $this->Models->priority($_rows->priority) ?></td>
                         <td class="text-center"><?php echo $_rows->fname ?> <?php echo $_rows->lastname ?></td>
                         <td class="text-center"><?php echo $_rows->location ?></td>
                         <td class="text-center"><?php echo date("m/d/Y h:i:s A", $_rows->date) ?></td>
                         <td class="text-center">
                             <?php if ($this->Models->status() == 4) { ?>
                                <input type="text" class="resolved-date" data-bind="<?php echo $_rows->ID ?>" value="<?php echo ($_rows->date_solve ? date("m/d/Y h:i:s A", $_rows->date_solve) : '') ?>"/>
                            <?php } else { ?>
                                <?php echo ($_rows->date_solve ? date("m/d/Y h:i:s A", $_rows->date_solve) : '') ?>
                            <?php } ?>
                         </td>
                         <?php $assignee = $this->Models->popup_assignee($_rows->ID) ?>
                         <?php
                         $_names = '';
                         foreach ($assignee->result() as $_as) {
                             $_names .= $_as->fname . ' ' . $_as->lastname . '</br>';
                         }
                         ?>
                         <td class="text-center"><span data-toggle="popover" data-content="<?php echo $_names ?>"><?php echo $this->Models->check_assignee($_rows->ID)->num_rows() ?></span></td>
                      </tr>
                  <?php } ?>
               </tbody>
            </table>

         </div>
         <div class="panel-footer text-right">
             <?php echo $this->Models->ticket_status_support_pagination() ?>

         </div>
      </div>
      <div class="animated fadeIn widget grid6">
         <div class="panel-body">
            <div class="row">

               <div class="col-sm-6 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Select Year</span></span>
                     <select class="form-control select-year-report" data-select="<?php echo date('Y') ?>">
                         <?php for ($x = 2016; $x <= 2020; $x++) { ?>
                            <option value="<?php echo $x ?>"><?php echo $x ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Select Month</span></span>
                     <select name="status" class="form-control select-month-report" data-select="<?php echo date('n') ?>">
                         <?php for ($a = 1; $a <= 12; $a++) { ?>
                             <?php
                             $dateObj = DateTime::createFromFormat('!m', $a);
                             $monthName = $dateObj->format('F'); // March
                             ?>
                            <option value="<?php echo $a ?>"><?php echo $monthName ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-4 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Date From</span></span>
                     <input type="text" class="form-control date-from" />
                  </div>
               </div>
               <div class="col-sm-4 form-group">
                  <div class="input-group">
                     <span class="input-group-addon" id="basic-addon3"><span>Date To</span></span>
                     <input type="text" class="form-control date-to" disabled="disabled" />
                  </div>
               </div>
               <div class="col-sm-2 form-group">
                  <button class="btn btn-primary btn-block export-all-button" disabled="disabled"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export All IT KPI</button>
               </div>
               <div class="col-sm-2 form-group">
                  <button class="btn btn-default btn-block show-all-button" disabled="disabled"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Show All IT KPI</button>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <canvas id="year-report"></canvas>
               </div>
               <div class="col-sm-6">
                  <canvas id="montly-report"></canvas>
               </div>
            </div>
            <hr>
            <div class="row">
               <div class="col-sm-6">
                  <div class="text-center">Type of issues</div>
                  <canvas id="issue-report"></canvas>
               </div>
               <div class="col-sm-6">
                  <div class="text-center">Status</div>
                  <canvas id="status-report"></canvas>
               </div>
            </div>
         </div>
      </div>

      <?php if ($user_position === '0') { ?>
          <div class="animated fadeIn widget grid6">
             <div class="panel-body">
                <div class="row">
                   <div class="col-sm-6 form-group">
                      <div class="input-group">
                         <span class="input-group-addon" id="basic-addon3"><span>Date From</span></span>
                         <input type="text" class="form-control date-from date-from-report" />
                      </div>
                   </div>
                   <div class="col-sm-6 form-group">
                      <div class="input-group">
                         <span class="input-group-addon" id="basic-addon3"><span>Date To</span></span>
                         <input type="text" class="form-control date-to date-to-report" disabled="disabled" />
                      </div>
                   </div>

                </div>
                <table class="inbox-table table table-bordered">
                   <thead>
                      <tr>
                         <th class="hide-xs text-center">Name</th>
                         <th class="hide-xs description">Lastname</th>
                         <th class="hide-xs text-center">Report</th>
                      </tr>
                   </thead>
                   <tbody class="support-view-report">
                       <?php
                       $_it = $this->Models->user_position_support();
                       foreach ($_it->result() as $sup) {
                           ?>
                          <tr>
                             <td><?php echo $sup->fname ?></td>
                             <td><?php echo $sup->lastname ?></td>
                             <td class="text-center">
                                <a href="<?php echo $sup->ID ?>" class="export-report-btn btn btn-success btn-xs"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export My Report</a>
                                <a href="<?php echo $sup->ID ?>" class="view-report-btn btn btn-info btn-xs"><i class="fa fa-book" aria-hidden="true"></i> View My Report</a>
                                <a href="<?php echo site_url('calendar/user/' . $sup->ID) ?>" class="btn btn-primary btn-xs"><i class="fa fa-calendar" aria-hidden="true"></i> View My Calendar</a>
                             </td>
                          </tr>
                          <?php
                      }
                      ?>
                   </tbody>
                </table>
             </div>
          </div>
      <?php } ?>

      <div class="animated fadeIn widget grid6">
         
         <?php 
            $all = $this->Models->ratings_query_all();
            $star = $all->num_rows();
         ?>
         <div class="panel-body">
            <div class="row"> 
               <div class="col-sm-4 form-group"> 
                  <div class="input-group"> 
                     <span class="input-group-addon" id="basic-addon3"><span>Date From</span></span> 
                     <input type="text" class="form-control date-from hasDatepicker" id="dp1500424023470"> 
                  </div> 
               </div> 
               <div class="col-sm-4 form-group"> 
                  <div class="input-group"> <span class="input-group-addon" id="basic-addon3"><span>Date To</span></span>
                     <input type="text" class="form-control date-to" disabled="disabled"> 
                  </div> 
               </div> 
               <div class="col-sm-2 form-group"> 
             
               </div> 
               <div class="col-sm-2 form-group"> 
                  <button class="btn btn-success btn-block show-button" disabled="disabled">
                     <i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</button> 
               </div> 
            </div>
            <div class="row">
            
               <div class="col-md-5">
                  <canvas id="chart-area"></canvas>
               </div>
        
            </div>
         </div>
      </div>
      <div class="animated fadeIn widget grid6">
         
         
      </div>
   </div>
   <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>


