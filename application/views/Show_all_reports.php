<?php
if ($this->uri->segment(5) and is_numeric($this->uri->segment(5))) {
    $_u = $this->Models->user_id($this->uri->segment(5));
    if ($_u->num_rows()) {
        $user_position = $this->uri->segment(5);
    } else {
        redirect(site_url());
    }
} else {
    $user_position = $this->session->userdata('user_session');
}
if ($this->Models->validateDate($this->uri->segment(3)) and $this->Models->validateDate($this->uri->segment(4))) {
    $from_id = $this->uri->segment(3);
    $to_id = $this->uri->segment(4);
    ?>
    <div class="content">
        <div class="container-fluid">
            <div class="animated fadeIn widget grid6">
                <div class="panel-body overflow-x">
                    <h3>
                        TOA MARKETING TEAM
                    </h3>
                   <table class="table table-bordered table-striped inbox-table table-type">
                          <tbody>
                             <tr>
                                <th>FROM ( <?php echo $from_id ?> ) TO ( <?php echo $to_id ?> )</th>
                                <th>WIP</th>
                                
                                <th>FOR REVISION</th>
                                <th>APPROVED</th>
                                <th>TOTAL</th>
                             </tr>
                             <?php
                             for ($i = 1; $i <= 7; $i++) {
                                 ?>
                                 <tr>
                                    <td> <?php echo $this->Models->issue_type($i) ?></td>

                                    <?php
                                    for ($a = 1; $a <= 4; $a++) {
                                        ?>
                                        
                                        <?php
                                            if($a == 2){

                                            }else{
                                        ?>    
                                        <td  class="type-<?php echo $a ?>">
                                            <?php
                                            $query = $this->Models->ticket_all_count($i, $a);
                                            echo $query->num_rows();
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    }
                                    ?>
                                    <td class="total-class">
                                        <?php
                                        $tota_query = $this->Models->ticket_it_total_count($i);
                                        

                                        echo $tota_query->num_rows();
                                        ?>
                                    </td>

                                 </tr>

                                 <?php
                             }
                             ?>
                             <tr>
                                <td>&nbsp;</td>
                                <td class="total-1 text-primary">&nbsp;</td>
                                <td class="total-2 text-primary">&nbsp;</td>
                                <td class="total-3 text-primary">&nbsp;</td>
                                <td class="total-4 text-primary">&nbsp;</td>
                                <td class="total-result text-success">&nbsp;</td>
                             </tr>
                          </tbody>
                       </table>
                    <table class="table table-bordered table-striped inbox-table">
                        <thead>
                            <tr>
                                <th>Specific Tasks</th>
                                <th>Type of Issue</th>
                                <th>Status</th>
                                <th>By Priority</th>
                                <th>Sender</th>
                                <th>location</th>
                                <th>Date</th>
                                <th>Date Solved</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            <?php
                            $_query = $this->Models->ticket_assignee_user();
                            $result = $_query->result();
                            foreach ($result as $row) {
                                ?>
                                <tr>
                                    <td><a href="<?php echo base_url('issues_page/issue_id/' . $row->issueid) ?>" class="ellipsis"><?php echo $this->Models->limit_text($row->message, 10) ?></a></td>
                                    <td><?php echo $this->Models->issue_type($row->subject) ?></td>
                                    <td><?php echo $this->Models->issue_status($row->status) ?></td>
                                    <td><?php echo $this->Models->priority($row->priority) ?></td>
                                    <?php
                                    $query_data = $this->Models->user_id($row->sender);
                                    $name_data = $query_data->result();
                                    ?>
                                    <td><?php echo $name_data[0]->fname . ' ' . $name_data[0]->lastname; ?></td>
                                    <td><?php echo $row->location ?></td>
                                    <td><?php echo date('m/d/Y h:i:s A', $row->date) ?></td>
                                    <td><?php echo ($row->date_solve ? date('m/d/Y h:i:s A', $row->date_solve) : '') ?></td>
                                    <td>
                                        <?php echo ($row->date_solve === '' ? ' --- ' : date_create(date('m/d/Y h:i:s A', $row->date))->diff(date_create(date('m/d/Y h:i:s A', $row->date_solve)))->format('%D:%H:%I:%S')) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                                
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
    </div>
    <?php
} else {
    redirect(site_url());
}
