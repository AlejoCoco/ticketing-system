
<div class="content">
   <div class="container-fluid">
      <div class="create-transfer-form animated fadeIn widget grid6" method="post">
         <div class="panel-body overflow-x">
            <form  class="create-table-list row" method="post">
               <div class="col-md-7 ">
                  <div class="row form-group">
                     <div class="col-md-6">
                        <div class="input-group"> 
                           <span class="input-group-addon" id="basic-addon3">Select Status</span> 
                           <select name="category" class="form-control input-inline"> 
                              <option value="">SELECT</option> 
                              <option value="1">SUPPORT</option> 
                              <option value="2">CLIENT</option> 
                           </select> 
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="input-group"> 
                           <span class="input-group-addon" id="basic-addon3"><span>Client Name</span></span> 
                           <input type="text" class="form-control" name="client_name">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5">
                  <button class="btn btn-success pull-right">CREATE PC LIST</button>
               </div>
            </form>

            <form class="inbox-form form-group row" method="get" action="" data-page=""> 
               <div class="col-sm-6"> 
                  <div class="input-group"> 
                     <span class="input-group-addon" id="basic-addon3"><span>Select Status</span></span> 
                     <select name="status" class="form-control select-inbox-status"> 
                        <option value="">SELECT</option> 
                        <option <?php echo ($this->Models->transfer_status() == '1' ? 'selected="selected"' : '') ?> value="1">Support</option> 
                        <option <?php echo ($this->Models->transfer_status() == '2' ? 'selected="selected"' : '') ?> value="2">Client</option>
                     </select> 
                  </div> 
               </div> 
               <div class="col-sm-6"> 
                  <input type="hidden" class="hide" name="page" value="1"> 
                  <div class="input-group"> 
                     <input type="text" class="form-control" name="keyword" value="<?php echo $this->Models->keyword() ?>" placeholder="Search for..."> 
                     <span class="input-group-btn"> <button class="btn btn-default" type="submit">Go!</button> 
                     </span> 
                  </div> 
               </div> 
            </form>

            <div class="row"> 
               <div class="col-sm-4 form-group"> 
                  <div class="input-group"> 
                     <span class="input-group-addon" id="basic-addon3"> <span>Date From</span></span> 
                     <input type="text" class="form-control date-from" id=""> 
                  </div> 
               </div> 
               <div class="col-sm-4 form-group"> 
                  <div class="input-group"> 
                     <span class="input-group-addon" id="basic-addon3"><span>Date To</span></span> 
                     <input type="text" class="form-control date-to" disabled="disabled"> 
                  </div> 
               </div> 
               <div class="col-sm-2 form-group"> 
                  <button class="btn btn-success btn-block export-button-transfer" disabled="disabled"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Report</button> 
               </div> 
               <div class="col-sm-2 form-group"> 
                  <button class="btn btn-default btn-block show-button-transfer" disabled="disabled">
                     <i class="fa fa-file-excel-o" aria-hidden="true"></i> View Report</button>
               </div> 
            </div>

            <table class="inbox-table table table-bordered">
               <thead>
                  <tr>
                     <th class="hide-xs text-center">STATUS</th>
                     <th class="hide-xs description">CLIENT NAME</th>
                     <th class="hide-xs text-center">TRANSFER FROM</th>
                     <th class="ellipsis" style="word-wrap: break-word;" data-original-title="" title="">TRANSFER TO</th>
                     <th class="hide-xs text-center">TRANSFER START</th>
                     <th class="hide-xs text-center">TRANSFER END</th>
                     <th class="hide-xs text-center">DURATION</th>
                     <th class="hide-xs text-center">SENDER</th>
                     <th class="hide-xs text-center"># OF IT</th>
                     <th class="hide-xs text-center"># OF PC</th>
                  </tr>
               </thead>
               <tbody class="">
                   <?php
                   $query = $this->Models->transfer_all();
                   $row = $query->result();
                   $array = array();
                   foreach ($row as $value) {
                       ?>
                      <tr>
                         <td class="text-center"><a href="<?php echo base_url('transfer/issue_id/' . $value->IDT) ?>"><?php echo ($value->client_support == 1 ? 'SUPPORT' : 'CLIENT') ?></a></td>
                         <td class="text-center"><?php echo $value->client_name ?></td>
                         <td class="text-center"><?php echo $value->transfer_from ?></td>
                         <td class="text-center"><?php echo $value->transfer_to ?></td>
                         <td class="text-center"><?php echo ($value->transfer_start == '' ? '' : date('m/d/Y H:i:s', $value->transfer_start)) ?></td>
                         <td class="text-center"><?php echo ($value->transfer_end == '' ? '' : date('m/d/Y H:i:s', $value->transfer_end)) ?></td>
                         <td class="text-center">
                             <?php
                             echo $total = ($value->transfer_end === '' ? '00:00:00' : date_create(date('m/d/Y h:i:s A', $value->transfer_start))->diff(date_create(date('m/d/Y h:i:s A', $value->transfer_end)))->format('%D:%H:%I'));
                             $array[] = $total;
                             ?>
                         </td>
                         <td class="text-center">
                             <?php
                             $_user = $this->Models->user_id($value->transfer_involve);
                             $_userid = $_user->result();
                             echo $_userid[0]->fname . ' ' . $_userid[0]->lastname
                             ?>
                         </td>
                         <td class="text-center">
                             <?php
                             $allit = $this->Models->transfer_assignee($value->IDT);
                             echo $allit->num_rows();
                             ?>

                         </td>
                         <td class="text-center">
                             <?php
                             $allpc = $this->Models->show_add_pc($value->IDT);
                             echo $allpc->num_rows();
                             ?>

                         </td>
                      </tr>
                      <?php
                  }
                  ?>
                  <!--
                                        <tr> 
                                           <td class="text-center" colspan="6"></td> 
                  
                                           <td class="text-center"> 
                  <?php
                  echo $this->Models->AddPlayTime($array);
                  ?>
                                           
                                           </td> 
                  
                                           <td class="text-center" colspan="3"> </td> 
                                        </tr>
                  -->

               </tbody>
            </table>
         </div>
         <?php echo $this->Models->transfer_pagination(); ?>
      </div>
   </div>
   <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>
<script>
    (function (j) {
        j(window).load(function () {

        });
    })(jQuery);
</script>  