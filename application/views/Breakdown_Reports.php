<div class="content">
    <div class="container-fluid">
        <?php
        if(is_numeric($this->uri->segment(3))){
                $query_data =  $this->Models->user_id($this->uri->segment(3));
                $name_data = $query_data->result();
        }else{
            redirect(site_url());
        }
        ?>
        <div class="animated fadeIn widget grid6">
           <div class="widget-heading m-calendar">CALENDAR</div>
            <div class="panel-body overflow-x">
                <h3><?php echo $name_data[0]->fname.' '.$name_data[0]->lastname; ?></h3>
                <div id="calendar" data-bind="<?php echo $this->uri->segment(3) ?>">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>
