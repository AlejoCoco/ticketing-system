</div>
<?php if ($this->session->userdata('user_session')) { ?>
    <div id="profile-picture" class="modal profile-picture-modal" role="dialog">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Profile Picture</h4>
                </div>
                <div class="modal-body">
                    <div class="video-stage">
                  
           
                    </div>
                    <button id="snap" class="btn btn-default btn-photoshoot btn-block">Snap Photo</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($this->session->userdata('user_session')) { ?>
<div class="hide chat-support-container <?php echo (!get_cookie('chat_toggle')?'':get_cookie('chat_toggle')) ?>">
    <div class="chat-support-header"><span class="support-chat-name"><?php echo (!get_cookie('support_chat_name')?'CHAT WITH IT SUPPORT':get_cookie('support_chat_name')) ?></span>  <span class=" hide alert-icon-message label label-danger">New message <i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>
        <a href="#" class="toggle-chat-button pull-right"><i class="fa" aria-hidden="true"></i></a>
    </div>
    <div class="chat-support-toggle">
        <div class="clmn chat-cont-main">
            <div class="chat-message loading" data-conversation="<?php echo get_cookie('chat_cookie_id') ?>" data-message="<?php echo $this->session->userdata('user_session') ?>">

            </div>
            <div class="chat-text">
                <textarea maxlength="300" class="chat-textarea"></textarea>
            </div>
        </div>
        <div class="clmn chat-online-con">
            <div class="search-user">
                <input type="text" class="form-control input-block" placeholder="Search" />
                <button type="button" class="clear-search hide"><i class="fa fa-times"></i></button>
            </div>
        <div class="chat-online"></div>
        </div>
    </div>
</div>
<?php } ?>
<script type="text/javascript">
    (function () {
        EM.script.init();
        EM.javascript.init();
    })(jQuery);
</script>
</body>
</html>