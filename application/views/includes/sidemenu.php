<div class="side-menu">
    <div class="side-menu-content">
     
       <div class="ticketing-user">
          <div class="profile-picture">
             <img class="add-profile" src="<?php echo site_url('request/image_id/' . $this->session->userdata('user_session') . '.jpg') ?>">
          </div>
          <div class="ticketing-username">
              <?php
              $query_data = $this->Models->user_id($this->session->userdata('user_session'));
              $name_data = $query_data->result();
              ?>
              <?php echo $name_data[0]->fname . ' ' . $name_data[0]->lastname; ?>
          </div>
           <?php if ($this->session->userdata('user_position') !== '') { ?>
              <div class="user-status">
                 <select class="user-status-noti">
                    <option<?php echo ($name_data[0]->availability == 0 ? ' selected="selected" ' : '') ?> value="0">Change your status</option>
                    <option<?php echo ($name_data[0]->availability == 1 ? ' selected="selected" ' : '') ?> value="1">I'm on my break</option>
                    <option<?php echo ($name_data[0]->availability == 2 ? ' selected="selected" ' : '') ?> value="2">I'm in a meeting</option>
                    <option<?php echo ($name_data[0]->availability == 3 ? ' selected="selected" ' : '') ?> value="3">I'm out of the office</option>
                    <option<?php echo ($name_data[0]->availability == 4 ? ' selected="selected" ' : '') ?> value="4">I'm on leave</option>
                 </select>
              </div>
          <?php } ?>
       </div>
        <ul>
       
            <li>
                <a href="<?php echo site_url(); ?>">
                    <i class="fa fa-ticket" aria-hidden="true"></i><span>CREATE NEW TICKET</span>
                </a>
            </li>
  
            <li>
               <a href="<?php echo site_url('sent_issues'); ?>">
                  <i class="fa fa-list-alt" aria-hidden="true"></i><span>SENT ISSUES</span>
               </a>
            </li>

            <?php if ($this->session->userdata('user_position') !== '') { ?>
          
            
                <li class="separator"></li>
                <li>
                    <a href="<?php echo site_url('calendar/user/'.$this->session->userdata('user_session')); ?>">
                        <i class="fa  fa-calendar" aria-hidden="true"></i><span>MY CALENDAR</span>
                    </a>
                </li>
               
                 <li>
                  <a href="<?php echo site_url('dashboard'); ?>">
                     <i class="fa fa-bar-chart" aria-hidden="true"></i><span>REPORTS</span>
                  </a>
               </li>
                <li class="separator"></li>
            <?php } ?>
            <li>
                <a href="<?php echo site_url('settings'); ?>">
                    <i class="fa fa-cog" aria-hidden="true"></i><span>SETTINGS</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="side-panel-button fa fa-bars"></div>
</div>  