<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');
header('Content-Type: application/json');

class Request extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Models');
        $this->load->database();
    }

    function index() {
        redirect(site_url());
    }

    function delete_comments() {
        $this->form_validation->set_rules('comment_id', 'comment_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $this->Models->delete_comment($this->input->post('comment_id'));
            $value = array("message" => "success", "text" => "Comment submitted");
            print json_encode($value);
        } else {
            
        }
    }

    function show_comments() {
        $this->form_validation->set_rules('comment_id', 'comment_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $comment = $this->Models->id_comment($this->input->post('comment_id'));
            print json_encode($comment->result());
        } else {
            
        }
    }

    function image_id() {
        $jpg = str_replace('.jpg', '', $this->uri->segment(3));
        if (is_numeric($jpg)) {
            header('Content-type: image/jpg;');
            $seconds_to_cache = 7200;
            $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
            header("Expires: $ts");
            header("Pragma: cache");
            header("Cache-Control: max-age=$seconds_to_cache");

            if (file_exists((FCPATH . 'profile/pro_' . $jpg . '.jpg'))) {
                echo $fileContent = file_get_contents(FCPATH . 'profile/pro_' . $jpg . '.jpg');
            } else {
                echo $fileContent = file_get_contents(FCPATH . 'profile/default.jpg');
            }
        } else {
            
        }
    }

    function capture() {
        if ($this->session->userdata('user_session')) {
            $uid = $this->session->userdata('user_session');

            $fp = fopen('profile/pro_' . $uid . '.jpg', 'w');
            fwrite($fp, $GLOBALS['HTTP_RAW_POST_DATA']);
            fclose($fp);
        }
    }

    function download() {
        if ($this->input->get('file')) {
            $filePath = 'uploads/' . $this->input->get('file');
            if (file_exists($filePath)) {
                $fileName = basename($filePath);
                $fileSize = filesize($filePath);
                header("Cache-Control: private");
                header("Content-Type: application/stream");
                header("Content-Length: " . $fileSize);
                header("Content-Disposition: attachment; filename=" . substr($fileName, 13));
                readfile($filePath);
                exit();
            } else {
                die('The provided file path is not valid.');
            }
        } else {
            die('The provided file path is not valid.');
        }
    }

    function edit_description() {
        $this->form_validation->set_rules('edit_descptn_id', 'edit_descptn_id', 'required|numeric');
        $this->form_validation->set_rules('edit_descptn', 'edit_descptn', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "ID" => $this->input->post('edit_descptn_id'),
                "message" => $this->input->post('edit_descptn')
            );
            $this->Models->edit_dscrptn($data);

            $value = array("message" => "success", "text" => "Description has been updated!");
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }

    function post_comments() {
        $this->form_validation->set_rules('user_id', 'user_id', 'required|numeric');
        $this->form_validation->set_rules('comment_id', 'comment_id', 'required|numeric');
        $this->form_validation->set_rules('comment', 'comment', 'required');

        if ($this->form_validation->run()) {
            $data = array(
                "user_id" => $this->input->post('user_id'),
                "message_id" => $this->input->post('comment_id'),
                "comments" => $this->input->post('comment')
            );
            $this->Models->ticket_comment($data);
            //$this->Models->cc_comment($data);
            $value = array("message" => "success", "text" => "Comment submitted");
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }

    function update_subject() {
        $this->form_validation->set_rules('issue_id', 'issue_id', 'required|numeric');
        $this->form_validation->set_rules('value', 'value', 'required|numeric');
        if ($this->form_validation->run()) {
            $data = array(
                "ID" => $this->input->post('issue_id'),
                "value" => $this->input->post('value')
            );
            $this->Models->ticket_update_subject($data);
            $value = array("message" => "success", "text" => "Subject has been updated");
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }
    function update_sub_category() {
            $this->Models->ticket_update_subcategory($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Other field has been updated");
            print json_encode($value);
    }
    function update_requestby() {
            $this->Models->ticket_update_requestby($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "REquested By Field has been updated");
            print json_encode($value);
    }
     function update_target() {
            $this->Models->ticket_update_target($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Target Fileld has been updated");
            print json_encode($value);
    }
    function update_title() {
            $this->Models->ticket_update_title($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Title has been updated");
            print json_encode($value);
    }
    function update_purpose() {
            $this->Models->ticket_update_purpose($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Purpose has been updated");
            print json_encode($value);
    }
    function update_link() {
            $this->Models->ticket_update_link($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Link to delivery has been updated");
            print json_encode($value);
    }
    function update_requestor() {
            $this->Models->ticket_update_requestor($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Requestor Name has been updated");
            print json_encode($value);
    }
    function update_approvedby() {
            $this->Models->ticket_update_approvedby($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Approved dropdown has been updated");
            print json_encode($value);
    }
    function update_date_required() {
            $this->Models->update_date_required($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "REquired date has been updated");
            print json_encode($value);
    }
    function update_content() {
            $this->Models->update_content($this->input->post('issue_id'), $this->input->post('value'));
             $value = array("message" => "success", "text" => "Content has been updated");
            print json_encode($value);
    }
    function update_availability() {
        $this->form_validation->set_rules('status', 'status', 'required|numeric');
        if ($this->form_validation->run()) {
            $data = array("availability" => $this->input->post('status'));
            $this->Models->user_update_status($data);
            $value = array("message" => "success", "text" => "Status has been updated");
        } else {
            $value = array("message" => "failed", "text" => "Failed");
        }
        print json_encode($value);
    }

    function update_status() {
        $this->form_validation->set_rules('message_status', 'message_status', 'required|numeric');
        $this->form_validation->set_rules('status', 'status', 'required|numeric');
        $this->form_validation->set_rules('sender_update', 'sender_update', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "ID" => $this->input->post('message_status'),
                "status" => $this->input->post('status'),
                "email" => $this->input->post('sender_update')
            );
            $this->Models->ticket_update_status($data);
            $value = array("message" => "success", "text" => "Issue status has been updated", "status" => $data['status'], "mid" => $data['ID']);
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }

    function new_assignee() {
        $this->form_validation->set_rules('issueid', 'issueid', 'required|numeric');
        $this->form_validation->set_rules('assignee', 'assignee', 'required|numeric');
        if ($this->form_validation->run()) {
            $data = array(
                "issueid" => $this->input->post('issueid'),
                "assignee" => $this->input->post('assignee')
            );
            $num = $this->Models->duplicate_assignee($data);
            if ($num->num_rows() === 0) {
                $this->Models->add_new_assignee($data);

                $value = array("message" => "success", "text" => "Success");
            } else {
                $value = array("message" => "duplicate", "text" => "Duplicate");
            }
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }

    function new_cc() {
        $this->form_validation->set_rules('cc_user_id', 'cc_user_id', 'required|numeric');
        $this->form_validation->set_rules('cc_issue_id', 'cc_issue_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $data = array(
                "cc_user_id" => $this->input->post('cc_user_id'),
                "cc_issue_id" => $this->input->post('cc_issue_id')
            );
            $num = $this->Models->duplicate_cc($data);
            if ($num->num_rows() === 0) {
                $this->Models->add_new_cc($data);
                $this->Models->mail_cc($data);
                $value = array("message" => "success", "text" => "Success");
            } else {
                $value = array("message" => "duplicate", "text" => "Duplicate");
            }
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }

    function show_cc() {
        $this->form_validation->set_rules('cc_issue_id', 'cc_issue_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $_cc = $this->Models->cc_ajax($this->input->post('cc_issue_id'));
            print json_encode($_cc->result());
        } else {
            
        }
    }

    function remove_cc() {
        $this->form_validation->set_rules('remove_cc', 'remove_cc', 'required|numeric');
        if ($this->form_validation->run()) {
            $this->Models->delete_cc($this->input->post('remove_cc'));
            $value = array("message" => "success", "text" => "Success");
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }

    function add_assignee() {
        $_query = $this->Models->user_position();
        print json_encode($_query->result());
    }

    function add_cc() {
        $_query = $this->Models->cc_users();
        print json_encode($_query->result());
    }

    function remove_assignee() {
        $this->form_validation->set_rules('remove_assignee', 'remove_assignee', 'required|numeric');
        if ($this->form_validation->run()) {
            $this->Models->delete_assignee($this->input->post('remove_assignee'));
            $value = array("message" => "success", "text" => "Success");
        } else {
            $value = array("message" => "error", "text" => "Error");
        }
        print json_encode($value);
    }

    function show_assignee() {
        $this->form_validation->set_rules('assignee', 'assignee', 'required|numeric');
        if ($this->form_validation->run()) {
            $_assn = $this->Models->ticket_assignee($this->input->post('assignee'));
            print json_encode($_assn->result());
        } else {
            
        }
    }

    function reset_password() {
        $this->form_validation->set_rules('userid', 'userid', 'required|numeric');
        $this->form_validation->set_rules('password', 'repassword', 'required');
        $this->form_validation->set_rules('repassword', 'repassword', 'required');
        if ($this->form_validation->run() === TRUE) {
            if (($this->session->userdata('user_session'))) {
                $data = array("password" => md5(sha1($this->input->post('repassword'))));
                $this->Models->user_password($data, ($this->input->post('userid')));
                $value = array("message" => "success", "text" => "Success");
            } else {
                $value = array("message" => "error", "text" => "Error please login");
            }
        } else {
            $value = array("message" => "error", "text" => "Error please check the information");
        }
        print json_encode($value);
    }

    function attached() {
        if (isset($_FILES['upload'])) {
            $key_id = strtotime("NOW") . rand(10, 99);
            $ext = array("doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "jpg", "jpeg", "JPG", "png", "gif");
            $fname = $_FILES['upload']['name'];
            $tmpFilePath = $_FILES['upload']['tmp_name'];
            move_uploaded_file($tmpFilePath, 'uploads/' . $key_id . '_' . $fname);
            $value = array("uploaded" => "1", "fileName" => "$fname", "url" => site_url('uploads/' . $key_id . '_' . $fname));
            if ($this->input->get('responseType') == 'json') {
                print json_encode($value);
            } else {
                header('Content-Type: text/html; charset=utf-8');
                echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(" . $value['uploaded'] . ",'" . site_url('uploads/' . $key_id . '_' . $fname) . "', '" . $value['fileName'] . "  has been successfully uploaded');</script>";
            }
        }
    }

    function check_ticket_status() {
        $_query = $this->Models->all_ticket_status();
        $json = '[';
        foreach ($_query->result() as $items) {
            $json .= '{';
            $json .= '"key_id":"' . $items->key_id . '",';
            $json .= '"status":"' . $items->status . '",';
            $json .= '"sender":"' . $items->sender . '",';
            $_assn = $this->Models->ticket_assignee($items->ID);
            $json .= '"assignee":"';
            foreach ($_assn->result() as $it) {
                $json .= "<span class='label label-primary'>" . $it->fname . ' (' . $it->location . ')' . "</span> ";
            }
            $json .= '"';
            $json .= '},';
        }
        $json = rtrim($json, ',');
        $json .= ']';
        print $json;
    }

    function create_ticket() {
        $this->form_validation->set_rules('priority', 'priority', 'required');
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('create_ticket', 'create_ticket', 'required');
        $this->form_validation->set_rules('link', 'link', 'required');
        if ($this->form_validation->run()) {
            $key_id = strtotime("NOW") . rand(10, 99);
            $insert = array(
                'priority' => $this->input->post('priority'),
                'subject' => $this->input->post('subject'),
                'message' => $this->input->post('create_ticket'),
                'key_id' => $key_id,
                'sender' => $this->session->userdata('user_session'),
                'date' => strtotime("NOW"),
                'status' => '1',
                'sub_category' => $this->input->post('otherfield'),
                'title' => $this->input->post('Title'),
                'requestby' => $this->input->post('requestby'),
                'link' => $this->input->post('link'),
                'target' => $this->input->post('target'),
                //'content' => $this->input->post('editor1'),
                'date_required' => $this->input->post('required'),
                'content' => "<p>".$this->input->post('contenttextarea')."</p>",
                'requestor' => $this->input->post('requestor'),
                'approvedby' => $this->input->post('approvedby')
                
            );

            if (isset($_FILES['file'])) {
                $total = count($_FILES['file']['name']);
                $ext = array("doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "jpg", "jpeg", "JPG", "png", "gif");
                for ($i = 0; $i < $total; $i++) {
                    $fname = $_FILES['file']['name'][$i];
                    if (in_array(pathinfo($fname, PATHINFO_EXTENSION), $ext)) {
                        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                        move_uploaded_file($tmpFilePath, 'uploads/' . $key_id . '_' . $fname);
                        $array = array(
                            'key_id' => $key_id,
                            'filename' => $key_id . '_' . $_FILES['file']['name'][$i],
                        );
                        $this->Models->new_file($array);
                    }
                }
            }
            $this->Models->create_new_ticket($insert);
            $message_id = $this->Models->latest_ticket($this->session->userdata('user_session'));
            $_id = $message_id->result()[0]->ID;
            $value = array("message" => "success", "text" => "Success", "message_id" => $_id);
            $this->Models->mailer($insert, $_id);
        } else {
            $value = array("message" => "error", "text" => "Status,Type Of Issue and Description are requried");
        }
        print json_encode($value);
    }

    function time() {
        $time = array(
            "strtime" => strtotime('now'),
            "day" => date('d'),
            "year" => date('Y'),
            "month" => date('m'),
            "hour" => date('H'),
            "hours" => date('h'),
            "minutes" => date('i'),
            "seconds" => date('s'),
            "amp" => date('A')
        );
        print json_encode($time);
    }

    function login() {
        $this->form_validation->set_rules('email_address', 'email_address', 'required|valid_email');
        $this->form_validation->set_rules('user_password', 'user_password', 'required');
        if ($this->form_validation->run() === TRUE) {
            $data = array(
                "email" => $this->input->post('email_address'),
                "password" => md5(sha1($this->input->post('user_password'))),
            );
            $query = $this->Models->login_query($data);
            if ($query->num_rows()) {
                $this->session->set_userdata('user_session', $query->result()[0]->ID);
                $this->session->set_userdata('user_position', $query->result()[0]->position);
                $expire = date("m/d/Y H:i:s", strtotime('+30 minutes'));
                $this->Models->login_online(array('online' => strtotime($expire)), $query->result()[0]->ID);
                $value = array(
                    "message" => "success",
                    "text" => "Success!",
                    "type" => "info"
                );
            } else {
                $value = array(
                    "message" => "error",
                    "text" => "Invalid email or password",
                    "type" => "warning"
                );
            }
            print json_encode($value);
        } else {
            redirect(site_url());
        }
    }

    function update_date() {
        $this->form_validation->set_rules('_date_id', '_date_id', 'required');
        $this->form_validation->set_rules('_submited_date', '_submited_date', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "date_id" => $this->input->post('_date_id'),
                "submited_date" => $this->input->post('_submited_date'),
            );
            $this->Models->update_date_submitted($data);

            $value = array(
                "message" => "success",
                "text" => "Date successfully updated.",
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function logout() {
        $this->Models->login_online(array('online' => '0'), $this->session->userdata('user_session'));
        $this->session->unset_userdata('user_session');
        $this->session->unset_userdata('user_position');
        $value = array(
            "message" => "success",
            "text" => "logout",
        );
        print json_encode($value);
    }

    function select_year() {
        $this->form_validation->set_rules('select_year', 'select_year', 'required');
        if ($this->form_validation->run()) {
            $this->input->post('select_year');
        } else {
            return date('Y');
        }
    }

    function select_month() {
        $this->form_validation->set_rules('select_month', 'select_month', 'required');
        if ($this->form_validation->run()) {
            return $this->input->post('select_month');
        } else {
            return date('n');
        }
    }

    function year_report() {
        $total = 0;
        $_query = $this->Models->all_tickets();
        $result = $_query->result();
        $val = array();
        foreach ($result as $row) {
            if (date('Y', $row->date) == ($this->input->post('select_year') ? $this->input->post('select_year') : date("Y"))) {
                for ($i = 1; $i <= 12; $i++) {
                    if (date('n', $row->date) == $i) {
                        $val[] .= $i;
                    }
                }
            }
        }
        $vals = array_count_values($val);
        ksort($vals);
        $label = '{' . "\n";
        $label .= '"label":';
        $label .= '[';
        foreach ($vals as $keys => $val) {
            $dt = DateTime::createFromFormat('!m', $keys);
            $number = $dt->format('F');
            $label .= '"' . $number . ' (' . $vals[$keys] . ')",';
            $total += $vals[$keys];
        }
        $label = rtrim($label, ',');
        $label .= ']';
        $label .= ",";
        $label .= "\n";
        $label .= '"data":';
        $label .= '[';
        foreach ($vals as $key => $value) {
            $label .= '"' . $value . '",';
        }
        $label = rtrim($label, ',');
        $label .= '],' . "\n";
        $label .= '"monthly":["MONTHLY TOTAL ISSUES (' . $total . ')"]' . "\n";
        $label .= '}';
        print $label;
    }

    function widget_number() {
        $this->form_validation->set_rules('widget_num', 'widget_num', 'required');
        if ($this->form_validation->run()) {
            $_open = $this->Models->ticket_status($this->input->post('widget_num'));
            print json_encode(array("number" => $_open->num_rows()));
        }
    }

    function month_report() {
        $total = 0;
        $_query = $this->Models->all_tickets();
        $result = $_query->result();
        $val = array();
        foreach ($result as $row) {
            if (date('n', $row->date) == $this->input->post('select_month') and date('Y', $row->date) == $this->input->post('select_year')) {
                for ($i = 1; $i <= 31; $i++) {
                    if (date('d', $row->date) == $i) {
                        $val[] .= $i;
                    }
                }
            }
        }
        $vals = array_count_values($val);
        ksort($vals);
        $label = '{' . "\n";
        $label .= '"label":';
        $label .= '[';
        foreach ($vals as $keys => $val) {
            $label .= '"' . $keys . '",';
        }
        $label = rtrim($label, ',');
        $label .= ']';
        $label .= ",";
        $label .= "\n";
        $label .= '"data":';
        $label .= '[';
        foreach ($vals as $key => $value) {
            $label .= '"' . $value . '",';
            $total += $value;
        }
        $label = rtrim($label, ',');
        $label .= '],' . "\n";


        $dateObj = DateTime::createFromFormat('!m', $this->input->post('select_month'));
        $monthName = $dateObj->format('F');

        $label .= '"daily":["TOTAL ISSUES FOR THE MONTH OF ' . strtoupper($monthName) . ' (' . $total . ')"]' . "\n";
        $label .= '}';
        print $label;
    }

    function bgcolor($color) {
        if ($color == 1) {
            return "rgba(169, 68, 66, 0.80)";
        } else if ($color == 2) {
            return "rgb(49, 112, 143)";
        } else if ($color == 3) {
            return "rgb(138, 109, 59)";
        } else if ($color == 4) {
            return "rgb(157, 203, 57)";
        } else if ($color == 5) {
            return "rgb(255, 152, 0)";
        } else if ($color == 6) {
            return "rgb(76, 175, 80)";
        } else if ($color == 7) {
            return "rgb(156, 39, 176)";
        }
    }

    function issue_report() {
        $_query = $this->Models->all_tickets();
        $result = $_query->result();
        $val = array();
        $total = 0;
        $json = '';
        foreach ($result as $row) {
            for ($i = 1; $i <= 7; $i++) {
                if ($row->subject == $i) {
                    $val[] = $row->subject;
                }
            }
            $total = ($total + 1);
        }
        $vals = array_count_values($val);
        ksort($vals);
        print '{';
        print '"label":[],';
        print '"data":[';
        print "\n";
        foreach ($vals as $key => $value) {
            for ($i = 1; $i <= 7; $i++) {
                if ($key == $i) {
                    $json .= '{"label": "' . $this->Models->issue_type($i) . ' (' . round(($vals[$key] / $total * 100), 2) . '%)", "backgroundColor":"' . $this->bgcolor($i) . '","borderColor":"' . $this->bgcolor($i) . '","borderWidth": 1,"data": [' . ($vals[$key] / $total * 100) . ']},';
                }
            }
        }
        $json = rtrim($json, ',');
        print $json;
        print "]";
        print "}";
    }

    function ajax_issue() {
        $_query = $this->Models->ticket_status_all();
        print json_encode($_query->result());
    }

    function status_report() {
        $_query = $this->Models->all_tickets();
        $result = $_query->result();
        $val = array();
        $total = 0;
        $json = '';
        foreach ($result as $row) {
            for ($i = 1; $i <= 4; $i++) {
                if ($row->status == $i) {
                    $val[] = $row->status;
                }
            }
            $total = ($total + 1);
        }
        $vals = array_count_values($val);
        ksort($vals);
        print '{';
        print '"label":[],';
        print '"data":[';
        print "\n";
        foreach ($vals as $key => $value) {
            for ($i = 1; $i <= 4; $i++) {
                if ($key == $i) {
                    $json .= '{"label": "' . $this->Models->issue_string($i) . ' (' . round(($vals[$key] / $total * 100), 2) . '%)' . '", "backgroundColor":"' . $this->bgcolor($i) . '","borderColor":"' . $this->bgcolor($i) . '","borderWidth": 1,"data": [' . $vals[$i] . ']},';
                }
            }
        }
        $json = rtrim($json, ',');
        print $json;
        print "]";
        print "}";
    }

    function itkpi() {

        if ($this->uri->segment(5) and is_numeric($this->uri->segment(5))) {
            $_u = $this->Models->user_id($this->uri->segment(5));
            if ($_u->num_rows()) {
                $user_position = $this->uri->segment(5);
            } else {
                redirect(site_url());
            }
        } else {
            $user_position = $this->session->userdata('user_session');
        }
        require_once APPPATH . "/PHPExcel/PHPExcel.php";
        $default_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '1b5a86')
                )
            )
        );
        $default_style = array(
            'font' => array(
                'name' => 'Verdana',
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size' => 9
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '1b5a86')
                )
            )
        );
        $styleGray = array(
            'font' => array(
                'name' => 'Verdana',
                'bold' => true,
                'color' => array('rgb' => '333333'),
                'size' => 12,
        ));
        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size' => 10,
        ));
        $textcolor = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '444444'),
                'size' => 11,
        ));
        $textred = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'C70000'),
                'size' => 11,
        ));
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("TOA SYSTEM");
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getStyle('A3:A9')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '1e4457')
                    )
                )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '1e4457')
                    )
                )
        );
        $objPHPExcel->getActiveSheet()->getStyle("A1:F9")->applyFromArray($default_border);
        $objPHPExcel->getActiveSheet()->getStyle("A11:I11")->applyFromArray($default_style);
        $objPHPExcel->getActiveSheet()->getStyle("A3:A9")->applyFromArray($default_style);
        $objPHPExcel->getActiveSheet()->getStyle("A2:F2")->applyFromArray($textcolor);
        $objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($styleGray);
        $objPHPExcel->getActiveSheet()->getStyle('A2:A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B2:F9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A10:F10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $query_data = $this->Models->user_id($user_position);
        $name_data = $query_data->result();
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', $name_data[0]->fname . ' ' . $name_data[0]->lastname)
                ->setCellValue('A2', 'FROM (' . $this->uri->segment(3) . ') TO (' . $this->uri->segment(4) . ')')
                ->setCellValue('A3', 'GRAPHICS')
                ->setCellValue('A4', 'VIDEO')
                ->setCellValue('A5', 'PHOTO')
                ->setCellValue('A6', 'CONTENT')
                ->setCellValue('A7', 'OTHERS');
              
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B2', 'OPEN')
                ->setCellValue('C2', 'RE OPEN')
                ->setCellValue('D2', 'ON GOING')
                ->setCellValue('E2', 'RESOLVED')
                ->setCellValue('F2', 'TOTAL')
                ->setCellValue('A10', 'TOTAL')
                ->setCellValue('B10', '=SUM(B3:B9)')
                ->setCellValue('C10', '=SUM(C3:C9)')
                ->setCellValue('D10', '=SUM(D3:D9)')
                ->setCellValue('E10', '=SUM(E3:E9)')
                ->setCellValue('F10', '=SUM(F3:F9)');


        $stut = 2;
        $open = 2;
        $reopen = 2;
        $ongoing = 2;
        $resolved = 2;
        for ($i = 1; $i <= 7; $i++) {
            $stut++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $stut, $this->Models->issue_type($i));
            for ($a = 1; $a <= 4; $a++) {

                if ($a === 1) {
                    $query = $this->Models->ticket_assignee_count($i, $a);
                    $open++;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $open, $query->num_rows());
                }
                if ($a === 2) {
                    $query = $this->Models->ticket_assignee_count($i, $a);
                    $reopen++;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $reopen, $query->num_rows());
                }
                if ($a === 3) {
                    $query = $this->Models->ticket_assignee_count($i, $a);
                    $ongoing++;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $ongoing, $query->num_rows());
                }
                if ($a === 4) {
                    $query = $this->Models->ticket_assignee_count($i, $a);
                    $resolved++;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $resolved, $query->num_rows());
                }
            }
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F3', "=SUM(B3:E3)");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F4', "=SUM(B4:E4)");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F5', "=SUM(B5:E5)");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F6', "=SUM(B6:E6)");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F7', "=SUM(B7:E7)");
       



        $tracker = 11;
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $tracker, 'Specific Tasks')
                ->setCellValue('B' . $tracker, 'Trackting Number')
                ->setCellValue('C' . $tracker, 'Type of Issue')
                ->setCellValue('D' . $tracker, 'Status')
                ->setCellValue('E' . $tracker, 'By Priority')
                ->setCellValue('F' . $tracker, 'Sender')
                ->setCellValue('G' . $tracker, 'Date')
                ->setCellValue('H' . $tracker, 'Date Solved')
                ->setCellValue('I' . $tracker, 'Total');



        $_query = $this->Models->ticket_assignee_user();
        $result = $_query->result();
        foreach ($result as $row) {
            $tracker++;
            $_query_data = $this->Models->user_id($row->sender);
            $name_data = $_query_data->result();
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $tracker, $this->Models->limit_text($row->message, 10))
                    ->setCellValue('B' . $tracker, '`' . $row->key_id . '`')
                    ->setCellValue('C' . $tracker, $this->Models->issue_type($row->subject))
                    ->setCellValue('D' . $tracker, $this->Models->issue_string($row->status))
                    ->setCellValue('E' . $tracker, $this->Models->priority_string($row->priority))
                    ->setCellValue('F' . $tracker, $name_data[0]->fname . ' ' . $name_data[0]->lastname)
                    ->setCellValue('G' . $tracker, date('m/d/Y h:i:s A', $row->date))
                    ->setCellValue('H' . $tracker, ($row->date_solve ? date('m/d/Y h:i:s A', $row->date_solve) : ''))
                    ->setCellValue('I' . $tracker, ($row->date_solve === '' ? ' --- ' : date_create(date('m/d/Y h:i:s A', $row->date))->diff(date_create(date('m/d/Y h:i:s A', $row->date_solve)))->format('%D:%H:%I:%S')));
        }




        $objPHPExcel->getActiveSheet()->getStyle("A11:I" . $tracker)->applyFromArray($default_border);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $name_data[0]->fname . '_' . $name_data[0]->lastname . '_' . $this->uri->segment(3) . '_' . $this->uri->segment(4) . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    function allitkpi() {
        if ($this->uri->segment(3) and $this->uri->segment(4)) {
            require_once APPPATH . "/PHPExcel/PHPExcel.php";
            $default_border = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '1b5a86')
                    )
                )
            );
            $default_style = array(
                'font' => array(
                    'name' => 'Verdana',
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                    'size' => 9
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '1b5a86')
                    )
                )
            );
            $styleGray = array(
                'font' => array(
                    'name' => 'Verdana',
                    'bold' => true,
                    'color' => array('rgb' => '333333'),
                    'size' => 12,
            ));
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                    'size' => 10,
            ));
            $textcolor = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '444444'),
                    'size' => 11,
            ));
            $textred = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'C70000'),
                    'size' => 11,
            ));
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("TOA SYSTEM");
            $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getStyle('A3:A9')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '1e4457')
                        )
                    )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '1e4457')
                        )
                    )
            );
            $objPHPExcel->getActiveSheet()->getStyle("A1:F9")->applyFromArray($default_border);
            $objPHPExcel->getActiveSheet()->getStyle("A11:I11")->applyFromArray($default_style);
            $objPHPExcel->getActiveSheet()->getStyle("A3:A9")->applyFromArray($default_style);
            $objPHPExcel->getActiveSheet()->getStyle("A2:F2")->applyFromArray($textcolor);
            $objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($styleGray);
            $objPHPExcel->getActiveSheet()->getStyle('A2:A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B2:F9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A10:F10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'IT SUPPORT TEAM')
                    ->setCellValue('A2', 'FROM (' . $this->uri->segment(3) . ') TO (' . $this->uri->segment(4) . ')')
                    ->setCellValue('A3', 'GRAPHICS')
                    ->setCellValue('A4', 'VIDEO')
                    ->setCellValue('A5', 'PHOTO')
                    ->setCellValue('A6', 'CONTENT')
                    ->setCellValue('A7', 'OTHERS');        
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B2', 'OPEN')
                    ->setCellValue('C2', 'RE OPEN')
                    ->setCellValue('D2', 'ON GOING')
                    ->setCellValue('E2', 'RESOLVED')
                    ->setCellValue('F2', 'TOTAL')
                    ->setCellValue('A10', 'TOTAL')
                    ->setCellValue('B10', '=SUM(B3:B9)')
                    ->setCellValue('C10', '=SUM(C3:C9)')
                    ->setCellValue('D10', '=SUM(D3:D9)')
                    ->setCellValue('E10', '=SUM(E3:E9)')
                    ->setCellValue('F10', '=SUM(F3:F9)');


            $stut = 2;
            $open = 2;
            $reopen = 2;
            $ongoing = 2;
            $resolved = 2;
            for ($i = 1; $i <= 7; $i++) {
                $stut++;
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $stut, $this->Models->issue_type($i));
                for ($a = 1; $a <= 4; $a++) {

                    if ($a === 1) {
                        $queryopen = $this->Models->ticket_all_count($i, $a);
                        $open++;
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $open, $queryopen->num_rows());
                    }

                    if ($a === 2) {
                        $queryreopen = $this->Models->ticket_all_count($i, $a);
                        $reopen++;
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $reopen, $queryreopen->num_rows());
                    }

                    if ($a === 3) {
                        $queryongoing = $this->Models->ticket_all_count($i, $a);
                        $ongoing++;
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $ongoing, $queryongoing->num_rows());
                    }

                    if ($a === 4) {
                        $query = $this->Models->ticket_all_count($i, $a);
                        $resolved++;
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $resolved, $query->num_rows());
                    }
                }
            }
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F3', "=SUM(B3:E3)");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F4', "=SUM(B4:E4)");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F5', "=SUM(B5:E5)");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F6', "=SUM(B6:E6)");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F7', "=SUM(B7:E7)");
            

            $tracker = 11;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $tracker, 'Specific Tasks')
                    ->setCellValue('B' . $tracker, 'Trackting Number')
                    ->setCellValue('C' . $tracker, 'Type of Issue')
                    ->setCellValue('D' . $tracker, 'Status')
                    ->setCellValue('E' . $tracker, 'By Priority')
                    ->setCellValue('F' . $tracker, 'Sender')
                    ->setCellValue('G' . $tracker, 'Date')
                    ->setCellValue('H' . $tracker, 'Date Solved')
                    ->setCellValue('I' . $tracker, 'Total');

            $_query = $this->Models->ticket_all_user();
            $result = $_query->result();
            foreach ($result as $row) {
                $tracker++;
                $_query_data = $this->Models->user_id($row->sender);
                $name_data = $_query_data->result();
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $tracker, $this->Models->limit_text($row->message, 10))
                        ->setCellValue('B' . $tracker, '`' . $row->key_id . '`')
                        ->setCellValue('C' . $tracker, $this->Models->issue_type($row->subject))
                        ->setCellValue('D' . $tracker, $this->Models->issue_string($row->status))
                        ->setCellValue('E' . $tracker, $this->Models->priority_string($row->priority))
                        ->setCellValue('F' . $tracker, @$name_data[0]->fname . ' ' . @$name_data[0]->lastname)
                        ->setCellValue('G' . $tracker, date('m/d/Y h:i:s A', $row->date))
                        ->setCellValue('H' . $tracker, ($row->date_solve ? date('m/d/Y h:i:s A', $row->date_solve) : ''))
                        ->setCellValue('I' . $tracker, ($row->date_solve === '' ? ' --- ' : date_create(date('m/d/Y h:i:s A', $row->date))->diff(date_create(date('m/d/Y h:i:s A', $row->date_solve)))->format('%D:%H:%I')));
            }
            $objPHPExcel->getActiveSheet()->getStyle("A11:I" . $tracker)->applyFromArray($default_border);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . 'IT_SUPPORT_TEAM_' . $this->uri->segment(3) . '_' . $this->uri->segment(4) . '.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        }
    }

    function calendar() {
        $array = '';
        if ($this->uri->segment(3) and is_numeric($this->uri->segment(3))) {
            $value = $this->Models->user_calendar($this->uri->segment(3));
            foreach ($value->result() as $row) {
                $array .= '{"status":"' . $row->status . '","description":"' . trim(preg_replace('/\s\s+/', ' ', $this->Models->limit_text($row->message, 50))) . '","url":"' . site_url('issues_page/issue_id/' . $row->ID) . '","title":"' . trim(preg_replace('/\s\s+/', ' ', $this->Models->limit_text($row->message, 10))) . '","start":"' . date('Y-m-d H:i:s', $row->date) . '"' . ($row->date_solve ? ',"end":"' . date('Y-m-d H:i:s', $row->date_solve) . '"' : '') . '},';
            }
            $array = rtrim($array, ',');
            print "[" . $array . "]";
        }
    }

    function update_rdate() {
        $this->form_validation->set_rules('_date_id', '_date_id', 'required');
        $this->form_validation->set_rules('_submited_date', '_submited_date', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "date_id" => $this->input->post('_date_id'),
                "submited_date" => $this->input->post('_submited_date'),
            );
            $this->Models->update_rdate_submitted($data);
            $value = array(
                "message" => "success",
                "text" => "Date successfully updated.",
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function profile() {
        $this->form_validation->set_rules('imgbase64', 'imgbase64', 'required');
        if ($this->form_validation->run()) {
            $rawData = $this->input->post('imgbase64');
            $filteredData = explode(',', $rawData);
            $unencoded = base64_decode($filteredData[1]);
            $fp = fopen('profile/pro_' . $this->session->userdata('user_session') . '.png', 'w');
            fwrite($fp, $unencoded);
            fclose($fp);
            $fn = 'profile/pro_' . $this->session->userdata('user_session') . '.png';
            $width = 200;
            $height = 150;
            $size = getimagesize($fn);
            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor(($width - 50), $height);
            imagecopyresampled($dst, $src, -25, 0, 0, 0, ($width), $height, $size[0], $size[1]);
            imagedestroy($src);
            imagepng($dst, $fn);
            imagedestroy($dst);
            $value = array(
                "message" => "success",
                "text" => "Image has been loaded.",
                "image" => $fn,
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function chat_to_me() {
        $data = $this->Models->my_chat_support();
        print json_encode($data->result());
    }

    function default_page() {
        $this->form_validation->set_rules('page', 'page', 'required|numeric');
        if ($this->form_validation->run()) {
            return $this->input->post('page');
        } else {
            return 1;
        }
    }

    function chat_ajax() {
        $user = array(
            'chat_id' => $this->input->post('conversation'),
            'page' => $this->input->post('page')
        );
        $data = $this->Models->chat_support($user);
        foreach ($data->result() as $row) {
            if ($row->from_id === $this->input->post('conversation')) {
                $this->Models->chat_update_read($row->ID);
            }
        }
        print json_encode($data->result());
    }

    function update_online_support() {
        $expire = date("m/d/Y H:i:s", strtotime('+30 minutes'));
        $this->Models->login_online(array('online' => strtotime($expire)), $this->session->userdata('user_session'));
        $data = array('status' => 'update');
        print json_encode($data);
    }

    function chat_online_support() {
        $expire = date("m/d/Y H:i:s", strtotime('+30 minutes'));
        $this->Models->login_online(array('online' => strtotime($expire)), $this->session->userdata('user_session'));
    }

    function chat_msg() {
        $this->form_validation->set_rules('userid', 'userid', 'required|numeric');
        $this->form_validation->set_rules('message', 'message', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                'from_id' => $this->session->userdata('user_session'),
                'to_id' => $this->input->post('userid'),
                'chat_message' => strip_tags(htmlspecialchars($this->input->post('message'))),
                'read' => 0,
                'date' => strtotime('now')
            );
            $this->Models->chat_insert($data);
            print json_encode($data);
            $this->chat_online_support();
        } else {
            print json_encode(array('from_id' => $this->session->userdata('user_session')));
        }
    }

    function position_support() {
        $_it = $this->Models->online_support_and_user();
        $data = '';
        foreach ($_it->result() as $row) {

            $data .= '{"ID":"' . $row->ID . '",';
            $data .= '"status":"' . ($row->online > strtotime('now') ? 'online' : 'offline') . '",';
            $data .= '"fname":"' . $row->fname . '",';
            $data .= '"lastname":"' . $row->lastname . '",';
            $data .= '"location":"' . $row->location . '",';
            $data .= '"job_title":"' . $row->job_title . '"},';
        }
        $data = rtrim($data, ',');
        print '[' . $data . ']';
    }

    function ronline() {
        $_it = $this->Models->test_online();
        print json_encode($_it->result());
    }

    function on_search() {
        $_it = $this->Models->online_support_and_user($this->input->post('search'));
        print json_encode($_it->result());
    }

    function create_transfer_list() {
        $this->form_validation->set_rules('category', 'category', 'required|numeric');
        if ($this->form_validation->run()) {
            $data = array(
                'client_support' => $this->input->post('category'),
                'client_name' => $this->input->post('client_name'),
                'transfer_start' => strtotime('now'),
                'transfer_involve' => $this->session->userdata('user_session')
            );
            $this->Models->create_transfer($data);

            $result = $this->Models->created_transfer();
            $_id = $result->result()[0]->IDT;
            $value = array(
                "message" => "success",
                "text" => "Success.",
                "message_id" => $_id,
                "type" => "info"
            );
            print json_encode($value);
        } else {
            
        }
    }

    function update_transfer_list() {
        $this->form_validation->set_rules('trans_id', 'trans_id', 'required|numeric');
        $this->form_validation->set_rules('category', 'category', 'required');
        $this->form_validation->set_rules('transfe_from', 'transfe_from', 'required');
        $this->form_validation->set_rules('transfer_to', 'transfer_to', 'required');
        $this->form_validation->set_rules('transfer_start', 'transfer_start', 'required');
        $this->form_validation->set_rules('transfer_end', 'transfer_end', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                'client_support' => $this->input->post('category'),
                'client_name' => $this->input->post('client_name'),
                'transfer_from' => $this->input->post('transfe_from'),
                'transfer_to' => $this->input->post('transfer_to'),
                'transfer_start' => strtotime($this->input->post('transfer_start')),
                'transfer_end' => strtotime($this->input->post('transfer_end')),
            );
            $this->Models->update_transfer_($data, $this->input->post('trans_id'));
            $value = array(
                "message" => "success",
                "text" => "Information successfully updated.",
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function show_create_list() {
        $result = $this->Models->created_transfer();
        print json_encode($result->result());
    }

    function transfer_u_list() {
        $this->form_validation->set_rules('trans_id', 'trans_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $query = $this->Models->transfer_assignee($this->uri->segment(3));
            print json_encode($query->result());
        }
    }

    function new_list_assignee() {
        $this->form_validation->set_rules('issueid', 'issueid', 'required|numeric');
        $this->form_validation->set_rules('assignee', 'assignee', 'required|numeric');

        if ($this->form_validation->run()) {
            $data = array(
                'transferid' => $this->input->post('issueid'),
                'transferuserit' => $this->input->post('assignee'),
            );
            $checkadd = $this->Models->check_transfer_assignee($data);
            if ($checkadd->num_rows() === 0) {
                $this->Models->new_transfer_assignee($data);
                $value = array(
                    "message" => "success",
                    "text" => "Information successfully updated.",
                    "type" => "info"
                );
                print json_encode($value);
            } else {
                $value = array(
                    "message" => "duplicate",
                    "text" => "Information successfully updated.",
                    "type" => "info"
                );
                print json_encode($value);
            }
        }
    }

    function delete_transfer_assignee() {
        $this->form_validation->set_rules('issueid', 'issueid', 'required|numeric');
        if ($this->form_validation->run()) {
            $this->Models->delete_trans_assignee($this->input->post('issueid'));
            $value = array(
                "message" => "success",
                "text" => "Deleted",
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function show_transfer_assignee() {
        $this->form_validation->set_rules('issueid', 'issueid', 'required|numeric');
        if ($this->form_validation->run()) {
            $query = $this->Models->transfer_assignee($this->input->post('issueid'));
            print json_encode($query->result());
        }
    }

    function add_pc_uname() {

        $this->form_validation->set_rules('transfer_id', 'transfer_id', 'required|numeric');
        $this->form_validation->set_rules('user_pc_name', 'user_pc_name', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "transfer_id" => $this->input->post('transfer_id'),
                "user_pc_name" => $this->input->post('user_pc_name'),
            );
            $this->Models->add_pc_name($data);
            $value = array(
                "message" => "success",
                "text" => "Deleted",
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function delete_added_pc() {
        $this->form_validation->set_rules('transfer_id', 'transfer_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $this->Models->delete_add_pc($this->input->post('transfer_id'));
            $value = array(
                "message" => "success",
                "text" => "Deleted",
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function show_added_pc() {
        $this->form_validation->set_rules('transfer_id', 'transfer_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $query = $this->Models->show_add_pc($this->input->post('transfer_id'));
            print json_encode($query->result());
        }
    }

    function export_transfer() {
        if ((!$this->uri->segment(3)) or ( !$this->uri->segment(4))) {
            redirect(site_url('transfer_list'));
        }


        require_once APPPATH . "/PHPExcel/PHPExcel.php";
        $default_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '1b5a86')
                )
            )
        );
        $default_style = array(
            'font' => array(
                'name' => 'Verdana',
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size' => 9
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '1b5a86')
                )
            )
        );
        $styleGray = array(
            'font' => array(
                'name' => 'Verdana',
                'bold' => true,
                'color' => array('rgb' => '333333'),
                'size' => 12,
        ));
        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size' => 10,
        ));
        $textcolor = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '444444'),
                'size' => 11,
        ));
        $textred = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'C70000'),
                'size' => 11,
        ));
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("TOA SYSTEM");
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '1e4457')
                    )
                )
        );
        $objPHPExcel->getActiveSheet()->getStyle("A1:J1")->applyFromArray($default_border);
        $objPHPExcel->getActiveSheet()->getStyle("A1:J1")->applyFromArray($styleArray);




        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'STATUS')
                ->setCellValue('B1', 'CLIENT NAME')
                ->setCellValue('C1', 'TRANSFER FROM')
                ->setCellValue('D1', 'TRANSFER TO')
                ->setCellValue('E1', 'TRANSFER START')
                ->setCellValue('F1', 'TRANSFER END')
                ->setCellValue('G1', 'DURATION D:H:M')
                ->setCellValue('H1', 'SENDER')
                ->setCellValue('I1', '# OF IT')
                ->setCellValue('J1', '# OF PC');


        $query = $this->Models->transfer_all_date();
        $row = $query->result();
        $array = array();
        $total = 0;
        $all = 0;
        $tracker = 1;
        foreach ($row as $value) {
            $tracker++;
            $total = ($value->transfer_end === '' ? '00:00:00' : date_create(date('m/d/Y h:i:s A', $value->transfer_start))->diff(date_create(date('m/d/Y h:i:s A', $value->transfer_end)))->format('%D:%H:%I'));
            $min = $total;
            $array[] = $total;
            $_user = $this->Models->user_id($value->transfer_involve);
            $_userid = $_user->result();

            $allit = $this->Models->transfer_assignee($value->IDT);
            $it = $allit->num_rows();

            $allpc = $this->Models->show_add_pc($value->IDT);
            $_total = $allpc->num_rows();
            $all += $_total;
            $objPHPExcel->getActiveSheet()->getStyle("A" . $tracker . ":J" . $tracker)->applyFromArray($default_border);
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $tracker, ($value->client_support == 1 ? 'SUPPORT' : 'CLIENT'))
                    ->setCellValue('B' . $tracker, $value->client_name)
                    ->setCellValue('C' . $tracker, $value->transfer_from)
                    ->setCellValue('D' . $tracker, $value->transfer_to)
                    ->setCellValue('E' . $tracker, ($value->transfer_start == '' ? '' : date('m/d/Y H:i:s', $value->transfer_start)))
                    ->setCellValue('F' . $tracker, ($value->transfer_end == '' ? '' : date('m/d/Y H:i:s', $value->transfer_end)))
                    ->setCellValue('G' . $tracker, $total)
                    ->setCellValue('H' . $tracker, $_userid[0]->fname . ' ' . $_userid[0]->lastname)
                    ->setCellValue('I' . $tracker, $it)
                    ->setCellValue('J' . $tracker, $_total);
        }
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('G' . ($tracker + 1), $this->Models->AddPlayTime($array))
                ->setCellValue('J' . ($tracker + 1), $all);



        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . 'IT_SUPPORT_TEAM_' . $this->uri->segment(3) . '_' . $this->uri->segment(4) . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    function ratings() {

        $this->form_validation->set_rules('sender_rate', 'sender_rate', 'required|numeric');
        $this->form_validation->set_rules('sender_id', 'sender_id', 'required|numeric');
        $this->form_validation->set_rules('issue_id', 'issue_id', 'required|numeric');
        $this->form_validation->set_rules('suggestion', 'suggestion', 'required');

        if ($this->form_validation->run()) {
            $data = array(
                "rate_number" => $this->input->post('sender_rate'),
                "sender_id" => $this->input->post('sender_id'),
                "issue_id" => $this->input->post('issue_id'),
                "rate_comment" => addslashes($this->input->post('suggestion')),
                "rate_date" => strtotime('now'),
            );

            $num = $this->Models->check_ratings_issues($data['issue_id']);
            if ($num->num_rows() === 0) {
                $this->Models->ratings_issues($data);
            } else {
                $this->Models->ratings_update($data, $data['issue_id']);
            }

            $value = array(
                "message" => "success",
                "text" => addslashes($this->input->post('suggestion')),
                "type" => "info"
            );
            print json_encode($value);
        }
    }

    function dashboard_month() {
        header('Content-Type: text/html; charset=utf-8');
        $this->form_validation->set_rules('select_month', 'month', 'required');
        $this->form_validation->set_rules('select_year', 'year', 'required');
        if ($this->form_validation->run()) {
            ?>
            <table border="1" class="table table-bordered table-striped inbox-table table-type">
               <tbody>
                  <tr>
                     <th>MONTH</th>
                     <th>In Progress</th>
                     <th>Completed</th>
                     <th>Return to Sender</th>
                     <th>TOTAL</th>
                  </tr>
            <?php
            for ($i = 1; $i <= 5; $i++) {
                ?>
                      <tr>
                         <td> <?php echo $this->Models->issue_type($i) ?></td>

                <?php
                for ($a = 1; $a <= 4; $a++) {
                    ?>
                            <?php if($a == 2){
                                echo "";
                            }else{

                            ?>

                            <td  class="type-<?php echo $a ?>">
                             <?php
                             $query = $this->Models->dash_all_count($this->input->post('select_year'), $this->input->post('select_month'), $i, $a);
                             echo $query->num_rows();
                             ?>
                             </td>
                                 <?php
                             }
                             }
                             ?>
                              <?php if($a == 2){
                                echo "";
                            }else{

                            ?>
                         <td class="total-class">
                         <?php
                         $tota_query = $this->Models->dash_total_count($this->input->post('select_year'), $this->input->post('select_month'), $i);
                         echo $tota_query->num_rows();
                         ?>
                         </td>
                         <?php 
                            }
                         ?>

                      </tr>

                <?php
            }
            ?>
                  <tr>
                     <td>&nbsp;</td>
                     <td class="total-1 text-primary">&nbsp;</td>
                    
                     <td class="total-3 text-primary">&nbsp;</td>
                     <td class="total-4 text-primary">&nbsp;</td>
                     <td class="total-result text-success">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
            <?php
        }
    }

    function ratings_report() {
        $all = $this->Models->ratings_query_all();
        $star = $all->num_rows();
        $num1 = $this->Models->ratings_query('1');
        $num2 = $this->Models->ratings_query('2');
        $num3 = $this->Models->ratings_query('3');
        $num4 = $this->Models->ratings_query('4');
        $num5 = $this->Models->ratings_query('5');
        ?>
        {
        "type": "pie",
        "data": {
        "datasets": [{
        "data": [ "<?php echo $num1->num_rows() / $star * 100 ?>", "<?php echo $num2->num_rows() / $star * 100 ?>", "<?php echo $num3->num_rows() / $star * 100 ?>", "<?php echo $num4->num_rows() / $star * 100 ?>", "<?php echo $num5->num_rows() / $star * 100 ?>" ],
        "backgroundColor": [
        "rgb(233, 0, 7)",
        "rgb(250, 152, 41)",
        "rgb(254, 200, 40)",
        "rgb(145, 208, 80)",
        "rgb(0, 96, 47)"],
        "label": "Dataset 1"
        }],
        "labels": [
        "Poor (<?php echo $num1->num_rows() ?>)",
        "Fair (<?php echo $num2->num_rows() ?>)",
        "Average (<?php echo $num3->num_rows() ?>)",
        "Good (<?php echo $num4->num_rows() ?>)",
        "Excellent (<?php echo $num5->num_rows() ?>)"
        ]
        },
        "options": {
        "responsive": true
        }
        }
        <?php
    }

}
