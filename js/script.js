var EM = EM || {};
EM.script = (function (j) {
    var _url = j('.page-url').attr('content');
    var _position = j('.page-url').attr('data-type');
    var _title = j('title').html();
    var init = function () {
        togglemenu();
        login();
        logout();
        create_ticket();
        fileattached();
        show_assignee();
        add_assignee();
        message_status();
        update_subject();
        data_select();
        comment_editor();
        pubnub_function();
        show_comments();
        update_description();
        ellipss();
        export_date();

        datepicker();
        year_report();
        month_report();
        type_issue();
        type_status();
        function_select();
        view_report();
        default_datepicker();
        open_profile_pic();
        chat_support_container();
        chat_menu();
        open_chat();
        start_chat();
        autologout();
        chat_search();
        chat_scroll_top();
        table_type_issues();
        cc_button();
        cc_names();

        it_stats();

        change_my_status();

        create_it_list();
        update_transfer();

        update_time_transfer();

        transfer_assignee();
        show_transfer_assign_it();
        delele_transfer_assignee();

        add_pc_name();
        pc_names();

        add_pc_username();

        show_pc_list();

        delete_added_pc();

        radio_rating();

        rating_pie();
        dash_month();
        update_sub_category();
        update_title();
        update_purpose();
        update_link();
        update_date_required();
        update_content();
        update_requestby();
        update_target();
        update_requestor();
        update_approvedby();
    };
    var pubnub = new PubNub({publishKey: 'pub-c-fe3a0094-1e25-4984-98dc-70ad26e14c8e', subscribeKey: 'sub-c-bc434462-8b72-11e6-bb6b-0619f8945a4f'});
    var pubnub_notify = function (data, url) {
        var _position = j('.page-url').attr('data-type');
        if (parseInt(_position) == 0 ) {
            PNotify.desktop.permission();
            (new PNotify({
                title: 'The Outsourced Accountant',
                text: (j(data).text()).substr(0, 100) + '....',
                desktop: {
                    desktop: true,
                    icon: _url + 'images/toa-icon.png'
                }
            })).get().click(function (e) {
                window.location = _url + 'issues_page/issue_id/' + url;
            });
        }
        
    };
    var user_status = function (status) {
        if (status === '1') {
            return "I'm on my break";
        } else if (status === '2') {
            return "I'm in a meeting";
        } else if (status === '3') {
            return "I'm out of the office";
        } else if (status === '4') {
            return "I'm on leave";
        }
    };
    var escapeHtml = function (text) {
        return text
                .replace(/&/g, "&amp;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#039;");
    };
    var title_notify = function () {
        var new_title = j('title');
        var num = 0;
        window.title = setInterval(function () {
            if (num === 0) {
                num = 1;
                new_title.html('New message ' + _title);
            } else {
                num = 0;
                new_title.html(_title);
            }
        }, 500);
    };
    var pubnub_function = function () {
        var chat_support_header = j('.chat-support-header');
        var chat_message = j('.chat-message');
        var chat_online = j('.chat-online');
        var _ctext = '';
        window.window_focus = true;
        j(window).focus(function () {
            var _ttl = j('title');
            window.window_focus = true;
            clearInterval(window.title);
            _ttl.html(_title);
        }).blur(function () {
            window.window_focus = false;
        });

        pubnub.addListener({
            message: function (obj) {
                
                if (obj.message === 'assignee') {
                    show_assignee();
                    it_stats();
                } else if (obj.message === 'comment') {
                    show_comments();
                } else if (obj.message === 'chat_menu') {
                    chat_menu();
                } else if (obj.message === 'new_cc') {
                    cc_names();
                    it_stats();
                } else if (obj.message.text === 'ticket') {
                    pubnub_notify(obj.message.msg, obj.message.url);
                    year_report();
                    month_report();
                    type_issue();
                    type_status();
                    status_update();
                    widget_number();
                    PlaySound(_url + 'images/sms.mp3');
                    it_stats();
                } else if (obj.message.text === 'update_status' && obj.message.status > 1) {
                    widget_number();
                    j('.remove_' + obj.message.class).remove();
                    it_stats();
                } else if (obj.message.text === 'chat') {
                    if (obj.message.data_message === chat_message.attr('data-message') && obj.message.data_conversation === chat_message.attr('data-conversation')) {
                        PlaySound(_url + 'images/job-done.ogg');
                        _ctext = '<div class="chat-text-message chat-you"><div class="chat-image"><img src="' + _url + 'request/image_id/' + chat_message.attr('data-conversation') + '.jpg"></div>' + escapeHtml(obj.message.chat) + '</div>';
                        chat_message.append(_ctext);
                        chat_message.stop().animate({scrollTop: chat_message.prop('scrollHeight')}, '500');
                        if (j('.chat-close').length) {
                            chat_support_header.find('.alert-icon-message').removeClass('hide');
                        }
                        if (!window.window_focus) {
                            title_notify();
                            PNotify.desktop.permission();
                            (new PNotify({
                                title: 'CHAT - he Outsourced Accountant',
                                text: obj.message.chat,
                                desktop: {
                                    desktop: true,
                                    icon: _url + 'images/toa-icon.png'
                                }
                            })).get().click(function (e) {
                                j(window).focus();
                            });
                        } else {
                            clearInterval(window.title);
                        }
                    }  else {
                        if (chat_message.attr('data-message') === obj.message.data_message) {
                            chat_online.find('.chat-online-list').each(function () {
                                var _this = j(this);
                                if (_this.attr('data-bind') === obj.message.data_conversation) {
                                    PlaySound(_url + 'images/what-friends-are-for.ogg');
                                    _this.addClass('chat-new-message');
                                    var _clone = (_this.clone());
                                    _this.remove();
                                    chat_online.prepend(_clone);
                                    chat_support_header.find('.alert-icon-message').removeClass('hide');
                                    active_button();
                                    if (j('.chat-close').length) {
                                        chat_support_header.find('.alert-icon-message').removeClass('hide');
                                    }
                                    if (!window.window_focus) {
                                        PNotify.desktop.permission();
                                        (new PNotify({
                                            title: 'The Outsourced Accountant',
                                            text: obj.message.chat,
                                            desktop: {
                                                desktop: true,
                                                icon: _url + 'images/toa-icon.png'
                                            }
                                        })).get().click(function (e) {
                                            j(window).focus();
                                        });
                                    }
                                }
                            });
                        }
                    }
                } else if (obj.message.msg === 'rating') {
                    rating_pie();
                    PNotify.desktop.permission();
                    (new PNotify({
                        title: 'Rate Us',
                        text: obj.message.text,
                        desktop: {
                            desktop: true,
                            icon: _url + 'images/toa-icon.png'
                        }
                    })).get().click(function (e) {
                        j(window).focus();
                    });
                }
            }
        });
        pubnub.subscribe({channels: ['78954512156412']});
    };
    var PlaySound = function (path) {
        var audioElement = document.createElement('audio');
        audioElement.setAttribute('src', path);
        audioElement.play();
    };
    var widget_number = function () {
        var _body = j('.main-widget');
        _body.find('.count').each(function (e) {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/widget_number',
                method: 'post',
                data: {widget_num: (e + 1)},
                success: function (e) {
                    _this.html(e.number);
                }
            });
        });
    };
    var datepicker = function () {
        j('.resolved-date').each(function () {
            var _this = j(this);
            _this.datetimepicker({
                format: 'm/d/Y h:i:s A',
                formatDate: 'm/d/Y h:i:s A',
                step: 5,
                onChangeDateTime: function (e) {
                    j.ajax({
                        url: _url + 'request/update_rdate',
                        dataType: 'json',
                        method: 'post',
                        data: {
                            _date_id: _this.attr('data-bind'),
                            _submited_date: _this.val()
                        },
                        success: function (e) {
                            _this.animate({'backgroundColor': '#337ab7'}, 500, function () {
                                _this.animate({'backgroundColor': '#FFF'}, 500, function () {
                                    _this.removeAttr('style');
                                });
                            });
                        }
                    });
                }
            });
        });

        j('.update-issue-date').each(function () {
            var _this = j(this);
            _this.datetimepicker({
                format: 'm/d/Y h:i:s A',
                formatDate: 'm/d/Y h:i:s A',
                step: 5,
                onChangeDateTime: function (e) {
                    j.ajax({
                        url: _url + 'request/update_date',
                        dataType: 'json',
                        method: 'post',
                        data: {
                            _date_id: _this.attr('data-bind'),
                            _submited_date: _this.val()
                        },
                        success: function (e) {
                            new PNotify({
                                title: 'Update Status',
                                text: e.text,
                                type: 'info',
                                styling: 'bootstrap3',
                                animate: {
                                    animate: true,
                                    in_class: 'bounceInUp',
                                    out_class: 'bounceOut'
                                },
                                before_open: function (PNotify) {

                                }
                            });
                        }
                    });
                }
            });
        });
    };
    var issue_type_script = function ($status) {
        if ($status == 1) {
            return 'GRAPHICS';
        } else if ($status == 2) {
            return 'VIDEO';
        } else if ($status == 3) {
            return 'NETWORK';
        } else if ($status == 4) {
            return 'PHOTO';
        } else if ($status == 5) {
            return 'CONTENT';
        } else if ($status == 6) {
            return 'OTHERS';
        } 
    };
    var issue_status = function ($status) {
        if ($status == 1) {
            return '<strong class="label label-danger">OPEN</strong>';
        } else if ($status == 2) {
            return '<strong class="label label-warning">RE OPEN</strong>';
        } else if ($status == 3) {
            return '<strong class="label label-info">ONGOING</strong>';
        } else if ($status == 4) {
            return '<strong class="label label-primary">RESOLVED</strong>';
        }
    };
    var priority = function ($priority) {
        if ($priority == 1) {
            return '<strong class="label label-danger">CRITICAL</strong>';
        } else if ($priority == 2) {
            return '<strong class="label label-warning">MAJOR</strong>';
        } else if ($priority == 3) {
            return '<strong class="label label-info">MINOR</strong>';
        } else if ($priority == 4) {
            return '<strong class="label label-primary">TRIVAL</strong>';
        }
    };
    var status_update = function () {
        var status_open = j('.status-open');
        var html = '';
        j.ajax({
            url: _url + 'request/ajax_issue',
            method: 'post',
            success: function (e) {
                html += '<tr class="remove_' + e[0].ID + '">';
                html += '<td class="text-center">' + issue_status(e[0].status) + '</td>';
                html += '<td>';
                html += '<a class="ellipsis"  href="' + _url + 'issues_page/issue_id/' + e[0].ID + '" style="word-wrap: break-word;" title="" data-content="' + j(e[0].message).text() + '">' + j(e[0].message).text() + '</a>';
                html += '</td>';
                html += '<td class="text-center">';
                html += '<a href="http://localhost/ticketing_v3/issues_page/issue_id/5232">' + e[0].key_id + '</a>';
                html += '</td>';
                html += '<td>';
                html += '<a href="http://localhost/ticketing_v3/issues_page/issue_id/5232">' + issue_type_script(e[0].subject) + '</a>';
                html += '</td>';
                html += '<td class="text-center">' + priority(e[0].priority) + '</td>';
                html += '<td class="text-center">' + e[0].fname + ' ' + e[0].lastname + '</td>';
                html += '<td class="text-center">' + e[0].location + '</td>';
                html += '<td class="text-center">' + dateToString(e[0].date) + '</td>';
                html += '<td class="text-center">&nbsp;</td>';
                html += '<td class="text-center"><span data-content="" data-toggle="popover">0</span></td>';
                html += '</tr>';
                status_open.prepend(html);
            }
        });
    };
    var ellipss = function () {
        j(".ellipsis").ellipsis({
            watch: 'window'
        }).popover({
            trigger: 'hover',
            placement: 'top'
        });
    };
    var dash_month = function () {
        var select_year_report = j('.select-year-report');
        var select_month_report = j('.select-month-report');
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        j.ajax({
            url: _url + 'request/dashboard_month',
            method: 'post',
            data: {
                select_year: select_year_report.val(),
                select_month: select_month_report.val()
            },
            success: function(output){

                j('.dash-month').html(output);
                table_type_issues();
                console.log(monthNames[parseInt(select_month_report.val())-1]);
            }
        });
    };
    
    
    var month_report = function () {
        var select_year_report = j('.select-year-report');
        var select_month_report = j('.select-month-report');
        var json_report = function (output) {

            var data = {
                labels: output.label,
                datasets: [
                    {
                        label: output.daily,
                        borderJoinStyle: 'miter',
                        lineTension: 0.1,
                        data: output.data,
                        backgroundColor: 'rgba(11, 140, 221, 0.50)',
                        borderColor: '#0b8cdd',
                        borderWidth: 1
                    }
                ]
            };
            if (typeof window.montlyreport !== 'undefined') {
                window.montlyreport.destroy();
            }
            if (j("#montly-report").length) {
                window.montlyreport = new Chart(j("#montly-report"), {
                    type: 'line',
                    data: data,
                    options: {
                        animation: false,
                        responsive: true,
                        backgroundColor: "#FFFFFF"
                    }
                });
            }
        };
        if (j("#montly-report").length) {
            j.ajax({
                url: _url + 'request/month_report',
                method: 'post',
                data: {
                    select_year: select_year_report.val(),
                    select_month: select_month_report.val()
                },
                success: json_report
            });
        }
    };

    var year_report = function () {
        var select_year_report = j('.select-year-report');
        var select_month_report = j('.select-month-report');
        var table_month_dash = j('.table-month-dash');
        var json_report = function (output) {
            var _m = '';
            var _t = 0;
            for (var m = 0; m < output.label.length; m++) {
                var put = (output.label[m]);
                var num = (output.label[m]);
                put = put.split("(")[0];
                num = num.split("(")[1];
                _m += ('<tr><td>' + put + '</td><td>' + parseInt(num) + '</td></tr>');
                _t += parseInt(num);
            }
           _m += ('<tr><td>Total</td><td>' + _t + '</td></tr>');
           table_month_dash.html(_m);
            var data = {
                labels: output.label,
                datasets: [
                    {
                        label: output.monthly,
                        borderJoinStyle: 'miter',
                        lineTension: 0.1,
                        data: output.data,
                        backgroundColor: 'rgba(7, 48, 76, 0.50)',
                        borderColor: '#07304c',
                        borderWidth: 1
                    }
                ]
            };
            if (typeof window.yearreport !== 'undefined') {
                window.yearreport.destroy();
            }
            if (j("#year-report").length) {
                window.yearreport = new Chart(j("#year-report"), {
                    type: 'line',
                    data: data,
                    options: {
                        animation: false,
                        responsive: true,
                        backgroundColor: "#FFFFFF"
                    }
                });
            }
        };
        j.ajax({
            url: _url + 'request/year_report',
            method: 'post',
            data: {select_year: select_year_report.val()},
            success: json_report
        });

    };
    var function_select = function () {
        j('.select-year-report').change(function () {
            year_report();
            month_report();
            dash_month();
        });
        j('.select-month-report').change(function () {
            year_report();
            month_report();
             dash_month();
        });
    };

    var type_issue = function () {
        var json_reports = function (output) {
            
            var data = {
                datasets: output.data
            };
            if (j("#issue-report").length) {
                new Chart(j("#issue-report"), {
                    type: 'horizontalBar',
                    data: data,
                    options: {
                        animation: false,
                        responsive: true,
                        backgroundColor: "#FFFFFF"
                    }
                });
            }
        };

        j.ajax({
            url: _url + 'request/issue_report',
            method: 'post',
            success: json_reports
        });
    };

    var type_status = function () {
        var json_reports = function (output) {
            var data = {
                datasets: output.data
            };
            if (j("#status-report").length) {
                new Chart(j("#status-report"), {
                    type: 'bar',
                    data: data,
                    options: {
                        animation: false,
                        responsive: true,
                        backgroundColor: "#FFFFFF"
                    }
                });
            }
        };
        j.ajax({
            url: _url + 'request/status_report',
            method: 'post',
            success: json_reports
        });
    };


    var fileattached = function () {
        j('.fileupload').change(function () {
            var _val = '';
            var _this = j(this);
            var files = _this.prop("files");
            for (var i = 0; i < files.length; i++) {
                _val += (files[i].name) + ',';
            }
            _val = _val.slice(0, -1);
            j('.fake-path').val(_val);
        });
    };
    var update_description = function () {
        var edit_descptn = j('.edit-textarea');
        var container = j('.description-container');
        var edit_textarea = j('.show-edit-textarea');
        if (edit_textarea.length) {
            var description = CKEDITOR.replace('edit-descptn', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']}
                ]
            });
            edit_descptn.submit(function (e) {
                e.preventDefault();
                var _this = j(this);
                _this.find('.edit-descptn').val(description.getData());
                j.ajax({
                    url: _url + 'request/edit_description',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            container.html(description.getData());
                            edit_descptn.addClass('hide');
                            container.removeClass('hide');
                        }
                    }
                });
            });
            edit_textarea.find('.btn').click(function () {
                edit_textarea.addClass('hide');
                container.addClass('hide');
                edit_descptn.removeClass('hide');
            });
        }
    };
    var update_subject = function () {
        var _subject = j('.update-subject');
        _subject.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_subject',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_sub_category = function () {
        var _subject = j('.update-sub_category');
        var _subject_id = j('.update-subject');
        _subject.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_sub_category',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject_id.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_requestby = function () {
        var _subject = j('.update-requestby');
        var _subject_id = j('.update-subject');
        _subject.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_requestby',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject_id.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
     var update_target = function () {
        var _subject = j('.update-target');
        var _subject_id = j('.update-subject');
        _subject.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_target',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject_id.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_title = function () {
        var _subject = j('.update-subject');
        var _subject_id = j('.update-title');
        _subject_id.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_title',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_purpose = function () {
        var _subject = j('.update-subject');
        var _subject_id = j('.update-purpose');
        _subject_id.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_purpose',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_requestor = function () {
        var _subject = j('.update-subject');
        var _subject_id = j('.update-requestor');
        _subject_id.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_requestor',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: j('.requestor').val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
     var update_approvedby = function () {
        var _subject = j('.update-subject');
        var _subject_id = j('.approvedby');
        _subject_id.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_approvedby',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: j('.approvedby').val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_link = function () {
        var _subject = j('.update-subject');
        var _subject_id = j('.update-link');
        _subject_id.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_link',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_date_required = function () {
        var _subject = j('.update-subject');
        var _subject_id = j('.update_date_required');
        _subject_id.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_date_required',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var update_content = function () {
        var _subject = j('.update-subject');
        var _subject_id = j('.update_content');
        _subject_id.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_content',
                dataType: 'json',
                method: 'post',
                data: {issue_id: _subject.attr('data-bind'), value: _this.val()},
                success: function (e) {
                    new PNotify({
                        title: 'Update Status',
                        text: e.text,
                        type: 'info',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };



    var message_status = function () {
        var message_stat = j('.message-status');
        message_stat.validate({
            rules: {
                status: {required: true}
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});

                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.parent().popover('destroy');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                _this.find('.btn-primary').attr('disabled', 'disabled');
                j.ajax({
                    url: _url + 'request/update_status',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        _this.find('.btn-primary').removeAttr('disabled');
                        new PNotify({
                            title: 'Update Status',
                            text: e.text,
                            type: 'info',
                            styling: 'bootstrap3',
                            animate: {
                                animate: true,
                                in_class: 'bounceInUp',
                                out_class: 'bounceOut'
                            },
                            before_open: function (PNotify) {

                            }
                        });
                        pubnub.publish({channel: '78954512156412', message: {
                                text: 'update_status',
                                status: e.status,
                                class: e.mid
                            }
                        });
                    }
                });
                return false;
            }
        });
    };
    var add_assignee = function () {
        j.ajax({
            url: _url + 'request/add_assignee',
            method: 'post',
            success: function (partTags) {
                j(".find-assignee").autocomplete({
                    delay: 45,
                    source: partTags,
                    select: function (event, ui) {
                        var _this = j(this);
                        j.ajax({
                            url: _url + 'request/new_assignee',
                            method: 'post',
                            data: {
                                'issueid': _this.attr('data-bind'),
                                'assignee': ui.item.value
                            },
                            success: function (e) {
                                pubnub.publish({channel: '78954512156412', message: 'assignee'});
                                show_assignee();
                                _this.val('');
                            }
                        });
                        return false;
                    }, focus: function (event, ui) {
                        var _this = j(this);
                        event.preventDefault();
                        _this.val(ui.item.label);
                    }
                });
            }
        });
    };
    var show_assignee = function () {
        var _assignee_form = j('.assignee-form');
        j.ajax({
            url: _url + 'request/show_assignee',
            data: {assignee: _assignee_form.attr('data-bind')},
            method: 'post',
            success: function (e) {
                var _html = '';
                for (var i = 0; i < e.length; i++) {
                    _html += '<strong class="label label-info">' + e[i].fname + ' ' + e[i].lastname + ' <a href="' + e[i].GETID + '" class="fa fa-times text-danger remove-assignee' + (_position === '' ? ' hide' : '') + '"></a></strong> ';
                }
                _assignee_form.find('.form-control').html(_html);
                remove_assignee();
            }
        });
    };
    var remove_assignee = function () {
        var _assignee_form = j('.assignee-form');
        _assignee_form.find('.remove-assignee').unbind().bind('click', function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/remove_assignee',
                method: 'post',
                data: {remove_assignee: _this.attr('href')},
                success: function () {
                    _this.parent().remove();
                    pubnub.publish({channel: '78954512156412', message: 'assignee'});
                }
            });
            return false;
        });
    };
    var comment_editor = function () {
        var comment_edtr = j('.comment-editor');
        var _comment = j('.comment-form');
        if (comment_edtr.length) {
            var comments = CKEDITOR.replace('comment_editor', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']}
                ]
            });
            _comment.submit(function (e) {
                e.preventDefault();
                var _this = j(this);
                _this.find('.comment-editor').val(comments.getData());
                j.ajax({
                    url: _url + 'request/post_comments',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            pubnub.publish({channel: '78954512156412', message: 'comment'});
                            comments.setData('');
                            show_comments();
                        }
                    }
                });
            });
        }
    };
    var show_comments = function () {
        var _output = j('.comment-output');
        j.ajax({
            url: _url + 'request/show_comments',
            method: 'post',
            data: {comment_id: _output.attr('data-bind')},
            success: function (e) {
                var _html = '<div class="list-group list-group-profile">';
                for (var i = 0; i < e.length; i++) {
                    _html += '<div class="list-group-item"> ';
                    _html += '<div class="comment-item-list"> ';
                    _html += '<img class="add-profile" src="' + _url + 'request/image_id/' + e[i].UID + '.jpg">';
                    _html += '</div> ';
                    _html += '<div class="comment-item-pro"> ';
                    if (e[i].UID === _output.attr('data-id')) {
                        _html += '<a class="close text-danger" href="' + _url + '"><i class="fa fa-times"></i></a> ';
                    }
                    _html += '<p><strong' + (e[i].UID === _output.attr('data-id') ? ' class="text-info"' : '') + '>' + e[i].fname + ' ' + e[i].lastname + '</strong></p>';
                    _html += '<div>' + e[i].comments + '</div>';

                    _html += '</div> ';

                    _html += '</div> ';
                }
                _html += '</div> ';
                _output.html(_html);
                delete_comment();
            }
        });
    };
    var delete_comment = function () {
        var _output = j('.comment-output');
        _output.find('.close').unbind().bind('click', function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/delete_comments',
                method: 'post',
                data: {comment_id: _this.attr('href')},
                success: function (e) {
                    show_comments();
                    pubnub.publish({channel: '78954512156412', message: 'comment'});
                }
            });
            return false;
        });
    };
    var create_ticket = function () {
        var _create = j('.create-ticket-form');
        var _htmled = j('.create-ticket');
        var _content = j('.content-ticket');

        $('.date-content').datepicker({
            dateFormat: 'dd-mm-yy',
            altField: '.date-content',
            altFormat: 'yy-mm-dd'
        });


        if (_htmled.length) {
            var editor = CKEDITOR.replace('create-ticket', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']}
                ]
            });





            _create.validate({
                rules: {
                    priority: 'required',
                    subject: 'required'
                    
                }, showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.parent().addClass('has-error');
                        _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.parent().removeClass('has-error');
                        _elm.parent().popover('destroy');

                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    _this.find('.create-ticket').val(editor.getData());
                    _this.ajaxSubmit({
                        url: _url + 'request/create_ticket',
                        beforeSend: function () {
                            _this.find('.progress').removeClass('hide');
                            _this.find('.btn-send').attr('disabled', 'disabled');
                            _this.find('.btn-send span').html('SENDING....');
                        },
                        uploadProgress: function (event, position, total, percentComplete) {
                            _this.find('.progress').removeClass('hide');
                            _this.find('.progress-bar').css({width: percentComplete + '%'}).html(percentComplete + '%');
                        },
                        complete: function (e) {
                            var _e = (j.parseJSON(e.responseText));
                            if (_e.message === 'success') {
                                j.message({text: _e.text}, function () {
                                    window.location = _url + 'issues_page/issue_id/' + _e.message_id;
                                });
                                pubnub.publish({channel: '78954512156412', message: {text: 'ticket', msg: editor.getData(), url: _e.message_id}});
                                editor.setData('');
                                _this[0].reset();
                            } else {

                            }
                            _this.find('.progress').addClass('hide');
                            _this.find('.btn-send').removeAttr('disabled');
                            _this.find('.btn-send span').html('SEND');
                        }
                    });
                    return false;
                }
            });
        }
    };
    var autologout = function () {
        var count = 1;
        j.ajax({
            url: _url + 'request/update_online_support',
            method: 'post',
            success: function (e) {
                pubnub.publish({channel: '78954512156412', message: 'chat_menu'});
            }
        });
        setInterval(function () {
            count = (count + 1);
            if (count > 1800) {
                logout();
            }
            if (count === 900) {
                pubnub.publish({channel: '78954512156412', message: 'chat_menu'});
            }
        }, 1000);
        j(window).mousemove(function () {
            count = 0;
        });
    };
    var logout = function () {
        var _logout = j('.logout-btn');
        _logout.click(function (e) {
            e.preventDefault();
            j.ajax({
                url: _url + 'request/logout',
                method: 'post',
                dataType: 'json',
                success: function (e) {
                    if (e.message === 'success') {
                        pubnub.publish({channel: '78954512156412', message: 'chat_menu'});
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                }
            });
        });
    };
    var login = function () {
        j('.login').validate({
            rules: {
                email_address: {
                    required: true,
                    email: true
                },
                user_password: 'required'
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('error-input');
                    _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                    var len = _elm.parent().find('.form-control-feedback').length;
                    if (len === 0) {
                        _elm.parent().append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
                    }
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('error-input');
                    _elm.parent().popover('destroy');
                    var len = _elm.parent().find('.form-control-feedback').length;
                    if (len > 0) {
                        _elm.parent().find('.form-control-feedback').remove();
                    }
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                _this.find('.login-btn').addClass('btn-loading');
                j.ajax({
                    url: _url + 'request/login',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            _this.find('.output').html('<div class="alert alert-success text-center">' + e.text + '</div>');
                            pubnub.publish({channel: '78954512156412', message: 'chat_menu'});
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        } else if (e.message === 'error') {
                            _this.find('.output').html('<div class="alert alert-warning text-center">' + e.text + '</div>');
                            _this.find('.login-btn').removeClass('btn-loading');
                        }
                    }
                });
                return false;
            }
        });
    };

    var export_date = function () {
        var date_from = j(".date-from");
        var date_to = j(".date-to");
        var ex_btn = j(".export-button");
        var sh_btn = j(".show-button");


        var ex_all_btn = j(".export-all-button");
        var sh_all_btn = j(".show-all-button");


        var ebutton_transfer = j(".export-button-transfer");
        var sbutton_transfer = j(".show-button-transfer");


        j.ajax({
            url: _url + 'request/time',
            method: 'post',
            success: function (e) {
                date_from.datepicker({
                    dateFormat: 'mm-dd-yy',
                    defaultDate: e.month + '-' + e.day + '-' + e.year,
                    onSelect: function (date) {
                        console.log(date);
                        var _this = j(this);
                        _this.parent().parent().parent().find(".date-to").val('');
                        _this.parent().parent().parent().find(".date-to").removeAttr('disabled');
                        _this.parent().parent().parent().find(".date-to").datepicker({
                          
                            dateFormat: 'mm-dd-yy',
                            defaultDate: e.month + '-' + e.day + '-' + e.year,
                            onSelect: function (date) {
                                var _this = j(this);

                                _this.parent().parent().parent().find(".export-button-transfer").removeAttr('disabled');
                                _this.parent().parent().parent().find(".show-button-transfer").removeAttr('disabled');

                                _this.parent().parent().parent().find(".export-button").removeAttr('disabled');
                                _this.parent().parent().parent().find(".show-button").removeAttr('disabled');

                                _this.parent().parent().parent().find(".export-all-button").removeAttr('disabled');
                                _this.parent().parent().parent().find(".show-all-button").removeAttr('disabled');
                            }
                        });
                    }
                });
            }
        });
        sh_btn.click(function () {
            window.open(_url + 'show_reports/date/' + date_from.val() + '/' + date_to.val(), '_blank');
        });
        ex_btn.click(function () {
            window.open(_url + 'request/itkpi/' + date_from.val() + '/' + date_to.val(), '_blank');
        });

        sh_all_btn.click(function () {
            var _this = j(this);
            window.open(_url + 'show_all_reports/date/' + _this.parent().parent().find('.date-from').val() + '/' + _this.parent().parent().find('.date-to').val(), '_blank');
        });
        ex_all_btn.click(function () {
            var _this = j(this);
            window.open(_url + 'request/allitkpi/' + _this.parent().parent().find('.date-from').val() + '/' + _this.parent().parent().find('.date-to').val(), '_blank');
        });

        ebutton_transfer.click(function () {
            var _this = j(this);
            window.open(_url + 'request/export_transfer/' + _this.parent().parent().find('.date-from').val() + '/' + _this.parent().parent().find('.date-to').val(), '_blank');
        });
        sbutton_transfer.click(function () {
            var _this = j(this);
            window.open(_url + 'view_transfer_report/date/' + _this.parent().parent().find('.date-from').val() + '/' + _this.parent().parent().find('.date-to').val(), '_blank');
        });

    };
    var view_report = function () {
        var date_from = j(".date-from-report");
        var date_to = j(".date-to-report");
        var support_view_report = j('.support-view-report');
        support_view_report.find('.view-report-btn').click(function () {
            if ((date_from.val() !== '') && (date_to.val() !== '')) {
                var _this = j(this);
                var _id = _this.attr('href');
                window.open(_url + 'show_reports/date/' + date_from.val() + '/' + date_to.val() + '/' + _id, '_blank');
            } else {
                new PNotify({
                    title: 'View report',
                    text: 'Select date',
                    type: 'warning',
                    styling: 'bootstrap3',
                    animate: {
                        animate: true,
                        in_class: 'bounceInUp',
                        out_class: 'bounceOut'
                    },
                    before_open: function (PNotify) {

                    }
                });
            }
            return false;
        });

        support_view_report.find('.export-report-btn').click(function () {
            if ((date_from.val() !== '') && (date_to.val() !== '')) {
                var _this = j(this);
                var _id = _this.attr('href');
                window.open(_url + 'request/itkpi/' + date_from.val() + '/' + date_to.val() + '/' + _id, '_blank');
            } else {
                new PNotify({
                    title: 'View report',
                    text: 'Select date',
                    type: 'warning',
                    styling: 'bootstrap3',
                    animate: {
                        animate: true,
                        in_class: 'bounceInUp',
                        out_class: 'bounceOut'
                    },
                    before_open: function (PNotify) {

                    }
                });
            }
            return false;
        });

    };
    var togglemenu = function () {
        var _main = j('.main');
        _main.find('.side-panel-button').click(function () {
            if (_main.hasClass('close-sidemenu') === false) {
                _main.addClass('hide-span');
                _main.find('.content').animate({'margin-left': '60px'}, 200, function () {
                    _main.find('.content').removeAttr('style');
                });
                _main.find('.side-menu').animate({width: '60px'}, 200, function () {
                    _main.addClass('close-sidemenu');
                    j.cookie("sidemenu", 'sidemenu');
                    _main.find('.side-menu').removeAttr('style');
                    _main.find('.side-menu').find('span').each(function () {
                        var _this = j(this);
                        var _badge = _this.parent().find('.badge');
                        var _name = _this.html();
                        if (_badge.length > 0) {
                            _name += (" <i class='badge'>" + _badge.html() + "</i>");
                        }
                        _this.parent().popover({content: _name, container: 'body', placement: 'right', trigger: 'hover', html: true});
                    });
                });
            } else {
                _main.find('.content').animate({'margin-left': '300px'}, 200, function () {
                    _main.find('.content').removeAttr('style');
                });
                _main.find('.side-menu').animate({width: '300px'}, 200, function () {
                    _main.removeClass('close-sidemenu');
                    _main.find('.side-menu').removeAttr('style');
                    _main.removeClass('hide-span');
                    _main.find('.side-menu').find('span').each(function () {
                        var _this = j(this);
                        _this.parent().remove('data-content').popover('destroy');
                    });
                    j.removeCookie("sidemenu");
                    j.cookie("sidemenu", '', {expires: -1, path: '/'});
                });
            }
        });
        j(window).on('resize load', function () {
            var _this = j(this);
            if (_this.width() < 768) {
                _main.addClass('close-sidemenu');
                _main.find('.side-menu').find('span').each(function () {
                    var _this = j(this);
                    var _badge = _this.parent().find('.badge');
                    var _name = _this.html();
                    if (_badge.length > 0) {
                        _name += (" <i class='badge'>" + _badge.html() + "</i>");
                    }
                    _this.parent().popover({content: _name, container: 'body', placement: 'right', trigger: 'hover', html: true});
                });
            }
        });
    };
    var data_select = function () {
        j('select').each(function () {
            var _this = j(this);
            if (_this.length) {
                _this.find('[value="' + _this.attr('data-select') + '"]').prop('selected', true);
            }
        });
    };
    var dateToString = function (FromString) {
        var now = new Date(FromString * 1000);
        var dd = now.getDate();
        var mm = now.getMonth() + 1;
        var yyyy = now.getFullYear();
        var hh = now.getHours();
        var ii = now.getMinutes();
        var ss = now.getSeconds();
        var aa = 'AM';
        if (hh > 12) {
            aa = 'PM';
            hh = 12;
        }
        if (dd < 10) {
            dd = '0' + parseInt(dd);
        }
        if (mm < 10) {
            mm = '0' + parseInt(mm);
        }
        if (hh < 10) {
            dd = '0' + parseInt(dd);
        }
        if (ii < 10) {
            ii = '0' + parseInt(ii);
        }
        if (ss < 10) {
            ss = '0' + parseInt(ss);
        }
        var result = (mm + "/" + dd + "/" + yyyy + ' ' + hh + ':' + ii + ':' + ss + ' ' + aa);
        return result;
    };

    var default_datepicker = function () {

        j.ajax({
            url: _url + 'request/time',
            method: 'post',
            success: function (output) {
                var _yer = output.year;
                var _mon = output.month;
                var _day = output.day;
                var _sec = output.seconds;
                var _min = output.minutes;
                var _hur = output.hours;
                var _ham = output.hour;
                var _amp = output.amp;
                var _cal = j('#calendar');
                if (_cal.attr('data-bind')) {
                    _cal.fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listWeek'
                        },
                        defaultDate: _yer + '-' + _mon + '-' + _day,
                        eventColor: '#07304c',
                        displayEventTime: false,
                        navLinks: true,
                        eventLimit: true,
                        events: {
                            url: _url + 'request/calendar/' + _cal.attr('data-bind'),
                            error: function () {

                            }
                        }, dayRender: function (date, cell, text) {

                        }, eventDrop: function (event, delta, revertFunc) {

                        },
                        eventRender: function (event, element) {
                            var _con = '';
                            _con += '<div style="font-size:12px;line-height:1">';
                            _con += '<div style="margin-bottom:5px;border-bottom:1px solid #999;padding-bottom:5px;">';
                            _con += '<div style="margin-bottom:5px;"><strong>Status:</strong> ' + issue_status(event.status) + '</div>';
                            _con += '<div style="margin-bottom:5px;"><strong>Date:</strong> ' + event.start._i + '</div>';
                            if (event.end !== null) {
                                _con += '<div><strong>Resolved:</strong> ' + event.end._i + '</div>';
                            }
                            _con += '</div>';
                            _con += '<p><strong style="margin-bottom:5px;display:block;">Description</strong>' + event.description + '</p>';
                            _con += '</div>';
                            element.qtip({
                                position: {
                                    my: 'bottom center',
                                    at: 'bottom center',
                                    target: 'mouse'
                                },
                                content: _con,
                                style: {
                                    classes: 'qtip-dark qtip-rounded qtip-shadow'
                                }
                            });
                        },
                        loading: function (bool) {
                            j('#loading').toggle(bool);
                        }, dayClick: function (date, jsEvent, view) {
                            var d = new Date(date._d);
                            var dmonth = d.getMonth() + 1;
                            var _month = dmonth < 10 ? '0' + dmonth : dmonth;
                            var _day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
                        }, eventClick: function (calEvent, jsEvent, view) {
                            if (calEvent.url) {
                                window.open(calEvent.url);
                                return false;
                            }
                        }
                    });
                }
            }
        });

    };
    var success = function () {
        var profile = j('.add-profile');
        var mod = j('.profile-picture-modal');
        mod.modal('hide');
        mod.find('.video-stage').html('');
        mod.find('.btn-photoshoot').removeAttr('disabled');
        profile.attr('src', profile.attr('src') + '?ver=' + Math.random());
    };
    var open_profile_pic = function () {
        var _profile = j('.add-profile');
        _profile.click(function () {
            j('.profile-picture-modal').modal();
            profile();
        });
        var _upload = j('.upload-photo-pro');
        j('.btn-photoupload').click(function () {
            _upload.find('.upload-pp').click();
        });
        _upload.find('.upload-pp').change(function () {
            _upload.ajaxSubmit({
                url: _url + 'request/upload_photo_pro',
                beforeSend: function () {

                },
                uploadProgress: function (event, position, total, percentComplete) {

                },
                complete: function (e) {
                    var _e = (j.parseJSON(e.responseText));
                    if (_e.uploaded === '1') {
                        _profile.attr('src', _e.url + '?ver=' + Math.random());
                        j('.profile-picture-modal').modal('hide');
                        _upload[0].reset();
                    } else {

                    }
                }
            });
            return false;
        });

    };
    var profile = function () {
        var mod = j('.profile-picture-modal');
        var cam = j('.video-stage');
        var object = '';
        object += '<object type="application/x-shockwave-flash" data="' + _url + 'js/webcam.swf?var=5" width="240" height="240" id="webcam-snapshot" style="float: none; vertical-align:middle">';
        object += '<param name="movie" value="' + _url + 'js/webcam.swf?var=5" />';
        object += '<param name="quality" value="high" />';
        object += '<param name="bgcolor" value="#ffffff" />';
        object += '<param name="play" value="true" />';
        object += '<param name="loop" value="true" />';
        object += '<param name="wmode" value="window" />';
        object += '<param name="scale" value="showall" />';
        object += '<param name="menu" value="true" />';
        object += '<param name="devicefont" value="false" />';
        object += '<param name="salign" value="" />';
        object += '<param name="allowScriptAccess" value="sameDomain" />';
        object += '<a href="http://www.adobe.com/go/getflash">';
        object += '<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />';
        object += '</a>';
        object += '</object>';
        cam.html(object);
        mod.find('.btn-photoshoot').unbind().bind('click', function () {
            mod.find('.btn-photoshoot').attr('disabled', 'disabled');
            j('#webcam-snapshot')[0].snapshot();
        });
    };
    var chat_support_container = function () {
        var toggle = j('.chat-support-container');
        toggle.find('.chat-support-header').click(function () {
            var _this = j(this);
            if (_this.parent().hasClass('chat-close')) {
                _this.parent().removeClass('chat-close');
                j.cookie("chat_toggle", '');
            } else {
                _this.parent().addClass('chat-close');
                j.cookie("chat_toggle", 'chat-close');
            }
            toggle.find('.chat-message').scrollTop(toggle.find('.chat-message').prop('scrollHeight'));
        });
    };
    var chat_scroll_top = function () {
        var chat_message = j('.chat-message');
        var _ctext = '';
        var _num = 1;
        var _msg = j('.chat-message');
        _msg.scroll(function () {
            var _this = j(this);
            if (_this.scrollTop() === 0) {
                chat_message.addClass('loading');
                _num = (_num + 1);
                j.ajax({
                    url: _url + 'request/chat_ajax',
                    method: 'post',
                    data: {
                        conversation: chat_message.attr('data-conversation'),
                        page: _num
                    },
                    success: function (e) {
                        if (e.length) {

                        } else {
                            chat_message.removeClass('loading');
                        }
                        setTimeout(function () {
                            chat_message.removeClass('loading');
                        }, 3000);
                    }
                });
            }
        });
    };
    var start_chat = function () {
        var chat_textarea = j('.chat-textarea');
        var chat_message = j('.chat-message');
        var _ctext = '';
        chat_textarea.on('keypress keydown', function (e) {
            var _code = e.keyCode || e.which;
            var _this = j(this);
            if (_code === 13) {
                if (_this.val().length > 0) {
                    j.ajax({
                        url: _url + 'request/chat_msg',
                        method: 'post',
                        data: {
                            userid: chat_message.attr('data-conversation'),
                            message: _this.val()
                        },
                        success: function (e) {
                            pubnub.publish({channel: '78954512156412', message: {
                                    text: 'chat',
                                    chat: escapeHtml(e.chat_message),
                                    data_conversation: e.from_id,
                                    data_message: e.to_id
                                }
                            });
                            chat_message.find('.sending').removeClass('sending');
                        }
                    });
                    _ctext = '<div class="chat-text-message chat-me sending">' + escapeHtml(_this.val()) + '</div>';
                    chat_message.append(_ctext);
                    chat_message.stop().animate({scrollTop: chat_message.prop('scrollHeight')}, '500');
                    setTimeout(function () {
                        _this.val('');
                    }, 10);
                } else {
                    setTimeout(function () {
                        _this.val('');
                    }, 10);
                }
                return false;
            }

        });
    };

    var open_chat = function () {
        var chat_textarea = j('.chat-textarea');
        var chat_message = j('.chat-message');
        var _ctext = '';
        if (!chat_message.attr('data-conversation')) {
            chat_textarea.addClass('hide');
            check_my_chat();
        } else {
            j.ajax({
                url: _url + 'request/chat_ajax',
                method: 'post',
                data: {conversation: chat_message.attr('data-conversation'), page: '1'},
                success: function (e) {
                    if (e.length) {
                        for (var i = 0; i < e.length; i++) {
                            _ctext += '<div title="' + dateToString(e[i].date) + '" class="chat-text-message chat-' + (e[i].to_id === chat_message.attr('data-conversation') ? 'me' : 'you') + '">' + (e[i].to_id === chat_message.attr('data-conversation') ? '' : '<div class="chat-image"><img src="' + _url + 'request/image_id/' + chat_message.attr('data-conversation') + '.jpg"></div>') + e[i].chat_message + '</div>';
                        }
                        chat_message.html(_ctext);
                        setTimeout(function () {
                            chat_message.scrollTop(chat_message.prop('scrollHeight'));
                        }, 500);
                        check_my_chat();
                    } else {
                        chat_message.html('');
                    }
                    chat_message.removeClass('loading');
                }
            });
            chat_textarea.focus();
        }
    };
    var change_my_status = function () {
        var _select = j('.user-status-noti');
        _select.change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/update_availability',
                method: 'post',
                data: {'status': _this.val()},
                success: function (e) {
                    pubnub.publish({channel: '78954512156412', message: 'chat_menu'});
                    new PNotify({
                        title: 'Status',
                        text: e.text,
                        type: 'warning',
                        styling: 'bootstrap3',
                        animate: {
                            animate: true,
                            in_class: 'bounceInUp',
                            out_class: 'bounceOut'
                        },
                        before_open: function (PNotify) {

                        }
                    });
                }
            });
        });
    };
    var chat_menu = function () {
        var chat_menu = j('.chat-online');
        var chat_message = j('.chat-message');
        var chat_m = '';
        j.ajax({
            url: _url + 'request/ronline',
            method: 'post',
            success: function (e) {
                for (var i = 0; i < e.length; i++) {
                    if (chat_message.attr('data-message') !== e[i].ID) {
                        chat_m += '<div class="chat-online-list ' + e[i].STATUS + '" data-bind="' + e[i].ID + '">';
                        if (e[i].availability > 0) {
                            chat_m += '<div class="chat-user_status">' + user_status(e[i].availability) + '</div>';
                        }
                        chat_m += '<div class="chat-profile-picture"><img src="' + _url + 'request/image_id/' + e[i].ID + '.jpg"></div>';
                        chat_m += '<div class="chat-profile-cont">';
                        chat_m += '<div class="chat-name">' + e[i].lastname + ' ' + e[i].fname + '</div>';
                        chat_m += '<div class="chat-position">' + e[i].job_title + ' (' + e[i].location + ')</div>';
                        chat_m += '</div>';
                        chat_m += '</div>';
                    }
                }
                chat_menu.html(chat_m);
                check_my_chat();
            }
        });
    };
    var check_my_chat = function () {
        var chat_support_toggle = j('.chat-support-toggle');
        if (chat_support_toggle.length) {
            var chat_support_header = j('.chat-support-header');
            var chat_menu = j('.chat-online');
            j.ajax({
                url: _url + 'request/chat_to_me',
                method: 'post',
                success: function (e) {
                    for (var i = 0; i < e.length; i++) {
                        if (e[i].read === '0') {
                            chat_menu.find('[data-bind="' + e[i].from_id + '"]').addClass('chat-new-message');
                            var _clone = (chat_menu.find('[data-bind="' + e[i].from_id + '"]').clone());
                            chat_menu.find('[data-bind="' + e[i].from_id + '"]').remove();
                            chat_menu.prepend(_clone);
                            chat_support_header.find('.alert-icon-message').removeClass('hide');
                        }
                    }
                    active_button();
                    var data = chat_support_toggle.find('.chat-message').attr('data-conversation');
                    if (!data.length) {
                        chat_support_toggle.find('.chat-message').removeClass('loading');
                    }
                }
            });
        }
    };
    var active_button = function () {
        var chat_support_header = j('.chat-support-header');
        var chat_textarea = j('.chat-textarea');
        var chat_menu = j('.chat-online');
        var chat_message = j('.chat-message');
        chat_menu.find('.chat-online-list').unbind().bind('click', function () {
            var _this = j(this);
            _this.removeClass('chat-new-message');
            if (chat_menu.find('.chat-new-message').length === 0) {
                chat_support_header.find('.alert-icon-message').addClass('hide');
            }
            chat_support_header.find('.support-chat-name').text(_this.find('.chat-name').html());
            j.cookie("chat_cookie_id", _this.attr('data-bind'));
            j.cookie("support_chat_name", _this.find('.chat-name').html());
            chat_message.attr('data-conversation', _this.attr('data-bind'));
            chat_textarea.removeClass('hide');
            open_chat();
            return false;
        });
    };

    var search_function = function (keyword) {
        var chat_menu = j('.chat-online');
        var chat_message = j('.chat-message');
        var chat_m = '';
        j.ajax({
            url: _url + 'request/on_search',
            method: 'post',
            data: {'search': keyword},
            success: function (e) {
                for (var i = 0; i < e.length; i++) {
                    if (chat_message.attr('data-message') !== e[i].ID) {
                        chat_m += '<div class="chat-online-list ' + e[i].STATUS + '" data-bind="' + e[i].ID + '">';
                        if (e[i].availability > 0) {
                            chat_m += '<div class="chat-user_status">' + user_status(e[i].availability) + '</div>';
                        }
                        chat_m += '<div class="chat-profile-picture"><img src="' + _url + 'request/image_id/' + e[i].ID + '.jpg"></div>';
                        chat_m += '<div class="chat-profile-cont">';
                        chat_m += '<div class="chat-name">' + e[i].lastname + ' ' + e[i].fname + '</div>';
                        chat_m += '<div class="chat-position">' + e[i].job_title + ' (' + e[i].location + ')</div>';
                        chat_m += '</div>';
                        chat_m += '</div>';
                    }
                }
                chat_menu.html(chat_m);
                check_my_chat();
            }
        });
    };
    var chat_search = function () {
        var search_user = j('.search-user');
        search_user.find('.form-control').on('keyup keydown', function () {
            var _this = j(this);
            if (_this.val().length) {
                search_user.find('.clear-search').removeClass('hide');
                clearTimeout(window.delay);
                window.delay = setTimeout(function () {
                    search_function(_this.val());
                }, 500);
            } else {
                search_user.find('.clear-search').addClass('hide');
                chat_menu();
            }
        });
        search_user.find('.clear-search').click(function () {
            var _this = j(this);
            _this.addClass('hide');
            search_user.find('.form-control').val('');
            chat_menu();
        });
    };
    var table_type_issues = function () {
        var table_type = j('.table-type');

        var total = 0;

        var total1 = 0;
        var total2 = 0;
        var total3 = 0;
        var total4 = 0;
        table_type.find('.total-class').each(function () {
            var _this = j(this);
            total += parseInt(_this.html());
        });
        table_type.find('.total-result').html(total);
        table_type.find('.type-1').each(function () {
            var _this = j(this);
            total1 += parseInt(_this.html());
        });
        table_type.find('.total-1').html(total1);

        table_type.find('.type-2').each(function () {
            var _this = j(this);
            total2 += parseInt(_this.html());
        });
        table_type.find('.total-2').html(total2);

        table_type.find('.type-3').each(function () {
            var _this = j(this);
            total3 += parseInt(_this.html());
        });
        table_type.find('.total-3').html(total3);
        table_type.find('.type-4').each(function () {
            var _this = j(this);
            total4 += parseInt(_this.html());
        });
        table_type.find('.total-4').html(total4);
    };
    var cc_remove = function () {
        var _cc_form = j('.cc-user');
        _cc_form.find('.remove-assignee').unbind().bind('click', function (e) {
            e.preventDefault();
            var _this = j(this);
            j.ajax({
                url: _url + 'request/remove_cc',
                method: 'post',
                data: {remove_cc: _this.attr('href')},
                success: function (e) {
                    _this.parent().remove();
                    pubnub.publish({channel: '78954512156412', message: 'new_cc'});
                }
            });

        });
    };
    var cc_names = function () {
        var _cc_form = j('.cc-user');
        j.ajax({
            url: _url + 'request/show_cc',
            data: {cc_issue_id: _cc_form.attr('data-bind')},
            method: 'post',
            success: function (e) {
                if (e.length > 0) {
                    var _html = '';
                    for (var i = 0; i < e.length; i++) {
                        _html += '<strong class="label label-info">' + e[i].fname + ' ' + e[i].lastname + ' <a href="' + e[i].GETID + '" class="fa fa-times text-danger remove-assignee' + (_position === '' ? ' hide' : '') + '"></a></strong> ';
                    }
                } else {
                    _html = '<i class="fa fa-users"></i>';
                }
                _cc_form.find('.cc-name-label').html(_html);
                cc_remove();
            }
        });
    };
    var cc_button = function () {
        var _cc = j(".cc-input-search");
        _cc.autocomplete({
            delay: 45,
            appendTo: _cc.parent(),
            source: function (request, response) {
                j.ajax({
                    url: _url + 'request/add_cc',
                    method: 'post',
                    data: {name: request.term},
                    success: function (partTags) {
                        var results = j.ui.autocomplete.filter(partTags, request.term);
                        response(results.slice(0, 10));

                    }
                });
            },
            select: function (event, ui) {
                var _this = j(this);
                j.ajax({
                    url: _url + 'request/new_cc',
                    method: 'post',
                    data: {
                        'cc_issue_id': _this.attr('data-bind'),
                        'cc_user_id': ui.item.value
                    },
                    success: function (e) {
                        pubnub.publish({channel: '78954512156412', message: 'new_cc'});
                        cc_names();
                        _this.val('');
                    }
                });
                return false;
            }, focus: function (event, ui) {
                var _this = j(this);
                event.preventDefault();
                _this.val(ui.item.label);
            }
        });
    };
    var it_stats = function () {
        var _it = j('.it_status_menu');
        var _html = '';
        j.ajax({
            url: _url + 'request/check_ticket_status',
            method: 'post',
            success: function (e) {
                if (e.length === 0) {
                    _html += '<li class="list-group-item ">';
                    _html += '<div>No pending ticket...</div>';
                    _html += '</li>';
                } else {
                    for (var i = 0; i < e.length; i++) {
                        _html += '<li class="list-group-item ' + (_it.attr('data-bind') == e[i].sender ? 'list-group-item-info' : '') + '">Ticket ID: ' + e[i].key_id + ' - ' + issue_status(e[i].status);
                        _html += '<div>' + (e[i].assignee) + '</div>';
                        _html += '</li>';
                    }
                }
                _it.html(_html);
            }
        });
    };
    var create_it_list = function () {
        var list = j('.create-table-list');
        list.validate({
            rules: {
                category: {required: true}
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.parent().popover('destroy');

                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'request/create_transfer_list',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            j.message({text: e.text}, function () {
                                window.location = _url + 'transfer/issue_id/' + e.message_id;
                            });
                        }
                    }
                });
                return false;
            }
        });
    };
    var update_time_transfer = function () {
        var _form = j(".update-table-list");
        var _start = _form.find('.transfer-start');
        _start.datetimepicker({
            startDate: _start.val(),
            format: 'm/d/Y H:i:s A',
            formatDate: 'm/d/Y H:i:s A',
            step: 5,
            onChangeDateTime: function (e) {

            }
        });
        j.ajax({
            url: _url + 'request/time',
            method: 'post',
            success: function (e) {
                _form.find('.transfer-end').datetimepicker({
                    startDate: e.month + '/' + e.day + '/' + e.year + ' ' + e.hours + ':' + e.minutes + ':' + e.seconds + ' ' + e.amp,
                    format: 'm/d/Y H:i:s A',
                    formatDate: 'm/d/Y H:i:s A',
                    step: 5,
                    onChangeDateTime: function (e) {

                    }
                });
            }
        });
    };


    var update_transfer = function () {
        var list = j('.update-table-list');
        list.validate({
            rules: {
                category: {required: true},
                transfe_from: {required: true},
                transfer_to: {required: true},
                transfer_start: {required: true},
                transfer_end: {required: true}
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.parent().popover('destroy');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'request/update_transfer_list',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            j.message({text: e.text}, function () {
                                window.location = _url + 'transfer_list';
                            });
                        }
                    }
                });
                return false;
            }
        });
    };
    var delele_transfer_assignee = function () {
        var _html = j('.trans-assignee');
        _html.find('.fa-times').unbind().bind('click', function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/delete_transfer_assignee',
                method: 'post',
                data: {'issueid': _this.attr('href')},
                success: function (e) {
                    if (e.message === 'success') {
                        _this.parent().remove();
                        if (_html.find('label').length === 0) {
                            _html.html('Assignee');
                        }
                    }
                }
            });
            return false;
        });
    };
    var show_transfer_assign_it = function (id) {
        var _html = j('.trans-assignee');
        var _temp = '';
        j.ajax({
            url: _url + 'request/show_transfer_assignee',
            method: 'post',
            data: {'issueid': id},
            success: function (e) {
                for (var i = 0; i < e.length; i++) {
                    _temp += ' <label class="label label-primary"><a href="' + e[i].ITID + '" class="fa fa-times"></a> ' + e[i].fname + ' ' + e[i].lastname + '</label>';
                }
                _html.html(_temp);
                delele_transfer_assignee();
            }
        });
    };
    var transfer_assignee = function () {
        j.ajax({
            url: _url + 'request/add_assignee',
            method: 'post',
            success: function (partTags) {
                j(".transfer-assignee").autocomplete({
                    delay: 45,
                    source: partTags,
                    select: function (event, ui) {
                        var _this = j(this);
                        j.ajax({
                            url: _url + 'request/new_list_assignee',
                            method: 'post',
                            data: {
                                'issueid': _this.attr('data-bind'),
                                'assignee': ui.item.value
                            },
                            success: function (e) {
                                if (e.message === 'success') {
                                    show_transfer_assign_it(_this.attr('data-bind'));
                                    delele_transfer_assignee();
                                    _this.val('');
                                } else {
                                    _this.val('');
                                }
                            }
                        });
                        return false;
                    }, focus: function (event, ui) {
                        var _this = j(this);
                        event.preventDefault();
                        _this.val(ui.item.label);
                    }
                });
            }
        });
    };
    var update_submit = function () {

    };


    var add_pc_username = function () {



    };


    var pc_names = function () {
        var _cc = j(".add-pc-name");
        _cc.autocomplete({
            delay: 45,
            appendTo: _cc.parent(),
            source: function (request, response) {
                j.ajax({
                    url: _url + 'request/add_cc',
                    method: 'post',
                    data: {name: request.term},
                    success: function (partTags) {
                        var results = j.ui.autocomplete.filter(partTags, request.term);
                        response(results.slice(0, 10));

                    }
                });
            },
            select: function (event, ui) {
                var _this = j(this);
                _this.val(ui.item.label);
                return false;
            }, focus: function (event, ui) {
                var _this = j(this);
                _this.val(ui.item.label);
                return false;
            }
        });
    };
    var delete_added_pc = function () {
        var added = j('.pc-added-name');
        added.find('.fa-times').unbind().bind('click', function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/delete_added_pc',
                method: 'post',
                data: {transfer_id: _this.attr('href')},
                success: function (e) {
                    _this.parent().remove();
                }
            });
            return false;
        });
    };
    var show_pc_list = function () {
        var pcname = j('.add-pc-name');
        var added = j('.pc-added-name');
        var _html = '';
        j.ajax({
            url: _url + 'request/show_added_pc',
            method: 'post',
            data: {transfer_id: pcname.attr('data-bind')},
            success: function (e) {
                for (var i = 0; i < e.length; i++) {
                    _html += ' <li class="list-group-item">' + e[i].user_pc_name + ' <a href="' + e[i].IDL + '" class="pull-right fa fa-times text-danger"></a></li>';
                }
                added.html(_html);
                delete_added_pc();
            }
        });
    };

    var add_pc_name = function () {
        var pcname = j('.add-pc-name');
        pcname.on('keydown', function (e) {
            var code = (e.keyCode || e.which);
            var _this = j(this);
            if (code === 13) {
                j.ajax({
                    url: _url + 'request/add_pc_uname',
                    method: 'post',
                    data: {
                        transfer_id: _this.attr('data-bind'),
                        user_pc_name: _this.val()
                    },
                    success: function (e) {
                        _this.val('');
                        show_pc_list();
                    }
                });
            }
        });
    };
    var radio_rating = function () {
        j('[data-toggle="tooltip"]').tooltip();
        var _modl = j('.modal-rating');
        _modl.on("contextmenu", function () {
            return false;
        });
        j('.star-radio').change(function () {

            var _this = j(this);
            var _feed = '';
            j('.start-rating').removeClass('star-checked');
            if (parseInt(_this.val()) === 1) {
                _this.parent().addClass('star-checked');
                _feed = "We're worried that you are unhappy at present. We have been immediately alerted and will be in touch to understand more about your issues.";
            } else if (parseInt(_this.val()) === 2) {
                _this.parent().addClass('star-checked');
                _this.parent().parent().addClass('star-checked');
                _feed = "We're concerned that things aren't exactly as you expected. Your feedback is appreciated and we will be in touch shortly to understand what more we can do in the future.";
            } else if (parseInt(_this.val()) === 3) {
                _this.parent().addClass('star-checked');
                _this.parent().parent().addClass('star-checked');
                _this.parent().parent().parent().addClass('star-checked');
                _feed = "We're concerned that things aren't exactly as you expected. Your feedback is appreciated and we will be in touch shortly to understand what more we can do in the future.";
            } else if (parseInt(_this.val()) === 4) {
                _this.parent().addClass('star-checked');
                _this.parent().parent().addClass('star-checked');
                _this.parent().parent().parent().addClass('star-checked');
                _this.parent().parent().parent().parent().addClass('star-checked');
                _feed = "We're really pleased that you are satisfied with us at the moment and we really appreciate your feedback.";
            } else if (parseInt(_this.val()) === 5) {
                _this.parent().addClass('star-checked');
                _this.parent().parent().addClass('star-checked');
                _this.parent().parent().parent().addClass('star-checked');
                _this.parent().parent().parent().parent().addClass('star-checked');
                _this.parent().parent().parent().parent().parent().addClass('star-checked');
                _feed = "We're delighted that you are so happy with us at the moment and we really appreciate your feedback.";
            }


            _modl.find('.rating-numb').html(_this.val() + ' Star' + (_this.val() > 1 ? 's' : ''));
            _modl.find('.rating-numb-val').val(_this.val());
            _modl.find('textarea').val('');
            _modl.find('.feedback-message').html(_feed);

            _modl.find('.form-suggestion').removeClass('hide');
            _modl.find('.success-rating').addClass('hide');
            _modl.find('.submit-suggestion').removeClass('hide');
            _modl.find('.ok-rating').addClass('hide');

            _modl.modal({
                backdrop: 'static',
                keyboard: true
            });
        });

        _modl.find('.submit-suggestion').click(function () {
            _modl.find('.form-suggestion').submit();
        });

        _modl.find('.form-suggestion').validate({
            rules: {
                suggestion: {
                    required: true
                }
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.parent().popover('destroy');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'request/ratings',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            _modl.find('.form-suggestion').addClass('hide');
                            _modl.find('.success-rating').removeClass('hide');
                            _modl.find('.submit-suggestion').addClass('hide');
                            _modl.find('.ok-rating').removeClass('hide');
                            pubnub.publish({channel: '78954512156412', message: {
                                    "msg": "rating",
                                    "text": e.text
                                }});
                        }
                    }
                });
                return false;
            }
        });

    };
    var rating_pie = function () {
        var json_reports = function (output) {
            if (typeof window.g_pie !== 'undefined') {
                window.g_pie.destroy();
            }
            window.g_pie = new Chart(j("#chart-area"), output);
        };
        if (j("#chart-area").length) {
            j.ajax({
                url: _url + 'request/ratings_report',
                dataType: 'json',
                method: 'post',
                success: json_reports
            });
        }
    };
    return{
        init: init,
        success: success
    };
})(jQuery);

