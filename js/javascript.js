var EM = EM || {};
EM.javascript = (function (j) {
    var _url = j('.page-url').attr('content');
    var init = function () {
        guest_ticket_create();
        fileattached_guest();
        add_users();
        update_users_info();
        delete_user();
        reset_password();
        uploadcsv();
    };
    
    
    var show_csv_report = function(){
        console.log("csv data was triggered!");
        j.ajax({
            url: 'response/getcsvinfo',
            dataType: 'json',
            method: 'post',
            success: function (output) {
                j(".validation").addClass("hide");
                        j("#table").removeClass("hide");
                j("#table").html("<thead><tr><th>#</th><th>Email</th><th>First Name</th><th>Last Name</th><th>Job Title</th><th>Location</th></tr></thead>");
                var count = 0;
                for (var i = 0; i < output.length; i++) {
                    console.log('hide error validation');
                    count++;
                    
                         
                    j("#table").append("<tr><td><span class='glyphicon glyphicon-ok'></span>&nbsp;"+count+"</td><td>"+output[i].email+"</td><td>"+output[i].fname+"</td><td>"+output[i].lastname+"</td><td>"+output[i].job_title+"</td><td>"+output[i].location+"</td></tr>");
                }
            }
        });
        

        
        
        
    };

    var uploadcsv = function () {
        var _form = j('.importusersform');
        _form.find('.uploadfile').click(function () {
            console.log('this was click');

            _form.ajaxSubmit({
                url: 'response/csvimport',
                dataType: 'json',
                method: 'post',
                data: _form.serialize(),
                success: function (output) {
                    console.log(output.validation);
                    if(output.validation != 'csv'){
                        j(".validation").removeClass("hide");
                        j("#table").addClass("hide");
                        j(".validation").html("<br><div class='alert alert-danger alert-dismissable' style='text-align: center;'>Please upload valid CSV file</div>");
                    }else if(output.validation === 'csv'){
                        show_csv_report();
                    }
                },
                error: function (output) {
                    console.log(output);
                }
            });

        });
    };
    var update_users_info = function () {

        var _form = j('.form_info');
       
        _form.each(function () {

            var _this = j(this);


            _this.validate({
                rules: {
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.parent().addClass('has-error');
                        _elm.popover({content: value.message, trigger: 'focus', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.parent().removeClass('has-error');
                        _elm.popover('destroy');
                    });
                },
                submitHandler: function (form) {
                    var _form = j(form);

                    j.ajax({
                        url: _url + 'Response/update_user',
                        dataType: 'json',
                        method: 'post',
                        data: _form.serialize(),
                        success: function (output) {
                            if (output.message === 'success') {
                                j.notify(output);
                                console.log(j('.user_value').val());
                            }
                            /*
                             if (output.message === 'success') {
                             j.notify(output);
                             console.log('aal');
                             setTimeout(function () {
                             
                             }, 4000);
                             }
                             console.log(output);
                             // _form.find('.output').html(output.text);
                             */
                        }
                    });
                    return false;
                }
            });
        });
    };
    var delete_user = function () {
        var _mod = j('.delete_user_div');
        j('.btn-delete').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _this = j(this);
                _mod.modal();
                _mod.find('.delete-id').val(_this.attr('data-reset'));
                console.log(_this.attr('data-reset'));
            });
        });
        j('.delete-password-form').validate({
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('destroy');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'Response/delete_user_r',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (output) {
                        if (output.message === 'success') {
                            j.notify(output);
                            _this[0].reset();
                            _mod.modal('hide');
                            location.reload();
                        }

                    }

                });
                return false;
            }
        });

    };
        var reset_password = function () {
        var _mod = j('.reset-password');
        j('.btn-password').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _this = j(this);
                _mod.modal();
                _mod.find('.password-id').val(_this.attr('data-reset'));
                console.log(_this.attr('data-reset'));
            });
        });
        j('.reset-password-form').validate({
            rules: {
                password: 'required',
                repassword: {
                    required: true,
                    equalTo: "#pass"
                }
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('destroy');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'Response/reset_password',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (output) {
                        if (output.message === 'success') {
                            j.notify(output);
                            _this[0].reset();
                            _mod.modal('hide');
                        }
                    }

                });
                return false;
            }
        });

    };
    var add_users = function () {
        var _form = j('.formaddusers');

        _form.validate({
            rules: {
                firstname: 'required',
                lastname: 'required',
                email : {
                    required: true,
                    email: true
                },
                password: 'required',
                JobTitle: 'required',
                location: 'required'
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'focus', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('destroy');
                });
            },
            submitHandler: function (form) {
                var _form = j(form);

                j.ajax({
                    url: _url + 'Response/add_users',
                    dataType: 'json',
                    method: 'post',
                    data: _form.serialize(),
                    success: function (output) {
                        if (output.message === 'success') {
                            j.notify(output);
                            setTimeout(function () {
                                location.reload();
                            }, 1000);
                        }
                        /*
                         if (output.message === 'success') {
                         j.notify(output);
                         console.log('aal');
                         setTimeout(function () {
                         location.reload();
                         }, 4000);
                         }
                         console.log(output);
                         //_form.find('.output').html(output.text);
                         */
                    }
                });
                return false;
            }
        });
    };
    var fileattached_guest = function () {
        j('.attached,.fake-path').click(function () {
            var _this = j(this);
            j('.fileupload').click();
        });
        j('.fileupload').change(function () {
            var _val = '';
            var _this = j(this);
            var files = _this.prop("files");
            for (var i = 0; i < files.length; i++) {
                _val += (files[i].name) + ',';
            }
            _val = _val.slice(0, -1);
            j('.fake-path').val(_val);
        });
    };

     var guest_ticket_create = function () {
        var _create = j('.Guest-form');
        var _htmled = j('.create-guest-ticket');
        if (_htmled.length) {
            var editor = CKEDITOR.replace('create-guest-ticket', {
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
            _create.validate({
                rules: {
                    priority: 'required',
                    subject: 'required',
                    firstname: 'required',
                    lastname: 'required',
                    location: 'required',
                    email: {
                        required: true,
                        email: true
                    }
                    
                }, showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.parent().addClass('has-error');
                        _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.parent().removeClass('has-error');
                        _elm.parent().popover('destroy');

                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    _this.find('.create-guest-ticket').val(editor.getData());
                    _this.ajaxSubmit({
                        url: _url + 'response/create_ticket_guest',
                        beforeSend: function () {
                            _this.find('.progress').removeClass('hide');
                            _this.find('.btn-send').attr('disabled', 'disabled');
                            _this.find('.btn-send span').html('SENDING....');
                        },
                        uploadProgress: function (event, position, total, percentComplete) {
                            _this.find('.progress').removeClass('hide');
                            _this.find('.progress-bar').css({width: percentComplete + '%'}).html(percentComplete + '%');
                        },
                        complete: function (e) {
                            console.log(e);
                            var _e = (j.parseJSON(e.responseText));
                            console.log(_e);
                            if (_e.message === 'success') {
                                j.message({text: _e.text}, function () {
                                    $('.validation').html("<div class='alert alert-success'><span class='glyphicon glyphicon-ok-sign'></span> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>" + _e.text + " <div>");

                                    window.location = _url + 'Guest';
                                });
                                editor.setData('');
                                _this[0].reset();
                            } else {
                                $('.validation').html("<div class='alert alert-warning'><span class='glyphicon glyphicon-warning-sign'></span> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>" + _e.text + " <div>");
                            }
                            _this.find('.progress').addClass('hide');
                            _this.find('.btn-send').removeAttr('disabled');
                            _this.find('.btn-send span').html('SEND');
                        }
                    });
                    return false;
                }
            });
        }
    };
    return{
        init: init
    };
})(jQuery);

